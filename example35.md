## Prolog

List, który otrzymał hrabia był lapidarny.

>>>
"Bracie,

Ojciec mocno choruje\
i chce cię ujrzeć.

Sloan"
>>>


Hrabia nie znał młodego posłańca, który teraz jechał wraz z nimi z powrotem do dalekich siedzib. Mieli już za sobą ponad tydzień drogi, ale wrażenia z podróży były takie, jakby nie istniał inny świat niż ten, który widać z karocy, którą jechał Maksimiliam z Gregorym. Drugi, uboższy powóz wiózł ze sobą zapasy i kilka osób do dbania o wielmożę podczas wyprawy.

Przez tydzień po wyjechaniu poza Novigrad mijali często miejsca, w którym można było zatrzymać się i wygodnie spędzić noc. Ba, czasem wręcz można było wybierać, czy tym razem domek bardziej ciemny, czy z podmurówką. To jednak skończyło się po przekroczeniu dużego przyrzeczonego miasta, które było bramą do terenów prowadzących do posiadłości rodu de Falco.

Na koniec trzeciego dnia zatrzymali się w gościnie u rodziny, która akurat mieszkała w okolicy. Oddali swoje domostwo, sami zaś umiejscowili się w kuchennym pomieszczeniu. Czy mogli narzekać, tego Gregory nie wiedział, bo nie znał zwyczajów. Hrabia jednak miał lekką rękę w wydawaniu pieniędzy w czasie tej podróży.

Domostwo pomimo, że nie było najwyższych standardów, miało komin a nie jak się często jeszcze spotykało palenisko na środku dużego pomieszczenia. Widocznie tutaj łatwiej było o pomysłowość i chęć do modernizacji, kiedy zima kąsała tak mocno, że czuć było jej lodowe kły przy każdym powiewie powietrza.

Domostwo miało trzy izby, ale dostępne dla gości było tylko jedno pomieszczenie, bo w drugim mieszkały sobie kilka owiec i kur. Zwierzęta nie były głośne, ale śmierdzące - owszem.

\-   Nie jest łatwo dostać się w zimę do siedziby Pana rodu, Panie Hrabio. - Powiedział Gregory, który raczej źle znosił zimne warunki. Pochodził w końcu z zupełnie innych krain. Ciągle kichał, siąkał nosem i wyglądał na nieszczęśliwego.

Hrabia Sokół nie utyskiwał, chociaż podróż i dla niego była męcząca. Nie mniej, już parę razy przebył tę drogę, raz nawet ze swym dobytkiem, całkiem z resztą pokaźnym. Zimno mu nie przeszkadzało także, on oraz Grot w końcu wychowali się wśród tych iglastych lasów, skał i chłodu, doskwierał za to szlachcicowi tępy ból uda. Rana, chociaż pod dobrą opieką już się zasklepiła, jednak definitywnie uczyniła Maksimiliama kaleką. Nadal był sprawny, potrafił jeździć konno, ale noga drętwiała na tyle szybko, że czasem miał już problem, aby samodzielnie zejść z siodła, dlatego zdecydował się na podróż powozem.

\-   Wiosną i jesienną porą nie byłoby łatwiej, obawiam się. - Odparł Sokół z grymasem pozwalając, aby sługa rozmasował obolałe miejsca. - Wiosną roztopy zalegają na trakcie, jest bagnisto i grząsko, jesienią natomiast niewielkie kaskady spływają z gór, a często siąpi całymi tygodniami. Zimy są ostre, ale za to wszystko skuwa lód i jest to moja, zaraz po lecie, zdecydowanie ulubiona pora. 

Sokół przytaknął i pociągnął łyk okowity po czym podał bukłak Gregoremu.

\-   Pijcie. Obawiam się o wasze zdrowie, Gregory. Pogoda i trudy podróży odciskają swe piętno, jak nietrudno spostrzec. Przyda wam się rozgrzać, gdzieś była jeszcze jedna barania skóra... tak, dziękuję, okryjcie nią Gregorego. Nie chcemy mieć go na sumieniu. 

Polecenie wykonała młoda dziewczyna o imieniu Ann. Była lekko piegowata, miała brązowe, krótkie włosy kończące się nad ramionami i wiecznie zamyślony wyraz twarzy. Pomimo tego, że wyglądała na żyjącą w swoim własnym świecie, zawsze szybko reagowała na polecenia. Gregory dziwił się, że wzięto dziewczynę na taką wyprawę, ale Grot mówił, że strasznie nalegała, aby ją zabrać. Co ostatecznie okazało się dobrą decyzją, bo opiekowała się swoim panem z zapałem i oddaniem. Teraz gdy inni woleli myśleć o swojej wygodzie, oprócz Deryka, który dość niedbale masował kolano hrabiemu, dziewczyna opatuliła Gregorego puchatym futrem i, wbrew jego obawie, nie wytarła mu nosa, a jedynie podsunęła kawałek miękkiego materiału.

\-   Strasznie Ci dziękuję, Ann! - Podziękował medyk, ale po zamyślonej twarzy Ann nie widać było w tym momencie emocji na które liczy człowiek mówiąc coś do dziewczyny. Nie zachowywała się jak byle dzierlatka. Ale byle dziewoi by nie wzięli na taką zamieć.
\-   Panie hrabio, mam nadzieje, że jutro też znajdziemy miejsce do noclegu. Ile mniej więcej drogi dzieli nas od Steriling, tego miasta u podnóża twierdzy? Pamiętam tylko, że musimy iść albo przez las Wyklętych albo wzdłuż jeziora Srebrzystego.

\-   Wolałbym wybrać drogę wzdłuż jeziora, ufam że teren będzie tam mniej nieregularny, a zamarznięta tafla pozwoli zaoszczędzić nam siły, oraz dnia, zetniemy w ten sposób szmat drogi. - Odparł szlachcic, miał szlachetne rysy, ale z szarozielonych oczu biła serdeczna przychylność. Dobrze przycięta bródka tym razem jednak powoli zamieniała się w brodę, nie do końca elegancką, za to chroniącą przed chłodem.

Maksimiliam pierwotnie ignorował niedbałość Deryka, ale w końcu i on, mimo całej dobroci serca, odsunął niecierpliwie dłoń swego sługi i sam zabrał się za bandażowanie, chociaż nie miał w tym żadnej wprawy. Tym razem bandaż nie tamował rany, a zwyczajnie chronił mięśnie przed nadmiernym uszkodzeniem. Ann zauważając to, dostąpiła do hrabiego i szybko mu pomogła, co nie umknęło oczywiście Derykowi, który gdyby ktoś na niego patrzył, mógł powiedzieć, że sytuacja go dotknęła. Zdenerwowany ogrzewał teraz ręce przy kominku.

Wszyscy jednak przerwali swoje działanie, kiedy rozległ się potworny kaszel. Gregory krztusił się palącym go w przełyk trunkiem.



\-   Przepraszam... zapomniałem się i napiłem się jak wina. - Mówił czerwony na twarzy. Zdziwienie nie znikało z oblicz obserwujących go osób, więc dodał - no, u mnie nie pija się okowity tylko wino, nie jestem wciąż przyzwyczajony\...

Spojrzał się w stronę spiżarni, gdzie pojawiło się ciekawskie dziecko, syn gospodarzy, na oko pięciolatek. 



Szlachcic roześmiał się serdecznie, jemu jednemu właściwie wypadało śmiać się ze swojego towarzysza tak otwarcie, przy czym służba zapewne także ową niecodzienną reakcją na okowitę była rozbawiona. Liam powędrował wzrokiem za spojrzeniem Gregorego, aby ujrzeć dziecko swoich dalekich krewnych, chociaż jego własne zapasy były już nadwątlone, wyjął jabłko i zawołał dziecko, aby wręczyć mu ten prezent. Poczochrał kudłatą, rudawą głowę chłopca, ciągle uśmiechając się pogodnie.

\-   Właśnie uświadomiłem sobie, że nie widziałem Sloana, mego brata, od pięciu lat. Gdy żeniłem się, on był na wyprawie, a później czas tak szybko popłynął... pewnie ma już stadko pociech... - podzielił się tą refleksją z Gregorym i tym razem jego słowa zabrzmiały zaskakująco ponuro jak na całą pogodę, którą szlachcic miał w sobie.

Gregory już doszedł do siebie i zaczął zajmować się kolanem i raną Hrabiego, ale zdawać się mogło, że po gorzko brzmiących słowach, próbował ulżyć nie ciału, ale jego duszy.



\-   Skąd taki smutek hrabio? - Zadał pytanie na tyle cicho, by mieć nadzieje, że pozostanie tylko dla uszu jego i jego rozmówcy. Na co hrabia Sokół potrząsnął tylko głową i uśmiechnął się nieznacznie.
\-   To niecodzienne przeżycie, wrócić do rodzinnych stron po tych paru latach, które przesadnie długimi się nie jawiły, aczkolwiek zmieniły tak wiele. - Mówiąc to nie zdawał sobie sprawy jak wiele miał racji.

## Gorące powitanie
**Postacie:** Liam, Irvette, Gregory

Trzy dni później...



Na szczęście, im bliżej miasta tym łatwiej znowu było o nocleg. Kolejne noce przespali w znacznie wygodniejszych miejscach, może nie tak wygodnych jak w okolicach Novigradu, ale tutaj, po dniach spędzonych na mrozie, każde miejsce było jak raj koronujący życie pełne cierpienia.

Zbliżali się szybko do Sterling brzegiem jeziora pomimo zmęczonych ciał. Pomimo ciepłych noclegów, świadomość, że wczesnym rankiem, kiedy jeszcze jest ciemno, trzeba wyruszyć w podróż, wyduszała z umysłów i ciał wszelką energię. I nagle, na widok murów miejskich jakby cała ta energia wracała i pchała dalej podróżników i konie, które ciągnęły sanie.

Uczucie to można porównać tylko do powrotu do domu po latach podróży, pomyślał Gregory. Chyba tak się poczuję, jak wrócę do swoich stron.

Pomimo zimna, mijało ich sporo osób - na wozach, koniach, pieszo (chociaż parę osób jeszcze miało na nogach dziwne drewniane obręczę, które ułatwiały im jakoś chodzenie po śniegu). Zdziwił się jednak w momencie, w którym zobaczył człowieka na reniferze i natychmiast wskazał go hrabiemu.

\ - To solidne i piękne zwierzęta, a przede wszystkim nie marzną tak, jak zmarznąć mogą wierzchowce. Chociażby te nasze. Gdy dotrzemy w me rodzinne progi dostaną ciepłe derki i ogrom siana. - Zapewnił hrabia, zdejmując na chwilę rękawiczki, aby założyć wyciągniętą z zabezpieczonego kuferka biżuterię. Złotą, zapewne rodową, wisior, oraz dwa pierścienie, z czego jeden z pewnością był pieczęcią, którą hrabia sygnował listy. Zauważyć mógł Gregory grawer przedstawiający skrzydła, a zgadując bystrze po nazwisku hrabiego, były to skrzydła drapieżnego ptaka. 

Ludzie niewątpliwie przywykli tu do zimna, ich stroje były adekwatne do pogody, grube, skórzane i futrzane, wyszywane nie delikatną złotą nitką i srebrną igiełką, a solidnymi dratwami. Nie każdą głowę zdobił czepiec czy czapka, kobiety, także niemłode nosiły odkrytą głowę dumnie eksponując grube warkocze, nierzadko kręcone czy kędzierzawe.



\-   Hrabio, jakoś tak te kobiety starsze też są piękne, jakby je te zimno tutaj konserwowało. Warto było stąd wyjeżdżać?

Maksimiliam zdziwił się.

\-   Nigdy nie zadałem sobie tak brzmiącego pytania, przyznać muszę, drogi Gregory. Zadawałem sobie za to nieco odmiennie brzmiące zapytania i jeśli cię to zajmuje, odpowiem w takiej właśnie formie. Czy chciałem? Nie, to naprawdę cudownie miejsce i wspaniałe ziemie, gdzie dzika natura i ostra przyroda w niczym nie wadzi, może wszystko jest niezwykłe dzięki temu, lub pomimo tego. Sam nie wiem. Nie chciałem wyjeżdżać. Czy powinienem był? Owszem, gdyż wierzę głęboko, że każdy ma swoją do wypełnienia rolę i swoją powinność względem innych ludzi, rodziny czy oddanych. Ja nie zostałbym panem okolicznych ziem, co wiadomym mi było od lat najmłodszych, gdyż tę rolę obejmie mój starszy brat, Sloan de Falco, a po nim jego synowie. I to nie tak, że mię kto wygnać z tej racji postanowił, bowiem mogłem zostać, oczywiście, ale ziemie mego rodu są dosyć rozległe, a zważywszy na fakt, iż także na dalekim Pograniczu, postanowiłem chlubnie pomnażać dobra mego rodu tam, gdzie będzie to bardziej użyteczne, a że przydarzyła się i perspektywa lukratywnej koligacji z rodziną tamtejszego księcia, decyzja chociaż trudna, była oczywista.

Wjeżdżali w obręb przedmurza, w niską i raczej biedną zabudowę drewnianą, jednak bieda tutaj miała nieco inny wymiar niż np. w Novigradzie. Różnica polegała na tym, że nawet najlichsze budynki były wykonane porządnie i małymi chociaz dekoracjami na oknach, okiennicach czy odrzwiach. Domy były chyba odzwierciedleniem ducha mieszkańców, bo żadna mijana osoba nie wyglądała, jakby miała kark stworzony do pochylania.

\-   Zatem dla hrabiego to ród jest najważniejszy?
\-   Tak, oraz zwierzęta. - Roześmiał się, bo brzmiało to cokolwiek dziwnie, ale nie było dalekie od prawdy. - Powiem z niemałą, acz mam nadzieje słuszną dumą, iż sokolnictwo oraz łowy mój ród doprowadził do perfekcji. Zapewne ujrzysz hodowlę, wybiegi oraz psiarnię. Zajmowanie się tym na Pograniczu daje mi namiastkę domu.

Gregory przez mgłę pamiętał psiarnię z pogranicza z uwagi na to, że głównie czytał księgę po dawnym mieszkańcu wieży. Czuł ulgę na wieść o wyjeździe, bo czuł się już chorobliwie od tego ciągłego czytania. Jednak kiedy przebywał w twierdzy, jego myśli uparcie wędrowały właśnie do tych kilku książek, które sprawiały, że potrafił dniami siedzieć i czytać. Ciągle miał wrażenie, że im częściej tam zaglądał, tym więcej znajdował elementów których wcześniej nie widział. Raz znalazł nawet cały rozdział w środku księgi wśród najwyraźniej zlepionych kart. Dlatego też lubił strasznie wyjazdy do Gustawa i dlatego też pomimo zimna ekscytował się całą tą wyprawą. Nie widział też, że Ann która zawsze sprawiała wrażenie nieobecnej, kiedy nikt na nią nie patrzał, usilnie mu się przypatrywała.

Dojeżdżali właśnie do bram miasta, szeroko otwartych dla wszystkich wchodzących i wychodzących. Nad bramą widniały inskrypcję zapisane dziwnymi znakami. Gregory zapytał się o nie hrabiego, na co hrabia odpowiedział lakonicznie, chociaż bardzo jasno.



Miasto przez które przejechali w drodze do zamku de Falco, było dziwnym mariażem cywilizacji i tego co dzikie. Ludzie którzy tutaj przebywali byli jak wyrzeźbieni z otaczających ich skał. Nieważne czy była to kobieta, mężczyzna czy dziecko. Wyobraźnia podpowiadała, że nawet miejscowe zwierzęta były twardsze niż te z pozostałej części cesarstwa. Choćby ta kobieta, która handlowała chyba mlekiem kiedy mróz nawet wewnątrz ich wozu był tak odczuwalny, że Gregory kulił się w futrach. Gdyby spotkał ją pośrodku gór, na grzbiecie renifera, mógłby na wszelki wypadek przyklęknąć, bojąc się czy to przypadkiem nie jest miejscowa boginii mrozu. Myślał sobie wtedy "Gdyby Ci ludzie chcieli, mogliby zejść z gór i podbić wszystkich rozkochanych w  wygodzie".



Tym większy był szok, kiedy na ich powitanie tuż za bramą zamku wybiegła młoda kobieta, która rzuciła się na Hrabiego, aby go wyściskać. W oczach miała masę radości, ekscytacji i czegoś, co Gregory uznał chyba za uwielbienie Maksymiliania.



Irvetta, bratowa hrabiego po uściskaniu odezwała się do niego z wyrzutem.

\- Pięć lat! Pięć lat! - W jej głosie brzmiała ukryta gorczyc, która przemieniała się w radość - Dobrze Cię widzieć Liamie! Kiedy mój maż powiedział, że przyjedziesz, codziennie wyglądałam kiedy wreszcie tutaj będziesz. Powiedz, co Cię sprowadza do nas? Stęskniłeś się? Ah, wybacz! Wchodzcie do zamku, podróż była pewnie strasznie męcząca. Wszystko ciepłe, wszystko gotowe dla Was!



Maksimiliam także rozpromienił się i równie serdecznie wytulił kobietę, ale gdy uścisk powitania zelżał, nie wypuścił jej od razu, uprzednio ucałował jeszcze jej dłonie, z miłością i szacunkiem, no i z radością. Zanim poszli dalej, hrabia sięgnął po laskę, z którą nie rozstawał się, chociaż chciałby.

\- Tęskniłem przeogromnie, Irvetto, bogów mi na światów, że tak było! Żaden dzień mi nie minął bez myśli o was, tyle rzeczy się przez ten czas zdarzyło, dobrych i złych, tyle mam wam do opowiedzenia. Wy mnie równie dużo, zapewne. Moja małżonka, niestety, nie mogła się zjawić, przepraszam za to. Pozwól natomiast, że przedstawię ci mego medyka i towarzysza podróży, Irvetto. To Gregory Winny. Gregory, moja zachwycająca bratowa, pani Irvette de Falco.



Zamek był wielki i wydawał się jakby był częścią koścca ziemi. Nie była to całkowicie sztuczna budowla, wiele tutaj elementów zbudowane było z użyciem naturalnego ukształtowania góry. Wyobraźnia podpowiadała, że kryją się tutaj zakamarki, ukryte strumienie i wielkie groty tuż pod stopami, zamieszkałe przez istoty starsze niż świat. Dużo można mówić o wyobraźni ludzkiej, ale czasem trafia prawie co do joty w punkt. Nawet teraz, w jasny dzień, w samo południe, było tutaj jakoś tak nieludzko, jakby miejsce to nie zostało zbudowane przez ludzi dla ludzi. Uczucie to potęgowały wielkie przejścia, o wiele za duże dla ludzi, czy schody które tylko w najbardziej oficjalnych wersjach były nadbudowane mniejszymi schodkami dla wygody obecnie panujących.



I wtedy weszli do głównej hali. Była wielka. Wielka jak katedra, zaś sufit prawie niknął w ciemności. Nie było tutaj jednak zimno, chociaż nie widać było płonących wielkich pieców, które byłyby w stanie ogrzać to gargantuiczne miejsce.



\-   Robi wrażenia prawda? - Spytała z dumą Irvetta Gregorego. - Nasza siedziba ma naprawdę wiele tajemnic i pięknych miejsc. Tak, że żal opuszczać, prawda Liamie?
\-   🔹Dopiero co przybyliśmy - odparł z rozbawieniem szlachcic - nigdzie na razie nie mamy w planach się wyruszać. Muszę jednak dosyć rychło pomówić z twym małżonkiem, Irvetto. To jego list ostatecznie ponaglił mnie i popchnął w stronę decyzji o przyjeździe w rodzinne strony. Przed spotkaniem z jaśniepaństwem jednakowoż wolałbym się odświeżyć po trudach podróży, które doprowadziły mnie do stanu, w którym niekoniecznie życzyłaby sobie widzieć mnie matka i ojciec.\

🔹Maksimiliam z niezadowoleniem zwrócił uwagę, że laska, którą się wspierał podczas chodzenia wydaje cichy, ale jednak wyraźnie słyszalny dźwięk, za każdym razem, gdy spotyka się z kamiennym podłożem. Był nawet skłonny pomyśleć sobie, iż jego kalectwo i ciągłe przypominanie o tem, jest karą bogów, za różne przewiny, których się dopuścił w tym, a może i także w poprzednim życiu. Jednak widok rodzinnego domu, a raczej komnat do których się kierowali, i wspomnienie ich przytulności, ciepłe obicia wnętrz i jasność kaganów sprawiały, że irytacja hrabiego powoli malała.

🔹Przez myśl przeszedł mu także Javier, bliski przyjaciel, z którym wychował się i który to czasem odwiedzał go na Pograniczu, mimo dalekiej drogi, którą wraz ze świtą dzielnie pokonywał. O tym że przybył do Stirling hrabia postanowił młodego barona powiadomić listownie przez gońca. Zdawał sobie sprawę, że to spotkanie może poczekać, w przeciwieństwie do brata i rodziców, gdyż list, który otrzymał, wcale nie napawał optymizmem.

🔹

Pierwszy szok Gregorego co do wielkości tego miejsca minął. Kiedy przyzwyczaił wzrok widział już sklepienie u góry, choć nadal musiał przyznać, że to miejsce było przeznaczone dla osób które są półtora-dwa razy większę od człowieka. Wiele było tutaj przykładów pokazujących, że ludzie mieszkają tutaj od dawna i zaadaptowali wiele z tego miejsca na własne potrzeby. Przeszli przez katedralną halę, jak w myślach nazwał ją Gregory i dotarli do lewej częsci gdzie idąc na górę, gdzie mijali po drodze małe przesmyki, zrobione już na wymiar ludzki. Wędrówka nie trwała jednak długo, bo Hrabia nie zaczął syczeć nawet z bólu gdy dotarli do korytarza z kilkoma drzwiami. Pomimo sporej przestrzeni, nie było tutaj widać kurzu czy brudu.



\-   Tutaj Twój pokój Liamie, zaś obok mniejszy pokój dla Gregorego. Niestety, jeszcze sprzątają w tym drugim pokoju, bo dopiero niedawno wpadłam na myśl, że dobrze by było, abyś miał swojego medyka pod ręką. - Zaśmiała się. - Albo nogą. Rozgościć się może obydwaj w pokoju większym, pewnie trzeba się zająć boleściami a jak służka skończy, to zastuka do Was.



Pokój był... surowy. Idealnie wkomponowywał się w oblicze tego miejsca, ale byle pokój w twierdzy miał więcej ciepła domowego niż ten tutaj. Sprawiał wrażenie raczej celi, ale takiej wyciętej idealnie, jak dla Króla który ma spędzić tutaj resztę życia po zamachu stanu. Łóżko jednak wyglądało bardzo porządnie, a wraz z nocnikiem, miednicą i małym dywanem pod łożkiem tworzy niezwykły kontrast pomiędzy światem wygody i perfekcyjnego sześcioboku.



🔹Słudzy wnieśl bagaże jaśniepana, a do kominka solidnych rozmiarów dorzucono drwa. Solidne, drewniane okiennice pozostawały otwarte, a przez mglisto-zielone szyby, wpadało do środka pomieszczenia jeszcze więcej światła. Słudzy pomogli szlachetnemu panu zdjąć futrzane okrycie i obmyć się w misie. Maks w krótkich dyspozycjach poprosił o przygotowanie świeżego odzienia, gorącą wodę, mydło i brzytwę, aby ktoś mógł go doprowadzić do stanu używalności. Usiadł w kamiennym fotelu, sporym, ale wyłożonym mięciutkim futrem i z kubkiem ciepłego, słabego wina odetchnął by móc chwilę odpocząć.

\-   🔹Jak znajdujesz Stirling, medyku? - Zwrócił się uprzejmie do swojego młodszego towarzysza.
\-   Nie wiem co sądzę o samym mieście, ale ludzie którzy je zamieszkują na pewno są ulepieni... nie, wyrzeźbieni z czegoś innego niż reszta kraju, Panie. Ja nawet nie chciałbym się targować z nimi na targu, co dopiero wejść w sprzeczkę.

🔹Szlachcic roześmiał się, ale słowa Gregorego zapewne poczytał sobie jako komplement.

\-   🔹Prawda, ludzie są hardzi, ale mają dobre serca. Z pewnością nie można im zarzucić, iż posiadają gorącą krew, to bardzo rozsądni ludzie. - Odparł hrabia i upił odrobinę ciepłego trunku, który przyjemnie rozgrzewał przełyk. Myślami co chwilę wędrował jednak do rychłego spotkania z najbliższą rodziną, które może nie być aż tak ciepłe, jak to z bratową. - Gregory, zostaniemy w tych stronach pewnie jakiś czas, mam nadzieję, że szybko poczujesz się tu swobodnie.
\-   Muszę przyznać, że obawiam się trochę tego miejsca. Jest nieludzkie. Jakby mieszkały tutaj jeszcze niedawno jakieś olbrzymy i zaraz wrócą i zapędzą nas do niewolniczej pracy. A,właśnie! Dlaczego jest tutaj tak ciepło? Przecież ogrzanie takiego miejsca wymagałoby wielkich pieców, a tutaj nic nie widziałem ani nie słyszałem.
\-   🔹O, tak. Mieszkały tu kiedyś pradawne istoty, wysokie, z pokaźnym porożem, pył ze startych rogów otwierał wejście do świata niedostępnego ludziom... a przynajmniej tak słyszałem, będąc dziecięciem- odparł hrabia Sokół, jednak jego ton sugerował, że on sam, całą tą historię, przynajmniej w połowie, uważa za piękną bajkę. -  Ciepło jest natomiast zasługą term. Jeszcze tam się wybierzemy, zapewne.

Gregory zesztywniał w panice kiedy usłyszał o tych istotach, ale uspokoił go ton wypowiedzi. Zaraz jednak podskoczył, gdy rozległo się stukanie do drzwi. Można było spodziewać się, że to wspomniana wcześniej służka skończyła sprzątać, ale stukanie rozległo się znowu, bardziej natarczywie.

\-   Wiadomość od następcy rodu, Sloana, Jaśniepana Brata.

Poproszony wszedł do środka, skłonił się i powiedział.

\-   Pana Brat oczekuje na Pana w wewnętrznych ogrodach.


## Braterska miłość
** Postacie:** Liam, Sloan

Sługa prowadził Maksymiliana pospiesznie, przepraszając za tempo, tłumacząc to, że wszystko wyjaśni się na miejscu. Miejsce i drogą którą wybrali sugerowała jednak chociaż trochę powód spotkania. Sloan chciał się z nim zobaczyć przed oficjalnym spotkaniem i nie chciał, aby przeszedł obok miejsc, gdzie mógłby go spotkać ani ojciec ani matka.

Wewnętrzny ogród był miejscem mało znanym na zewnątrz. Naturalnie było tutaj zawsze cieplej z powodu gorącego źródła rozlewającego się w małe jeziorko, ale po dodaniu tutaj szklanego dachu rozkwitło tutaj wspaniała i bujna roślinność, którą widzieć można było tylko w ciepłych krainach i to niekoniecznie w takiej obfitości i kolorach jak tutaj. Zupełnie nie pasowało to miejsce do jego brata. Było to miejsce w którym uwielbiała przebywać Irvetta, często też z Maksymilanem, zanim opuścił to miejsce. I zanim Irvetta i Sloan się pobrali.

Stał w czerni, poluzowując sobie kaftan dla ulżenia sobie od panującego tutaj ciepła. Przyszykowany był już do oficjalnego posiłku. Był wielki jak zawsze, ze szczęką jak niedźwiedź i spojrzeniem godnym największego z tych, których tak obawiał się wchodzić w konflikt Gregory. Hrabia nie był nikczemnej postury, ale z dwóch braci to ten starszy odziedziczył najpotężniejsze cechy zarówno tych ziem jak i ich ojca. Oczy jak zawsze zimne, ale jeszcze nigdy nie tak mordercze jak teraz. Jak oczy zwierzęcia, które waha się czy zabić ofiarę czy jednak uznać obcość sytuacji i wycofać się, poczekać, aż sytuacja stanie się prostsza. I wtedy zabić.

\-   Bracie. Nasz ojciec oszalał i chce, abyś został jego następcą.



🔹Maksymiliam zatrzymał się o parę kroków od brata, który zamiast kurtuazyjnego pozdrowienia, przywitał go szokującą informacją. Zaskoczenie wyraźnie odmalowało się młodszego szlachcica. Dosyć zgrabnie oparł się obiema dłońmi o laskę postawioną przed sobą, chociaż po prawdzie zrobiło mu się przez chwilę słabo na myśl, że informacja podana przez Sloana może być prawdziwa.

\-   🔹Drogi bracie... - zaczął łagodnie - to chyba nie jest najlepszy moment na żarty, nie uważasz? - Mówił powoli, ważąc słowa, tym bardziej że poczuł się wybitnie nieswojo, a to uczucie było mu niemal obce. - Nawet jeśli usłyszałeś takie słowa od naszego ojca, to,wyobrażam sobie, musiałby być wypowiedziane w gniewie i tylko po to, aby cię nimi ugodzić, gdyż... ja nic nie słyszałem na ten temat. Przybyłem natomiast na twoją wyraźną prośbę, Sloanie.

🔹

\-   Dlatego, że ojciec myśli że posłaniec niósł jego wiadomość a nie moją do Ciebie Bracie. I wiesz, że nie jestem dobry w żartach a najlepszy dowcip który zrobiłem, to kiedy wylaliśmy wodę na głównych schodach w czasie mrozu a potem, gdy ojciec miał właśnie pójść tymi schodami pobiegłem, aby go powstrzymać i sam się wywaliłem. Więc nie, to nie żart. - Mówił pospiesznie i rzeczowo - Musisz wiedzieć, że ojca ogarnęła obsesja na punkcie poznania dawnych mieszkańców tego miejsca a po pewnej wyprawie do niższych grot musiał coś zobaczyć, co sprawiło, że postradał zmysły. Za chwilę musimy iść do olbrzymiej sali i tam ojciec chce Cię oficjalnie mianować na następce. Miesiąc temu to miałem być według wszelkich zamierzeń, praw i przypuszczeń ... Ja.



🔹Sokół potrząsnął głową, jakby jego niemy sprzeciw miał jakąkolwiek moc sprawczą w tym momencie. Patrzył na brata szerozielonymi oczami, jeszcze przez chwilę łudząc się, że to żart.

🔹

\-   🔹Dlaczego więc nie napisałeś mi o tym? Czemu dowiaduję się dopiero teraz? Czemu nie pozwoliłeś mi oszczędzić sobie tej podróży i po prostu się nie zjawić, jeśli oczywiście twe słowa, bracie, są prawdą? - Tym razem w jego głosie pobrzmiewała irytacja i nie udało mu się skryć tych emocji. Wyciągnął chustkę, barwioną i delikatnie wyszywaną, otarł nią spocone czoło, nie wiedząc czy z powodu zdenerwowania, czy też z powodu panującego tu gorąca, a może obu tych rzeczy czuje się tak osłabiony. - Dlaczego stawiasz mnie w tej sytuacji, bracie? - ciągnął dalej. - Wiesz, że nie chcę podważać żadnej decyzji ojca, nie chcę mu się sprzeciwiać, na równi z tym, jak bardzo nie chcę zostać tutejszym włodarzem. Przywiozłem nawet podarunki tobie na ceremonię, gdyż sądziłem że jaśniepan ojciec... że jest bliski...

🔹

🔹Liam usiadł na kamiennej ławeczce, czując że napięte mięśnie tylko pogarszają jego samopoczucie.

🔹

\-   🔹Dlaczego ty mi to robisz, Sloanie?
\-   Zrobiłem to, abyś nie wątpił ani przez moment, że to ja zostanę władcą tego miejsca. Jednak zanim pomyślisz, że to tylko moja chora ambicja, odmów Ojcu dzisiaj wprost. Wtedy zobaczysz co się stanie. Na to nic cię nie przygotuje. Zobaczysz kim stał się nasz Ojciec i jaka choroba go toczy. Tylko wtedy mi uwierzysz.

Soczystość barw tego miejsca tak mocno potęgowała szaleństwo tego, co właśnie dowiedział się Hrabia. Tak jak dawne pogawędki z Irvettą pozwalały mu kochać życie, tak teraz...



\-   Wróć do swojej komnaty, dokończ przygotowania. Ja musze już iść.

## Przyjacielu przybywaj!
** Postacie:** Liam, Gregory

🔹Czuł, że krew huczy mu w uszach, a plątanina myśli nie pozwala mu wyłowić tylko jednej, konkretnej. W następnym zaraz momencie umysł pobrzękiwał absolutną pustką, przeto Liam nawet nie zauważył, kiedy znalazł się w swojej komnacie. Umył się w zimnej wodzie i to trochę ostudziło jego nerwy, a dalsze zabiegi, którym się poddawał pozwoliły mu rozważyć wszelkie ścieżki rozwoju wydarzeń.

🔹


🔹Rozległo się pukanie do drzwi i chociaż uprzejme, to bez oczekiwania na zaproszenie, gdyż składającą wizytę osobą, był sam hrabia Sokół. Znów wyglądał na możnego szlachcica, którym był w istocie, z doskonale przyciętą bródką i gładkimi policzkami, uwydatniającymi jego proporcjonalne rysy twarzy. Brokatowa, wyszywana tunika w złoto-zielonym kolorze, podkreślała jego sylwetkę, razem ze skórzanym pasem, dopełnia komplet.

🔹

\-   🔹Gregory, mam do ciebie prośbę niecierpiącą zwłoki. - Zaczął szlachcic podając mu zalakowany list. Palce hrabiego zdobiły kunsztowne pierścienie, które współgrały z wisiorem, z resztą ten komplet Gregory już znał. - Chcę, abyś wyszedł przed dziedziniec, znalazł miejscowego chłopaka i kazał mu zanieść wiadomość do barona Javiera de Mounier. Tu masz mieszek, daj mu to w zapłacie. Nikt nie zapyta... ale jeśli, nie mów, że to moje polecenie.

## Następca
** Postacie:** Liam, Sloan, Irvette, Drummond, Kylie

Chwilę później


Wielka sala rzadko była miejscem spotkań rodziny, przynajmniej w dawnych czasach w których Maksymilian mieszkał tutaj. Duży stół ustawiony był na środku, tworząc idealną linię z tronem stojącym głębiej w sali na podwyższeniu. Przy stole, u szczytu siedział ojciec. Ubrany był w purpurową szatę obszytą złotą nicią. Ubiór na okazje z rodzaju koronacji czy przekazania władzy. Po jego lewej siedziała Jaśnie Pani Matka, piękna pomimo wieku i milcząca jak zawsze, ubrana również w zdobione szaty, z wzorami roślinnymi na ramionach i przy dekolcie..

Znaczące było puste miejsce po prawej stronie, zarezerwowane zwykle dla jego brata. Ten zaś siedział obok matki, na przeciwko jego zaś Irvetta. Hrabia schodząc po schodach, nie mógł widzieć zaciśniętych ust bratowej i jej bladej twarzy. Jednak niepokój matki był dostrzegalny kiedy hrabia dotarł do stołu.



\-   Witaj synu! - Głośno powiedział, prawie krzyknął ojciec, który zerwał się chwilę wcześniej ze swojego miejsca, szybkim krokiem podszedł by najwyraźniej uściskać Ilama.



Drummond de Falco pomimo, że nie tylko ze względu na prawo faworyzował starszego brata, to zawsze okazywał sympatię do młodszego, nawet gdy wiele lat temu, gdy przedstawił mu pomysł przeprowadzki na Pogranicze. Nawet wtedy stwierdził, że czuje żal i wyrzuty sumienia z powodu jego wyjazdu.



Teraz jednak nie widać tam było niczego, oprócz pustego spojrzenia człowieka, który stracił wszelką nadzieję.



\-   Zasiądziesz po mojej prawicy? Pewnie masz dużo pytań! - Ciągle mówił za głośno, jakby był przygłuchy. Dotknął bark Hrabiego i szerokim gestem wskazał omówione miejsce. Jego ruchom brakowało dawnej sprężystości, którą tak dobrze pamiętał ze wspólnych polowań Maksymilian.



Maksimiliam przystanął opierając dłoń na inkrustowanej, srebrnej laseczce, której rękojeść zdobił fragment rzeźbionego, jeleniego poroża, jak na łowcę przystało.



\-   Jaśniepaństwo, ojcze, matko. - Sokół skłonił im się z szacunkiem. - Rad jestem was widzieć... dziękuję, ojcze, to dla mnie zaszczyt. - Zapewnił w odpowiedzi na zaproponowane miejsce, poczekał aż głowa rodu i włodarz zasiądzie na swoim miejscu dopiero wtedy spoczął. - Bracie, bratowo, jestem niezwykle rad, mogąc usiąść z wami do wspólnego stołu. Oczywiście mam wiele pytań, do was wszystkich, jeśli mógłbym, tym bardziej, że nie znam celu naszego spotkania. Co prawda goniec do mnie dotarł, lecz\... nieroztropność mojej służki sprawiły, że cały atrament uległ zniszczeniu. Zasugerowany jednak pieczęcią nadawcy przybyłem najrychlej jak to było możliwe.



Każdy ruch hrabiego był ostrożny, a słowa wyważone. Kiwnął też na służbę, aby przyniosła skrzyneczki i pudełka.



\-   Najpierw jednak uczyńcie mi ten zaszczyt i pozwólcie się obdarować upominkami z dalekiego Pogranicza.



Napięcie wiszące w powietrzu było wręcz namacalne. Sloan miał kamienną twarz, Irvette wyglądała na bardzo niepewną co do sytuacji w której się wszyscy znajdowali, Kylie de Falco, z rodu d'Mounier siedziała zaś niezwykle sztywno i wpatrzona była gdzieś w ścianę. Ojciec wydawał się żywo zainteresowany, ale wrażenie było jakby odgrywał tylko swoją rolę i to w niewprawny sposób. 🔹Ze strony Maksimiliama to też była wyłącznie kurtuazja, ale uważał, że nie powinien uchybić żadnym dworskim gestom. Toteż podarkiem dla ojca była srokata skórka, dobrze wyprawiona o niecodziennym i zupełnie niespotykanym u tutejszych zwierząt umaszczeniu. Matce podarował natomiast kościany grzebyk ozdobny, z bursztynami, których okrągłe oczy wyglądały jak żarzący się ogień, gdyż każdy wiedział, że Pani pomimo starszego już wieku odznacza się niezwykłą urodą, a jej włosy są tego wizerunku nieodzownym elementem. Brat otrzymał skórzany, trawiony kołczan i starannie wytworzone i wyważone strzały o jasnożółtych, dobrze widocznych w zaroślach lotkach. Irvette natomiast po otworzeniu kuferka zobaczyła trzy egzotyczne rośliny w szczelnych słoiczkach, w których wilgotność była na wystarczająco dużym poziomie, aby wytworzył się mikroklimat.

🔹

Gdy prezenty trafiły przed obdarowywanymi, tylko ojciec wziął na chwilę swój do ręki i chwilę oglądał.



\-   Maksymilianie, Twój czas na wygnaniu się zakończył. Pięć lat spędziłeś poza domem, będąc posłuszny mojemu poleceniu. Teraz za to czeka Cię nagroda...
\-   OGŁASZAM CIĘ PANEM NA TWIERDZY, MOIM NASTĘPCĄ - Oznajmij głośnym głosem, który odbijał się echem po sali, po czym wyraźnie zaakcentował każde kolejne słowo - I DWUDZIESTYM PIĄTYM WŁADCĄ ZAMKU GORĄCEJ SKAŁY!



Gorąca skała była nazwą twierdzy sprzed czasów Cesarstwa, a która nazwa została zakazana po podbiciu tego miejsca 3 wieki temu.



\-   🔹Ojcze, skąd ta decyzja? Czyż nie przygotowywałeś Sloana całe jego życie do tego, aby był znamienitym władcą? Ja jedynie mogę podziwiać jego przymioty - skinął głową w stronę brata, udając, iż jest to normalna konwersacja. W istocie nie interesowało na chwilę obecną Liama co myśli Sloan, bacznie natomiast obserwował ojca i matkę.



Jednak największa rzecz warta uwagi wydarzyła się ze strony Irvetty. Siedziała z pochyloną głową, jakby ukrywała łzy i chwyciła go za rękę, szepcząc- Błagam, nie zgadzaj się...



\-   Kiedy wysyłałem Cię na pogranicze, nie pytałeś dlaczego, tylko posłusznie pojechałeś. Nagle przestałeś się słuchać swojego Ojca i próbujesz podważyć moją decyzję, jak niektórzy z tu zebranych!? - Mówiąc to, spojrzał na Matkę i Syna.



\-   🔹Wybacz ojcze, że się ośmielam... wcześniej rozumiałem tę decyzję, gdyż spodziewałem się jej w takiej czy innej formie. Mój Starszy Brat bowiem był przygotowywany od naszych najmłodszych lat do tego, aby odziedziczyć pobra i obowiązki po tobie, jaśniepanie ojcze. Nikt nie chce kwestionować twych decyzji, tego jestem pewien, jednak nie chciałbym równie mocno niesnasek między mną a Sloanem... któremu wolałbym służyć wsparciem i radą.

🔹

\-   A może ośmielisz się przyjąć władzę, co? - Twarz ojca była wykrzywiona w grymasie. - Moze jeżeli stracisz tą swoją twierdzę daleko stąd, przestaniesz myśleć...
\-   Przestań! - Krzyknęła nagle Kylie de Falco i obdarzyła męża siarczystym policzkiem. Znana była ze swojej porywczości w młodości, ale obydwoje pomimo małżeństwa politycznego świetnie się ze sobą czuli. Jednak ten stan musiał od jakiegoś czasu nie być taki jak dawniej. - Nie wiem co w Ciebie wstąpiło, myślałam, że ciężko przeżywasz oddanie władzy w ręce syna, ale tego już za wiele. Maksymilianie, Tobie jednak powiem coś, co Ci się nie spodoba. Sloan miał być władcą nie tylko dlatego, że jest moim pierworodnym, ale dlatego, że Ty strach przekułeś na bycie posłusznym.  - Jej głos był jak ostrze, które przecina tą zatrutą atmosferę, by zadać jednak raz za razem.



Wszyscy zamarli, łącznie z ojcem, który z władcy stał się nagle wstrząśniętym człowiekiem, a w jego oczach pojawiły się już inne emocje, wskazującymi na to, że mieszkają w nim jeszcze inne emocje.



\-   W takim... w takim razie przepraszam... - Powiedział ojciec wstając. - Ja muszę się przejść. - Mówiąc to ruszył w kierunku drzwi.



🔹Sokół wstał, gdy wstał władca Sterling i odprowadził go wzrokiem, chociaż nawet przez myśl nie przeszło mu, aby zatrzymywać ojca. Po jego wyjściu z kamiennej sali atmosfera tylko nieznacznie zelżała, za to zapanowała niezręczna cisza. Przerwać ją postanowił jednak sam Maksimiliam nie zrażony przytykiem.

🔹

\-   Dziękuję, Matko, za to cenne pouczenie. Zapewniam, że przemyślę tą uwagę, jednak pozwól, że skupimy się na włodarzu, bowiem mam wrażenie, że umknęło nie tylko mnie... coś bardzo istotnego, a mianowicie stan zdrowia głowy naszej rodziny. - Podniósł wzrok na matkę, później przesunął go na brata. - Nigdy nie chciałem brać na swoje barki tego brzemienia, jakim jest władanie naszymi ziemiami, wybaczcie jeśli do tej pory nie padły z moich ust te słowa wystarczająco jasno. Znam się na łowach, sokolnictwie i chartach, nigdy\...dotąd... nie uskarżałem się również na braki w dyplomacji. Natomiast, jak jeszcze raz podkreślę  n i e s t e t y  {.c18 .c8 .c19 .c31}🔹 nie poznałem powodu dla którego pragnęliście, lub też sam ojciec, mnie zobaczyć. Sytuacja, która zaistniała jest dla mnie niezrozumiała i wprawia w konsternację. Nie wiem co tu się dzieje, po minie bratowej oraz brata mego, wnioskuję, iż oni także nie do końca orientują się w tej irracjonalnej sytuacji. Matko, bądź zatem łaskawa wyjawić nam o co właściwie chodzi? No, chyba że wolisz widzieć na tronie dwudziestego piątego władcy tchórza? Tu padła z twych ust parafraza tegoż, aczkolwiek nie ukrywajmy, że to chciałaś powiedzieć. Zatem? Co tu się, na bogów, dzieje?
\-   Chodźmy stąd. - Odezwał się wreszcie milczący do tej pory Sloan. - Służba już dosyć się nasłuchała, udajmy się w nieco lepsze miejsce do takiej rozmowy.


## Dawny pokój skrywa tajemnice
** Postacie:** Sloan, Liam, Kylie
Chwilę później



\-   Zastanawiałeś się dlaczego nie zostałeś zaproszony do swojego pokoju prawda? - Zapytał Sloan Maksymiliana, kiedy to stali w rzeczonym miejscu, patrząc na dziurę w podłodzę wraz z liną pokrytą więzami, okręconą na kołowrocie, umożliwiający zejście na dół. Aby tu wejść Sloan użył siekiery, aby zniszczyć zamknięte na klucz drzwi które tutaj prowadziły.
\-   Po twoim odejściu Maksymilianie - zaczęła opowieść Matka - Drummond zainteresował się przeszłością tego miejsca. Chyba to, że stracił równego miłośnika do polowań co on sprawiło, że musiał zabrać się za coś innego, bo samemu nie lubił tego robić.
\-   A ja nie lubiłem tego - Wtrącił brat.
\-   Coś musiało stać się tutaj, miesiąc temu, kiedy dowiedział się, że pod twoją komnatą coś się znajduje. Zszedł tam ze swoim wiernym sługą, Kardem. Kard jest teraz w złym stanie, ale żyje, tylko kontakt z nim jest ciężki. Irvetta się nim opiekuje. - Irvetta skineła głową.
\-   Tak, nic sensownego nie powiedział jednak, mówi ciągle o cieniach pod pałacem...
\-   A mój jaśnie małżonek od tego czasu przestał ze mną rozmawiać, przestał dzielić ze mną łoże. Pomyślałabym, że kogoś sobie przygruchał... - Spojrzała na nich gniewnie w odpowiedzi na ich reakcję - Myślicie że jestem ślepa albo głucha? Ale nie, siedzi ciągle w księgach i widać, że coś go pożerało. Myślałam, że to minie, że jak wyznaczy dziedzica to mu przejdzie. Jak powiedział tuż przed spotkaniem, że mianuje Ciebie, to byłam wściekła, ale lepsze to niż stracić go na zawsze...



🔹Gdy zobaczyli wyrwę w skale, której dno co prawda było widoczne, ale wszystko wokół nikło w mroku, przenieśli do bardziej przytulnego miejsca, aby pomówić. Wyszli na dziedziniec skąd roztaczał się piękny widok na góry, których kształt był ostry i wyraźny, zachodzące słońce zabarwiło niebo na kolor złota i fioletu. Hrabia Sokół oparł się o piękną, zdobioną laskę.

\-   🔹Muszę to przemyśleć. - Odparł lakonicznie, podsumowując całą sytuację. Marszcząc czoło zastanawiał się usilnie i próbował odnaleźć sens w tym, co się wydarzyło. - Też, być może, powinienem zajrzeć do ksiąg. Sloanie, przynieś do moich komnat, proszę te książki, w których zaczytywał się ojciec, poza tym, musimy porozmawiać. Irvette, do ciebie też mam prośbę. Skoro opiekujesz się Kardem, to spróbuj delikatnie wypytać go o wyprawę do podziemi. Nawet jeśli wydadzą ci się jego słowa irracjonalne i dziwne, nawet jeśli będą to pojedyncze słowa, to spróbuj je zapamiętać. Matko, ciebie też muszę o coś prosić... spróbuj uspokoić ojca, poleć przygotować służbie jakiś wywar na uspokojenie i miej nad nim pieczę, dobrze? Wierzę, że równie mocno jak ja, nie chcecie abym został włodarzem naszych rodowych ziem, więc bądźcie tak łaskawi i spełnijcie moją prośbę.

🔹

Kylie miała się właśnie odezwać a po jej wyrazie twarzy, pewnie było to coś w rodzaju stwierdzenia, że nie będzie słuchała rozkazów od kogoś takiego jak on, ale wtedy odezwał Sloan.

\-   Matko, proszę. Nie dość, że Liam przeciwstawił się ojcu, to jeszcze i mnie, odrzucając moją zachętę do tego, aby wprost odmówił ojcu. Ma świeże spojrzenie, niech działa i mówi nam co robić, bo... Widzisz bracie, jest jeszcze jeden problem. Mamy może dwa tygodnie do momentu, w którym przyjedzie do nas posłaniec Cesarza. Nie wiem czego to dotyczy, bo przekazanie mi panowania nad ziemiami nie było oficjalnie zapowiedziane ani spodziewane. Więc albo to jakiś dziwny wyraz szaleństwa ojca, albo mamy tutaj na zamku kogoś kto szpieguje.
\-   Nie przesadzasz mężu? - odezwała się Irvetta - mamy potężny problem, o którym mi nawet nie raczyłeś powiedzieć, uspakając że przecież nic się nie dzieje, że wszystko masz pod kontrolą. No i nie masz. Co do Karda, możesz poczuwać ze mną noc czy dwa przy nim, bo majaczy głównie w nocy, mówiąc że boi się ciemności. Oczywiście może nie tej, bo musisz wypocząć...

Sloan nie skomentował, tylko rzucił gromy spojrzeniem na żonę, która podważała jego kompetencję publicznie, chociaż na publikę składała się tylko najbliższa rodziny.

\-   Matko, uspokój nieco ojca, bo jak będzie taki obląkany\...

Oczy Sloana i Kylie spotkały się i rozszerzyły w przerażeniu.

\-   Na bogów, przecież w obliczu szaleństwa głowy rodu - wyszeptała Matka - to cesarz ma prawo ingerencji w wyboru następcy\...

Obydwoje spojrzeli się badawczo na hrabiego, na co Irvetta zbladła.

\-   Nie, nie, o czym wy w ogóle myślicie! Mój mąż ma zostać kolejnym władcą. Oszaleliście?

🔹Maksimilisma potrząsnął głową, patrząc na Irvettę, później Brata i Matkę.

\-   🔹O czym pomyśleliście, na bogów?
\-   Musimy rozważyć... - Powiedziała powoli Kylie - że w takiej sytuacji może...
\-   NIE! - Krzyknęła Irvetta - Liam nie może przyjąć tego tytułu!
\-   Dziękuje moja żono, że tak bardzo chcesz, abym został władcą... - Powiedział Sloan - Ale Ojciec naznaczył Cię na następcę, a teraz właściwie musiałbyś się tylko zgodzić i uczestniczyć w oficjalnej ceremonii.

Cała trójka zamilkła. Irvetta tylko lekko kręciła głową i wytrzeszczyła oczy, tak aby inni nie zauważyli ale żeby Maksymilian zrozumiał o co jej chodzi.

\-   🔹Po co to wszystko? - Westchnął zmęczony, przypominając sobie z całą mocą, dlaczego "wygnanie" na Pogranicze przyjął z taką ulgą. - Jeśli myślicie, a zapewne nie, bowiem jest to irracjonalne, że mamy odgrywać tą pokrętną grę, aby podkreślić szaleństwo ojca i ukazać jak złe wybory podejmuje... to możecie się jeszcze srogo zdziwić. Nie jestem waszym wrogiem, chciałbym, abyście postarali się o tym pamiętać. Jestem po bardzo długiej podróży, więc pozwólcie, że odpocznę. Matko, Bracie. Irvetto. - Skłonił się im i poszedł do wewnętrznego ogrodu, nie oglądając się za siebie.

🔹

## D'oh
** Postacie:** Liam, D'oh

Co Panicz taki zamyślony co? - Charkliwy głos treserki, mającej co najmniej ćwierć krwi orka, nie można było pomylić z nikim innym. - Lepiej zaraz mi powiedz, jak szkoli się harty i sokoły do współpracy, bo zaraz wszystkiego co ważne zapomnisz gamoniu Ty! 

Hrabia wiedział, że w jej ustach było to pieszczotliwe określenie, bo gdyby chciała obrazić to znała wiele sposób na ludzki i nie mało na orczy sposób.



D'oh była wszędzie indziej ciekawostką, ale tutaj jak potrafisz gołymi rękami zabić atakującego cię wilka, to obdarzają cię strachliwą akceptacją, ale niczym więcej. Dopiero zabicie gołymi rękami wilka w obronie syna lokalnego władcy powoduje, że ktoś cię nie tylko zauważa, nie tylko docenia Twoje umiejętności, ale też swoją uwagą, awansuje cię społecznie o wiele więcej poziomów. Nadal jednak treserka czuła się najlepiej wśród zwierząt, robiąc mały wyjątek dla rodziny władcy oraz duży wyjątek dla gamonia, czy tak jak woli Maksymilajajajanina.



Jednak dostosowała się i wcześniej i teraz do tego, gdzie przebywa, czyli nosiła się całkowicie po ludzku, ale zapach zdradzał, że jej sypialnia jest tam, gdzie jej podopiecznych. Można by narzekać, ale hej! Nikt chyba nie miał tak dobrze ułożonych zwierząt jak de Falco, o samych Sokołach już nie mówiąc. Była tak dobra i znana z tego, że kiedy ojciec hrabiego proponował komuś sprzedanie zwierzęcia, nie musiał nigdy zniżyć się do targowania za nie, bo chętnych było znacznie więcej niż możliwości ich zadowolenia.



D'oh żałowała, że hrabia nie zabrał jej ze sobą. Nie zgodził się na to jednak na to Drummond, który czerpał niemały dochód z hodowli zwierząt.

🔹

\-   🔹Tego nie zapomnę, D'oh. Wierz mi, na całym Pograniczu nie ma lepiej wytresowanych charów i bystrzejrzych sokołów niż w mojej twierdzy, chociaż lokalna szlachta nie podziela mojego zamiłowania do łowów, to cieszę się tam z owego powodu odpowiednią estymą.. - Odparł, słysząc najpierw jej głos, dopiero później odwracając się w jej stronę.

🔹

🔹Widok jej osoby, wbrew wszystkim instynktom, wywoływał uśmiech na twarzy Maksa i spokój ducha w jego sercu. Słońce wstało niedawno, ale Maks był już na nogach, jakby świt wyczuwał podskórnie i nie mógł odmówić sobie tej rozkoszy, jaką było przywitanie pierwszych promieni dnia.

\-   🔹Dobrze ci się tu żyje, D'oh?

🔹

Stała w szarym męskim stroju całym we wzorze psiego zęba, bo owszem przyjęła zasady ludzi, ale żeby jeszcze ciągle zmieniać ubrania? "Po ich trupach" jak to mawiała w takich sytuacjach, dopasowując do siebie nie tylko ubiór ale i powiedzenia. Stała jednak o dziwo też na mocnym kosturze, którego na pewno nie miała 5 lat temu.

\-   Co się gapisz, no starzeje się. Już by mnie coś zjadło w dziczy, ale tutaj to wygoda! Wygoda że hej. Jakby ktoś chciał dopaść Ciebie lub Pana, to była bym pierwsza do obrony, bo łatwo się zapomina! Zapomina o tym, że ktoś Ci tyle dał i dupsko obrasta! Obrasta w pierzynkę i już w dupie się od tego przewraca. Czyli dobrze, dobrze! Dobrze się tutaj żyje i słabo się umrze, po cichu albo w drodze do wychodka.



\-   Albo w walce. Nigdy nie wiadomo. - Rzucił, pół żartem, pół serio Maksimiliam. Uśmiechając się przy tym nad wyraz smutno, ale aby nie wprowadzać ponurej atmosfery, od razu zmienił nieco temat. - Za to widzę, że obydwojgu nam przyszło już wspierać się o lasce.



Słońce powoli, leniwie podnosiło się nad szczyty gór, a długie promienie słońca rzucały blask na korony drzew i złociły śnieg zalegający na ziemi, wtedy też Maksimiliam dojrzał kogoś znikającego wśród konarów drzew, przeprosił zatem D'oh, obiecując, że odwiedzi ją w psiarni, zabrał konia i pognał w tę stronę, w którą jak mu się wydawało, wybrała się owa osoba.



## Chatka na uboczu
*Postacie:* Liam, Irvette


Tutejsze lasy, z małym wyjątkiem, są naprawdę piękne, niezależnie od pory roku. Zimą zyskiwały jednak na pozornej ciszy - wszystko dlatego, że osoba poruszająca się po śniegu bez rakiet śnieżnych po prostu wydawała masę dźwięków na które wyczulona była miejscowa fauna.



Ślady kopyt prowadziły prowadziły do dobrze znanej hrabiemu chatki z której korzystał czasem on lub ojciec gdy nie chciał przebywać na osobności podczas wypraw na polowanie. Koń hrabiego cicho zmierzał w tamtym kierunku, zaś hrabia miał czas do rozmyślań i podziwiania widoku, który stracił kiedyś.



Na tej wysokości las był nadal bujny i okazały, obfitujący w zwierzynę o każdej porze. Zastanawiające było tylko to, że zające nie stały tak daleko jak dawniej, gdy polowali tutaj z ojcem. Może naprawdę od tego czasu nie było tutaj zbyt dużo polowań i zwierzęta już nie były tak trudną zdobyczą jak kiedyś. Jednak zające nigdy nie były zbyt sprytnymi zwierzętami, więc może nic to nie znaczyło.



Dojazd do chaty był widocznym miejscem, ale sama chata skrywała się w lesie i jedynie nieosuszone drewno mogło dymem zdradzić, że ktoś tutaj jest. I tak właśnie wyglądało to teraz.



Hrabia uwiązał konia, lecz zatrzymał się przed drzwiami, poprawiając zarzucony tylko na jedno ramię kołczan. Zawahał się, gdy jego dłoń spoczęła na klamce, w końcu to nie było najście, ale z drugiej strony był też u siebie, więc po prostu zastukał i wszedł do pomieszczenia.



W środku było cieplej niż na zewnątrz, ale dopóki nie rozpali się paleniska na środku chaty, nie można liczyć na na lepsze warunki. Panowałą tutaj ciemność, bo chata posiadała małe okna, pozwalające na tyle, by ocenić czy mieszkańców zasypało, jak to zgryźliwie mawiał czasem ojciec. Czuć było w powietrzu dym, który wgryzł się w drewno z którego zbudowane było całe pomieszczenie.



Na brzegu jednego z dwóch łóżek siedziała Irvette i płakała.



\-   Znalazłaś swoją samotnię. - Stwierdził Liam wchodząc głębiej, musiał chwilę odczekać, aby wzrok przywykł mu do panującego w chacie półmroku. Laskę oparł w kącie przy drzwiach i podszedł do kobiety, usiadł obok niej, i objął ją ramieniem, aby mogła się wypłakać. Wiatr na zewnątrz gwizdał przeciągle, co też wznieciło małe tumany śniegu raz za razem.



\-   Chciałabym żeby Sloan umarł. Czy to znaczy, że jestem złą osobą? - Mówiła, szlochając w ramionach Maksymiliana. - On jest zupełnie inny niż ty, jakby zupełnie nie interesował się tym, co mam wewnątrz...



Maksimiliam westchnął, i pogładził ją po włosach uspokajająco.

\-   Nie jesteś złą osobą, Irvette. Tego jestem pewien. Ta cała złość... bezsilność, którą czujesz, to nie jest twoja wina. Myślę, że nawet on czuje obecnie się zagubiony.
\-   Pięć pieprzonych lat nie miała się komu wyżalić i ciągle musiałam sobie wmawiać, że muszę go wspierać, bo mu ciężko. Odkąd ciebie tutaj nie ma jest mi cholernie ciężko i nikt mnie nie wspiera. Więc jak masz dalej opowiadać takie rzeczy, to może idź już sobie! - Czerwona od łez i nagłego przypływu złości nie wyglądała jak słodka i piękna żona, tylko jak ktoś, kto zrzucił właśnie maskę i pokazuję nagromadzoną, wieloletnią frustracje.
\-   Rozumiem, Irvette. Przepraszam. Nie chciałem bronić mego brata, sam jestem na niego niepomiernie wściekły. Przepraszam też, że zostałaś sama z każdym problemem,wyobrażam sobie jak musiało ci być tutaj ciężko.
\-   To jak ci tam jest na pograniczu? Kłopoty z żoną?
\-   Nie bawisz się w półśrodki. - Zaśmiał się zaskoczony hrabia, zupełnie odzwyczajony od tak bezpośrednich pytań dotyczących w gruncie rzeczy dosyć osobistych spraw. - To urocza, pełna naturalności młoda kobieta, odważna, o ogromnym temperamencie, zapale do działania... ale tak - westchnął, jakby po prostu rezygnując z pozytywnej narracji - jest moim największym utrapieniem. 



Był człowiekiem, który widział w ludziach więcej dobrego niż złego, zawsze znajdywał w głowie usprawiedliwienie na czyny i motywacje innych, ale czasem odwołując się do swoich własnych uczuć, nie mógł nie powiedzieć prawdy.

\-   Nie musimy rozmawiać o mnie, jeszcze się o mnie przez najbliższe tygodnie sporo nasłuchasz - zauważył uprzejmie. Odpiął swoją podbitą lisim futerkiem, pelerynę i okrył nią Irvettę, sam wstał, aby rozpalić palenisko, i uchylić daszek, który blokował przepustowość komina. Już po chwili miało zrobić się jaśniej i cieplej.
\-   Jesteś z nią szczęśliwy, czy nie? Sam ją wybrałeś, czy ktoś Cię zmusił do tego? I tak, chcę rozmawiać o tobie, bo mam dosyć tego wszystkiego co tutaj się dzieje. No oprócz wewnętrznego ogrodu.



Liam ustawił drwa z wielką pieczołowitością i wśród suchych gałązek rozniecił żar, smużki dymu poszybowały ku górze. Nieśmiały, nieregularny blask ognia zaczął się stopniowo powiększać.

\-   Przyjąłem propozycję tamtejszego szlachcica, droga moja Irvette, który to swą bratanicę pragnął dobrze wydać za mąż, a ja uznałem słuszną myśl o ustatkowaniu się i zawarciu nowych przyjaźni na Pograniczu. Jego propozycja była dobra pod względem politycznym i... wymiernym. Prianka w posagu wniosła parę wsi i inwentarz, kilka wartościowych kontaktów, poza tym przyniosła mi więcej trosk, niż radości. Nie doczekałem się także dziedzica przez te wszystkie lata, nad czym najbardziej ubolewam.
\-   Myślę, że to nie wina twoja - powiedziała i obiema dłońmi ujęła jego rękę. Jej oczy wyrażały smutek, troskę, nadzieję i coś jeszcze, coś jak uwielbienie drugiej osoby.

<!-- -->

\-   Ja też byłam cennym kąskiem. Dla wszystkich, tylko nie dla ciebie.
\-   Irvette... - Maksimiliam zaczął przepraszającym tonem, ale nie uciekł wzrokiem. - Nie mogłem ci nic zaoferować, wiesz o tym. Wybacz, jeśli dawałem ci złudną nadzieję, że nasza relacja mogła rozwinąć się inaczej, nie było to moją intencją. Nie przybyłaś na nasz dwór, aby ofiarować rękę komuś bez ziemi i bez perspektywy na objęcie władzy. Nie było nam pisane nic więcej, wiem, że to rozumiesz. - Podniósł jej dłonie do swoich ust i ucałował. 
\-   Ja nigdy nie chciałam ani ziemi, ani władzy. Ja tylko chciałam Ciebie, choćby przez moment chciałabym mieć Ciebie za coś więcej niż przyjaciela. Choćby ten moment miał być w jakiejś zimnej chacie.
\-   Nie tylko ciebie nikt nie pytał o to, czego pragniesz\... lecz powiedz szczerze, wystarczy ci tylko ta chwila? Bowiem faktem jest, że nadal mam żonę na Pograniczu, ale to może ulec zmianie. 

Irvette była w pierwszej sekundzie zszokowana, niczym dziecko otrzymujące niespodziewany prezent, przechodzące przez moment niedowierzania zmieszany z wielką ilością hamowanej jeszcze radości, która z sekundy na sekundę wzrasta coraz bardziej.

\-   Liamie, drogi Liamie... Czy ty chcesz wstrząsnąć światem aby być ze mną?
\-   Rozwiodę się z Prianką, a ty będziesz pierwszą niewiastą o której rękę poproszę. - Zapewnił z całą swoją szczerością. Oczywiście ciągle mając przed oczyma parę całkiem dużych ku temu przeszkód. - Będę wówczas liczył na twą przychylność, Irvette - Pochylił się tym razem, całując wnętrza jej otwartych dłoni. - Jednak nim ten dzień nastąpi, muszę zająć się kwestią elekcji. To, jak rozumiesz, obecnie niezwykle paląca sprawa. 



Opuścił jej dłonie delikatnie i wstając sięgnął po pelerynę. W chacie było już jaśniej i cieplej, blask ognia oświetlał okazałe poroża zdobiące ściany i plecione arrasy. Srebrne futra wyścielały podłogę i niektóre ze skrzyń, a sama przestrzeń nie wyglądała już na tak ponurą.



\-   Chciałbym wiedzieć co sądzisz o poczynaniach jaśniepana mego ojca, a także o tym, co być może planuje mój brat?



Irvette chwilę milczała, ale zanim hrabia ją poprosił o jakikolwiek pośpiech, powiedziała cicho patrząc mu w oczy z wielkim uczuciem, pełnym lęku ale i troski o niego.

\-   Mój kochany... kłamałam, kiedy mówiłam, że Kord majaczy bez sensu. Tylko ja się nim opiekowałam i tylko ja wiem o tym, co odkrył ojciec w jakiś księgach pod zamkiem. Kord nie potrafi czytać, ale wiedział co wykrzykiwał ojciec "Mojemu następcy pisana jest śmierć! Pisana jest śmierć!". Tam było jednak coś więcej niż księgi, były też dziwne przedmioty, mikstury. Jedną z nich Kord stłukł i czymś się chyba zatruł. Możliwe, że ojciec też. Jak wyszedł, to zaczął podważać kompetencje mojego męża a Twojego brata. Nie od razu, ale nagle stał się dla niego bardzo surowy i wytykał mu wszystkie braki. Ma je, owszem... Ale nie tyle, ile chciałby wmówić otoczeniu ojciec.



Liam skinął powoli głową. Wyglądał na mniej zaskoczony, niż można się było spodziewać po takich wieściach.

\-   Dziękuję, Irvette... to ma wiele sensu... - odparł, bardzo powoli zapinając klamrę peleryny, z nieco nieobecnym wzrokiem utkwionym gdzieś w przestrzeni. - Dobrze. - Maksimiliam sięgnął po laskę i położył dłoń na klamce. - Irvetto, wiedz, że ty i twoja przychylność jest najlepszym, co mnie w Sterling spotyka. Dbaj o siebie i bądź ostrożna, za tydzień przybywa wysłannik cesarski, do tego czasu zachowaj szczególną czujność, proszę. - Skłonił się jej i wyszedł, było jeszcze parę rzeczy, które musiał sprawdzić, parę osób z którymi musiał pomówić, a cała masa rzeczy, które musiał dobrze przemyśleć.

        

## W głębi ksiąg
*Postacie:* Sloan, Liam



Gdy wszedł Sloan, Maks siedział na podłodze wśród rozłożonych ksiąg.

\-   Dziękuję, że zgodziłeś się mi pomóc, bracie. To szaleństwo musi mieć jakieś źródło.

Stwierdził Maksimiliam Te księgi, które przyniósł Sloan także mogły okazać się pomocne. Komnaty, które zajmował Liam były obecnie łożem i składzikiem literatury, jasne lampy i żarzące się kryształy pozwalały mu na studiowanie do późna, część ksiąg odłożył i zamknął w kufrze, część natomiast ciągle kartkował. Lekturę młodszy z braci de Falco umilał sobie winem, nie stronił od dobrego alkoholu, a książka i lampka wina pasowały do siebie idealnie, toteż wstał i z butelki stojącej na paprapecie napełnił drugi kielich i podał bratu.

Sloan nie był typem który gardzi książkami, ale zdecydowanie wolał pozostawić innym ślęczenie nad księgami. Był człowiekiem czynu, który pewnie świetnie odnalazłby się w czasach wojny albo wielkiego kryzysu. Przywlókł stosy książek samodzielnie i po trzecim kursie oklapł na fotelu zasapany mówiąc

\-   Naprawdę, mam nadzieję, że znajdziesz coś w tym księgach rodowych. - powiedział, biorąc do ręki kielich i łapczywie z niego pijąc. - To co, mam zaglądać też do tych ksiąg i czegoś tam szukać? I właściwie, to czego?

Opuścił swobodnie rękę z kielichem i odchylił głowę na wezgłowiu, patrząc się na sufit.

\-   Jeszcze nie wiem bracie, być może Jaśnie Ojciec wraz ze swym sługą podczas eksploracji nawdychali się szkodliwych oparów, wydzielanych przez jakąś roślinę, lub rodzaj grzybów. Może ujrzeli coś przerażającego, cmentarzysko na przykład.... - snuł domysły Liam i sięgnął po swój kielich z którego upił odrobinę. - Na razie to tylko domysły, a moją roboczą hipotezą są właśnie szkodliwe opary, dlatego szukam w zielnikach, nawet w baśniach, w końcu znajdę jakiś trop.

Przez chwilę milczeli, syczały knoty w kaganku i dało się jedynie słyszeć szelest przerzucanych powoli stronnic. Ciszę przerwał Maksimiliam, opierając się plecami o skrzynię i spoglądając na brata.

\-   Zauważanie posunąłeś się wiekiem przez te ostatnie lata, Sloanie. Wyglądasz na człowieka o zbyt wielu zmartwieniach.
\-   Co się tak mną tak martwisz? Chcesz może zaproponować ściągnięcia brzemienia władzy z moich barków? Cóż, pomyślmy... - dumał ciągle w tej samej pozycji - Irvette dwa razy poroniła, więc na razie brak mi potomka. Już sporo rzeczy jest na moich barkach. Mamy lokalny problem z orkami, zima za długo już trwa, przydało by się wzmocnić mury ale nie ma za co, bo nałożony jest podatek wojenny psia mać! Co nas obchodzą wojny innych, w dodatku na podbój a nie na obronę! Dewarowie otworzyli drugi szyb wydobycia srebra i zalewają nim rynek przez co nasz majątek na gorsze czasy jest mniej warty a dodatkowo przez to stali się pyskaci, wynajmują żołdaków i opłacają sędziów, aby rozwiązywali spory na ich korzyść. Na razie kiepsko im to idzie. Może to z tego powodu przybywa tutaj wysłannik cesarski, w poszukiwaniu pieniędzy? Był mały pomór kóz, ale zaradziliśmy temu chociaż w jednym momencie wyglądało to kiepsko...
\-   Martwię się, bowiem jesteś moim bratem, nawet jeśli tym najmniej bystrym z całego miotu. - Odparł Maksimiliam, topiąc ironiczny uśmiech w kolejnym łyku wina. - Powtórzę się zatem. Nie, nie chcę zdjąć z twych ramion brzemienia, które dźwigasz. Zasłużyłeś na nie przez pierwszeństwo urodzenia, a ja w całej swej pokorze nie opowiem ci o moim majątku ziemskim i moich troskach. Nie atakuj mnie, gdy wyciągam do ciebie rękę. Naprawdę tak bardzo przeraża cię myśl, że to ja mógłbym zostać włodarzem rodzinnych ziem i twoim panem?
\-   Hahahahahahahaha - Zaśmiał się słysząc słowa które miały go uspokoić tak mocno, że aż upuścił kielich. - Liamie, Liamie. - Usiadł prosto, spojrzał się na brata. - Ja się dziwię jak Ty w ogóle władasz tam na pograniczu. Nie widziałeś tego, że ojciec wcale nie mnie chciał na następcę, bo znacznie Ciebie wolał? Tylko że ty byłeś na to ślepy, bawiłeś się z psami, tylko jeździłeś na polowania i tylko lekcje erudycji brałeś na poważnie. Ja natomiast, mniej lubiany syn, harowałem, żeby być godnym bycia następcą, skoro miałem być planem B, to takim, aby nie przyniósł wstydu. Ty natomiast pomimo, że najlepiej byłoby Cię wysłać na kapłana albo do wojska, wykpiłeś się i trafiłeś do miejsca, gdzie mogłeś polować do woli, bo ojciec miał tam interes do pilnowania.
\-   Jak zwykle źle oceniasz sytuację, ale nie sądziłem, iż pamięć może cię aż tak mocno zawodzić. - Zakołysał kielichem wpatrując się w jego, już niemal całkiem widoczne pod cieniutką warstwą wina, dno. - Ani przez chwilę nie byłem brany pod uwagę przy wyznaczaniu linii rodowych. "To mój syn, owoc mych lędźwi, dziedzic Sloan, a to... Maksimiliam." - Roześmiał się, hiperbolizując lub po prostu wymyślając słowa, włożone w ojcowskie usta. - Objąć rządy po ojcu mogłem w dwóch przypadkach, primo, gdybyś w swej niepomiernej głupocie utracił życie przedwcześnie, secundo, gdyby ojciec oszalał a ja pragnąłbym zarządzać stertą głazów i tuzinem sosen. Więc ten drugi przypadek występuje nam tu, i tylko połowicznie. Powinieneś odsunąć ojca od władzy, gdyż jest już niezdolnym do jej pełnienia. A przynajmniej do czasu pojawienia się wysłannika cesarskiego, pieczę nad rodem, niejako namiestniczo, powinna sprawować jaśniepani matka.
\-   Może pamiętam, może nie pamięta. Wszystko nagle wywraca się do górny nogami, jakby innych problemów nie było. Co do namiestnictwa - musielibyśmy mieć właśnie kogoś od cesarza, aby uznał go za niepoczytalnego i chciał to zrobić. Albo zebrać pozostałe rody z okolic i w wielkim sądzie tego dokonać. Pamiętasz Dewerów od srebra? Oni tylko czekają na coś takiego. Dalej, w tak krótkim czasie nikt się nie zbierze, żeby zdążyć przed wysłannikiem. I ostatnie, chyba najgorsze. Ojciec niby oszalał, ale on działa prawie tak jak zawsze. Tak jakby tylko chęć obsadzenia Cię na moim miejscu było tym jednym objawem szaleństwa. No i odsunięcie matki od siebie. Śpią osobno teraz, wiesz? Oni, papużk nie rozłączki.

Maksimiliam wstał niespiesznie i dolał starszemu bratu wina, po czym usiadł w drugim ze skórzanych foteli, przecierając kąciki oczu palcami.

\-   Możliwe, że rada nie zebrałaby się, albo li też widząc słabość naszego rodu, wolałaby podzielić ziemię, to kolejny problem, na równi z tym, że zawsze dostawałeś wszystko, co najlepsze, bracie. Do tego stopnia, przywykłeś do owego stanu rzeczy, że nie możesz znieść myśli, iż coś znajduje się nagle poza twoim zasięgiem. Jakie właściwie miejsce zajmują w twoim sercu ziemia rodowa, dobro nazwiska, Irvette, kopalnia srebra, sama władzy? O siebie nie zapytam. Co jest dla ciebie najważniejsze, a z czego mógłbyś zrezygnować?
\-   Cóż - Zaczął niepewnie Sloan, zmieszany chyba faktem, że zaśmiewał się z młodszego brata podczas gdy on zniósł to ze spokojem i wymyślał rozwiązania, podczas gdy on widział tylko problemy. - W nadzwyczajnych okolicznościach sukcesja mogłaby być natychmiastowa i bezdyskusyjna, o ile nie zostanie jakiś dokument ojca wskazujący na coś innego. Najoczywistsza jest śmierć...

Napił się nalanego świeżo wina i milczał chwilę myśląc.

\-   Kord by wiedział co zrobić, ale jest w stanie nie do użycia. Jest jednak osoba, która świetnie zna się na prawie i ty go dobrze znasz, bo to Javier. Ja ledwo kojarzę, że jest prawo które mówi, że w wypadku niezdolności władcy do jej sprawowania i sytuacji kryzysowej, osoba najbardziej do tego zdatna, pasująca czy tam będąca spodziewanym dziedzicem... nie pamiętam jak to szło. No taka pasująca osoba może zostać natychmiast następcą. Tylko że Liamie, to będzie straszny blamaż, bo ojciec będzie walczył z tym, trzeba by go uwięzić. Potem zaś ród Dewerów będzie miał już drugą nogę w sądach, jedną pieniężną a drugą naszą słabość. Gdyby tylko pozbawić ich chociaż części srebra. No i jak Matka na to zareaguje? Ona była bardziej skłonna ustawić cię władcą, pewnie po to aby ubezwłasnowolnić ojca a potem zrzec się praw na rzecz mnie. Lub jej. Wszystko po to, aby przeczekać ten okres. Co sądzisz?
\-   Porozmawiam o tym z Javierem, tego możesz być pewien. Niepokoi mnie jednak równie mocno wyrwa skalna, najchętniej wysłałbym tam małą ekspedycję, w tym mojego medyka, który zna się na florze i mógłby ustosunkować się do moich podejrzeń co do trujących właściwości porostów, grzybów, czy cóż tam mogło się zalęgnąć. Do tego czasu, i do mej rozmowy z Javierem chciałbym, abyś jasno zapewnił jaśniepanią matkę oraz ojca, że jesteś w stanie zaakceptować decyzję głowy rodu, a mnie wspierać w działaniach, jeśli zajdzie taka potrzeba. Chcę, aby był to twój gest zaufania, poczyniony w moją stronę. - Ostatnie zdanie powiedział wolno, ale dobitnie po czym sięgnął po karafkę stojącą na stoliczku nieopodal i dolał sobie wina do kielicha, czekając na reakcję brata.

Brat milczał, myśląc o tym, że Maksymilian albo zawsze był inni niż myślał, albo bardzo zmienił się przez te pięć lat. Nie przyznawał się do tego nawet przed samym sobą, ale bał się po raz pierwszy o swoją przyszłość, która nagle nie była tak pewna jak by tego chciał jako dotychczasowy następca, szlachcic czy wreszcie człowiek. Zapytał wreszcie.

\-   Powiedz, a czego chcesz w zamian za swoją pomoc? Bo właściwie jak zgodzisz się na zostanie następcą by potem przekazać władzę, to z czym właściwie zostajesz na końcu? - Ciągnął z nieufnością w głosie - Nasze relacje nie są już tak dobre jak kiedyś a i wtedy nie były zbyt dobre. Teraz jesteś jak do rany przyłóż. Gdzie tkwi haczyk?
\-   Jesteś moim bratem, nie chcę twojej krzywy, ani twojego nieszczęścia. Wystarczyło parę twych słów listownie przesłanych, abym wybrał się w długą podróż i przybył tu. Nie musimy się przyjaźnić, z resztą nigdy nie wychodziło nam to jakoś szczególnie dobrze, ale z bliskiej rodziny mamy tylko siebie... - dodał unosząc kielich do ust, w drugiej dłoni leniwie bawiąc się korkiem od karafki. - Chciałbym widzieć cię jako głowę naszego rodu, niepodważalnego włodarza tych ziem, a po wszystkim chciałbym zabrać z twego dworu parę osób, i wrócić do siebie. I nie musieć tu nigdy wracać. "Móc" ale już nie musieć. 
\-   No cóż, zatem zgoda! - Z poczuciem ulgi wstał z fotela, podszedł do Liamia i stuknął się z nim kielichem. - Za powodzenia planu!
\-   Za powodzenie. - Uśmiechnął się Maksimiliam, także stukając kielichem i upijając łyk. 



Sloan natomiast czuł zmęczenie i mięśnie wydawały się jak ze stali, chociaż wino, które wypił, miło odprężało to jednak przytępiało zmysły i władzę nad członkami ciała. Młodszy brat zauważając lekką niedyspozycję Sloana, skomentował to jakimiś mało istotnymi słowy, po czym pomógł bratu dojść do jego komnat, gdzie Irvette przeprosił za problem jaki jej czynią, a wszystko zrzucił na karby zmęczenia, trosk oraz wina. Noc zlała się ze świtem w jedną, lepką maź.  

## Przybywa Javier
** Postacie:** Javier, Liam

Poranek Maksimiliam witał na dziedzińcu, przy kamiennym, niewysokim murku, który pozwalał obserwować drogę i dachy niektórych chat, z których kominów sączył się popielaty dym i mieszał się z szarym, nienasyconym niemal żadną barwą niebem. Słońce powoli wstawało, jego pierwsze złote promienie wyjrzały zza gór niemal tylko po to, by musnąć rudą czuprynę jeźdźca kierującego się w strony kamiennej posiadłości. W złotym świetle wstającego słońca, jego kędziorkowate włosy przypominały miedź, lub rozżarzone do czerwoności węgielki. 

Maksimiliam przypatrywał się jeźdźcowi, a gdy ten zauważył samotnie stojącą na dziedzińcu Liama, uśmiechnął się, będąc bliżej z gracją zszedł z siodła i uwiązał konia.

\- Panie hrabio, jak dobrze widzieć! - Zawołał z nieukrywaną radością na co Maksimiliam rozłożył ramiona i uściskał mężczyznę bardzo serdecznie.\
\- Ciebie również, Javierze, ciebie również! 

\- Niech no ci się przyjrzę, Maksimiliamie! Pełen elegancji, przystojności, młodości i siły, a jednak, jak czytałem w liście, coś cię jednak trapi.

\- Jestem tu zaledwie parę dni, Javierze, a już zdążyłem rozwścieczyć brata, sprzeciwić się ojcu, oświadczyć się niewiaście, a matka wzięła mnie za zachowawczego tchórza, w moich komnatach jest przepastna dziura, a ja zostałem mianowany dwudziestym piątym dziedzicem Sterling. W dodatku nikt z rodziny nawet nie zwrócił uwagę, że chodzę o lasce. Czy może być gorzej? - Wyżalił się mu młody hrabia, na co Javier roześmiał się kręcąc głową.

\- Wszystko omówimy, chodź Liamie, jestem by cię wesprzeć. A z laską jest ci bardzo godnie i przystojnie, mnie się podoba, gdyż pasuje do ciebie. - Zapewnił baron de Mounier.

\- Czyli są w tej sytuacji jakieś pozytywne aspekty.

\- Zdecydowanie.

## Suma wszystkich strachów
** Postacie:** Liam, Javier


Słońce było wysoko, konie sapały jeszcze, zmęczone po pościgu, teraz uwiązane do drzew, a nad ogniskiem powoli piekły się dwa króliki. Nieopodal, na grubych skórach odpoczywali dwaj łowcy. Jeden, nieco starszy spoglądał z góry na młodszego, którego głowa spoczywała na jego kolanach, i powoli zanurzył palce w jego płomienno-rudych włosach. Przeczesywał jego loki leniwie.

\-   Streściłem ci właściwie całą tutejszą, napiętą sytuację, więc wiesz w jakim znalazłem się impasie. Na domiar złego nie wiem czego mógłby chcieć cesarski wysłannik, Javierze. - Westchnął habia Sokół.
\-   To bardzo wiele tematów, ale zacznijmy od tego ostatniego. Wiem czego chce wysłannik cesarza, a dokładnie Hastriel var Hess, którego poznałem będąc w stolicy i od tej pory pozostajemy w życzliwych stosunkach. Nasz Cesarz szuka pieniędzy i znajdzie je tutaj, bowiem złoża srebra rodu Dewerów okazały się być większe, niż nadawał to im poprzednik Cesarza. Głupi są, bo gdyby tylko tak się tym nie afiszowali, to napełniliby sobie naprawdę dobrze kabzy i byli by potężniejsi, bo z dwa razy większym majątkiem, niż wszyscy sądzą, że mają. O Bogowie, dlaczego to nie na naszej ziemi nie dali nam srebra! Oh, dlaczego!? - Javier wniósł w teatralnym geście dłonie do góry. - Ja bym wiedział jak to zrobić. Wystarczyło pokolenie w zbytnim dobrobycie i już zgnuśnieli. Co do reszty... Na kły niedźwiedzia! To jak, zostaniesz dziedzicem i padniesz trupem? Tego byśmy nie chcieli, mój drogi!
\-   Nie, nie chcielibyśmy. Bratu jednak też tego nie życzę, chociaż nie jest najbystrzejszą rybką w potoku, to ciągle mój brat. Tyle rzeczy w jednym momencie się zawaliło. Wiesz, co jeszcze przyszło mi do głowy? Przez chwilę, ale jednak. Otóż słuchaj: Mówię Sloanowi jak się mają sprawy, albo lepiej, zostawiam mu list. O wszystkim, o szaleństwie ojca, o przepowiedni, o wysłanniku cesarza, a w tym czasie wracam na swoje włości.
\-   No tak, bo po chwili uświadomiłeś sobie że to by było w stylu twojej rozhisteryzowanej żony! No, ale ja widuję twojego brata częściej niż ty i nie oceniam go tak surowo. To ty jesteś lotnym umysłem, który powinien być w stolicy i doradzać naszemu Słońcu. Ty wolałeś jednak zaszyć się w głuszy i polować na zwierzynę. Tak, czy inaczej, intrygi, problemy i szaleństwa dopadły cię, czy tego chciałeś czy nie.  No i powiedz, co planujesz?

Maksimiliam wywrócił oczyma.Nie mówił do końca poważnie, o czym Javier doskonale wiedział, ale jednak uszczypliwość rudzielca nosiła znamiona prawdy. Liam nie zdecydowałby się wyjechać, nie zmieniało to jednak faktu, że w głębi duszy wcale nie chciał uczestniczyć w tym irracjonalnym przedstawieniu. Wolał łowy od opowiadania się po którejś ze stron.

\-   Spotkam się z wysłannikiem cesarskim, wyślę ekspedycję do wnętrza grot...
\-   Chcesz ojca z pomocą wysłannika ubezwłasnowolnić? Niech tak będzie, ale co jeżeli klątwa jest prawdziwa i umrze wtedy Sloan? Chyba najpierw trzeba sprawdzić te podziemia, a nie wiem czy wiesz, że zwiedziłem sporo jaskiń, zaś z jednej prawie nie wyszedłem, ale dzięki temu, że byłem przygotowany i miałem niedźwiedzi łój, wyślizgnąłem się tej opresji, ha! Poszedłbym tam choćby i teraz, ale wiesz czego mi brakuje? Kąpieli w waszych gorących źródłach!
\-   Jesteś okropny,  jakim sposobem ciągle kradniesz serca wszystkim wokół? - Zaśmiał się Liam, czochrając włosy swego towarzysza. - Ale gorąca kąpiel naprawdę brzmi rozkosznie, może odrobina przyjemności podsunie mi lepszy pomysł.

## Gorące źródła
** Postacie:** Sloan, Liam, Javier

Powiadają, że kiedyś były tutaj huty dziwnych istot, w których robiono stal której ludzkość nadal nie potrafiła odtworzyć. Wszystko dzięki temu, że miały tu gdzieś płynąć podziemne ogniste rzeki które pozwalały osiągnąć niezwykle wysokie temperatury. Nie wiadomo jaka jest prawda, ale wiadomo że podziemne geotermia pod twierdzą są wyśmienite i pozwalają odpocząć po naprawdę męczącym dniu. Lub dniach.



I tak się właśnie zdarzyło, że kiedy Liam i Javier dotarli do źródeł, czekał tam już Sloan. To znaczy nie czekał ani na nich, ani na kogokolwiek. Po prostu tam był, zanurzony w myślach i w wodzie.



Wystarczyło zejść tylko po kamiennych schodach w dół, aby poczuć narastające ciepło i wilgoć wypełniającą powietrze. Wysokie, wapienne ściany pokryte były zaciekami i skraplająca się wodą, bladoniebieskie kryształy sterczały natomiast tu i ówdzie oświetlając wnętrza.

Słudzy pomogli młodszemu z synów de Falco zdjąć odzienie, aby mógł się w towarzystwie brata, oraz barona d'Mounier cieszyć przyjemnością gorącej kąpieli. Maksimiliam nie spodziewał się zastać brata właśnie w tym miejscu, ale absolutnie nie dał mu tego odczuć.

\- Bracie - przywitał go Liam zanurzając się w wodzie i od razu czując relaksującą przyjemność. - Mamy gościa, który, co mówię z radością, może zdjąć z twych barków przynajmniej część zmartwień. Wiemy już przynajmniej jaki jest cel wizyty cesarskiego wysłannika.

\- Witaj Javierze d'Mounier - Powiedział wyrwany z rozmyślań Sloan, pochmurny i jakby niechętny przybyłym gościom - Co takiego wiemy o celach jego wizyty? Przybył żeby zadać nam cios w plecy jakby innych problemów było mało? Podnieść podatki?

\- Nic z tych rzeczy. - Zapewnił go młodszy brat. - Przybywa niejako na inspekcje. Pamiętasz dodatkowe punkty wydobywcze srebra naszej drogiej rodziny? Zatajali wysokość przychodów na niekorzyść cesarstwa. Wizytacja może nie będzie należała do najprzyjemniejszych, ale środek ciężkości jest poza naszą ziemią, nie mniej, tę rozmowę wezmę na siebie, o to jedno nie musisz się trapić.

\- Ha! - Zakrzyknął - Wreszcie, wreszcie coś dobrego! Trzeba to uczcić! Ej, przynieście nam wina! - Krzyknął do służby. - Javier, bo rozumiem że zostajesz nie do ranka prawda? 

\- Zostanie. - Liam odpowiedział za niego i spojrzał na przyjaciela, a ten rozłożył ręce nieco teatralnym gestem.

\- Zostanę! Skoro dwóch drogich mi przyjaciół nalega, nie mogę oponować! Z resztą już dawno miałem zamiar zajrzeć tu do waszych włości, mości Sloanie, a gdy dowiedziałem się, że i Maksimiliam się zjawi, miód na moje serce. Z resztą, Sloanie, moja najmłodsza siostra Cilla ciągle o ciebie pyta, czyżbyś zawrócił jej jej głowie?

\- Cóż, wystarczy abyś o kimś wspomniał dobrze, to część Twojego uroku przechodzi na dowolną wspomnianą osobę, nawet gdyby był to kamienny posąg krowy, mój drogi! - Mówił jeszcze-nie-władca-a-może-nigdy wyraźne w polepszonym humorze. - Ja nie jestem najlepszym mężczyzną dla kobiety, bardziej romantyczne ode mnie są góry. Niby takie ostre i zimne, a jakoś kobiety bardziej do nich wzdychają niż do mnie! - Chwycił przyniesioną butelkę od młodego chłopaka - Jedna butelka!? Migiem przynieś ich więcej! - Butelkę podał zaś Javierowi - Goście pierwsi!

Javier wziął butelkę i przeniósł wzrok na Liama, ten pokiwał głową.

\-   Słuchaj mego brata, dobrze prawi.

<!-- -->

\-   Dobrze, zatem wasze zdrowie!  - Uniósł ją w toaście i napił się solidnego łyka, po czym oddał butelkę Sloanowi. - Mm, nie wykręcałbym się tak na twym miejscu, Cilla wzdychała do tych "pięęęknych, chłodnych oczu, do tej barwy głosu jak burza w górach, do tej postury niedźwiedzia, który niczego się nie lęka!" - Zacytował Javier, śmiejąc się i sięgając po butelkę gdy znów przyszła jego kolej. 
\-   No dobra, może mam w sobie coś. O, dziękuję! - Powiedział miło tym razem do chłopca, który przyniósł kilka butelek wina. Nikt nie zwrócił uwagi na dziwną urodę dziecka, który na pewno nie pochodził z tych okolic. Miał jasną karnację, ale nie taką, jaką mają ludzie z tych okolic. Wyglądał, jakby był szlachcicem, który naprawdę usilnie unikał słońca. - Powiem Ci, że nie wiem coś ty za wino wtedy przytargał Liam, ale głowa mnie po nim strasznie bolała. Więc pijmy moje, sprawdzone, robione z winorośli z naszego ogrodu! Zasługa Irvette, więc jej zdrowie!
\-   Moje najszczersze przeprosiny, chyba było po prostu zbyt mocne, lecz faktem jest, że temu winu trudno sprostać. - Odparł Maksimiliam delektując się lokalnym, naprawdę pysznym trunkiem. Gorąca woda i dobry alkohol sprawiał, że miło zaczynało szumieć w głowie, jednak był to moment wytchnienia na który wszyscy sobie zasłużyli.
\-   Liamie, a tobie jak idzie w sercowych sprawach? Żona nie przyjechała... - Sloan nagle zamilkł i zaczął mocno kaszleć. - No\... nie masz raczej szczęścia do kobiet, co? - Dokończył niewprawnie, zakrywając twarz butelką z której pociągnął spory łyk.
\-   Mam szczęście do kobiet, ale do nieodpowiednich. - Wywrócił oczami i zabrał bratu butelkę, aby opróżnić do końca. - Mam szczęście do niewiast nieodpowiedniego rodu, zamężnych, komuś przyobiecanych, zakonnych nawet. Prianka to doprawdy urocza niewiasta, ale bardzo młoda, zbyt młoda i niezbyt rozgarnięta. Za to nie mogę narzekać na nudę. - Dodał z przekąsem.
\-   Przesadzasz mój drogi, zresztą jak Ci żona nie pasuje, to może znajdź sobie kobietę na grzanie łoża. Jak ci brakuje kobiet tam, to weź jakąś stąd.
\-   Pyszny pomysł. - Mruknął z kwaśną miną. - Wolałbym jednak dziedzica z prawego łoża. 
\-   Ha, ja też bym chciał, ale Irvette dwa razy poroniła a na koniec urodziła dziewczynkę. I co zrobisz? Zresztą, zaczyna być przez to smętnie, że pytam się o twoją żonę... Javier, co u Ciebie? Nie widać, abyś się statkował, dalej korzystasz z życia?

Rudy towarzysz braci de Falco rozparł się wygodnie na brzegu basenu skalnego i z szelmowskim uśmiechem przeniósł wzrok z jednego mężczyzny na drugiego i westchnął teatralnie.

\-   Moi najmilsi przyjaciele, nie mógłbym dzielić się sobą z tylko jedną os\...kobietą. Mam tyle miłości, że jeszcze trochę czasu minie nim faktycznie skłonię się ku myślom o ożenku. Nie ujmując wam, bowiem podziwiam starszych mężczyzn, jeszcze parę lat przede mną nim dopadną mnie wasze zmartwienia. 
\-   Ustatkowanie się to tylko kwestia przymusu raczej, rzadziej miłości. Zresztą, dla nas, ludzi z tytułami, powinność zmusza do robienia rzeczy które są potrzebne a nie których pragniemy. Czy pragnąłem Irvette? Nie, ale czy mi z nią źle? No nie za bardzo, chociaż w łożu jest trochę zimna. - Snuł w odpowiedzi na wypowiedź gościa Sloan. - No ale chyba Ty najgorzej masz co Maksymilianie? Na większym odludziu niż tutaj, z żoną która przyprawiłą ci tą laskę. Cierpisz co?

Maksimiliam spojrzał na brata z jakąś nutą niezrozumienia i niechęci, jakby po części sądził, że ten mu współczuje a po części kpi, i gdyby teraz zapytał go ktoś co byłoby gorsze, nie potrafiłby stwierdzić, gdyż drwina i litość bywają równie mocno upokarzające. 

\-   Takie wnioski wysnułeś widząc mnie po niemal pięciu latach, bracie? Pogranicze nie jest tak okrutnym odludziem, do Novigradu nie mam daleko, lasy są urodzajne, ziemie żyzne a chłopi pracowici, tylko co do żony miałem nieco wyższe oczekiwania. Wiedziałbyś o tym wszystkim, gdybyś zechciał mnie czasem odwiedzić. Mógłbyś wziąć ze sobą nawet Irvette wraz z córką, lub nie, wedle uznania. 
\-   Wiesz, nie pałałeś do mnie miłością w czasie wyjazdu i nie zaprosiłeś mnie. Skąd miałem wiedzieć, że czekasz na mnie z otwartymi ramionami. Irvette też jakoś nie naciskała na wyjazd, mówiąc, że musisz sobie ułożyć życie w nowych rejonach i jak będziesz gotów, to nas zaprosisz.
\-   Zaiste, mój ślub z Prianką był zbyt małym powodem, aby się zjawić\...
\-   Khem. - Odkaszlnął Javier. - Panowie... Jesteśmy tu jednak, prawda, w pysznym towarzystwie, stare zatargi powinny odejść już w niepamięć. Przyznam, że serce pękło by mi, gdyby moi ulubieni, najserdeczniejsi przyjaciele pozostawali poróżnieni. Jak jak wam zazdroszczę! 

Powiedział, i zdał sobie sprawę, że uczynił to nieco zbyt przesadzonym tonem, ale zaaferowany publicznością i spojrzeniem dwóch par bystrych oczu, poruszył się, by usiąść wygodniej i kontynuował wywód.

\-   Naprawdę! Macie siebie. Ileż ja bym dał by mieć takiego brata jak Liam - powiedział do Sloana, a zaraz przeniósł wzrok też na Maksymiliama  - albo jak Sloan. Widzicie, moi drodzy, sekretem nie nie jest iż posiadam sióstr pięć, co nie jest brzemieniem, gdyż są to niewiasty cokolwiek wspaniałe, ale nigdy nie mógłbym dzielić się z nimi swą przyjaźnią, myślami i perspektywami... jak z bratem. - Zrobił żałosną, smutną minę, rozkładając bezradnie ręce. - Nawet więcej wam powiem! Ta zaciętość wasza, ta walka, jest taka pełna braterskiej miłości, no bo przecież ostatecznie, no przyznacie, macie szczęście, bo macie siebie. Hm? Kto się zamieni na rodzeństwo? - Zażartował na koniec, wyczekująco patrząc na Liama i Sloana, sam ochoczo podnosząc rękę i zgłaszając się w ten sposób.
\-   Dobrze, dobrze! Dość może o kobietach! Jak się sprawia Kord? Powiem szczerze, że to od niego wiemy o Twojej lasce, bo był tak zaniepokojony, że wysłał nam list o tej sytuacji, po czym wysłał kolejny że nie jest to takie poważne. Jaka wtedy chandra dopadła moją żonę... ha, ale miało być bez nich. No i ten medyk, jaki on jest?

Maksimiliam zwrócił uwagę na słowa brata, dotyczące tego, iż Irvette zmartwiła się jego stanem zdrowia, ale nic na to nie odpowiedział. W głębi duszy jednak obiecał sobie, że z Irvette porozmawia, gdyż jej przyjaźń, troska oraz zaangażowanie wydawały się być dużo większymi niż sądził jeszcze dwa dni temu. Sam uważał Sterling za umiarkowanie ciekawe miejsce, a osoba ciekawa świata może czuć się tu po pewnym czasie znużona, dlatego jej gorące wyznanie i skłonność do romansu uznał raczej za przejaw nudy niż faktycznego, emocjonalnego zaangażowania. Teraz jednak zaczynał się nad tym poważnie zastanawiać.

\-   Kord zatem był tak niedyskrety w sprawie moich zdrowotnych ekscesów? Bogowie, zdaje się, wyciągnęli tego surowe konsekwencje, gdyż gdy wyjeżdżałem zaniemógł, mając problem z płucami. Nie wiem co obecnie z nim się dzieje, ale Grot jest moją prawą ręką i czuwa nad Twierdzą oraz jej mieszkańcami najlepiej jak potrafi. Ale odpowiadając na pytanie dotyczące medyka - kontynuował hrabia - nazywa się Gregory, jest bystrym, młodym mężczyzną ciekawym świata. Wydaje mi się, że nie ma zbyt rozległej wiedzy, ale za to ogromną intuicję. On oraz lokalny na Pograniczu medyk sprawili iż czuję się nienajgorzej.
\-   Nienajgorzej! - Żachnął się Javier naprawdę poruszony tą narracją, aż potrząsnął głową. - Sloanie, nawet sobie nie wyobrażasz w jakim stanie był twój brat! Wrócił zza grobu! Wybacz, że nie rozmawialiśmy o tym jakoś wcześniej wprost, alem sądził iż wiesz o wszystkim... nie mniej, przyjmij moje przeprosiny, gdyż myliłem się najwyraźniej. Wracając. Wyobraź sobie, że parę tygodni Liam leżał w gorączce, jakby go kto kubłami z wodą oblewał, rana jątrzyła się i nie chciała zasklepić, co efektem było niechybnej elfiej, zatrutej strzały. Przeszyła udo Liama, gdy gromił leśną watahę bandytów mających w pogardzie wszelką przyzwoitość...
\-   Ależ Javierze.
\-   - Nie, nie! Nie przerywaj, mój drogi! Sam widziałem w jakim byłeś stanie. Majaki na przemian z nieprzytomnością mroziły nas wszystkich wtedy, łącznie z twą żoną jaśnie panią hrabiną Prianką de Falco, i to był jedyny moment, kiedy coś nas jednoczyło. Troska o twoją, wyjątkową osobę. 

Javier westchnął głęboko, przypominając sobie tamte emocje i obmył twarz wodą, a włosy odruchowo zaczesał do tyłu, gdyż mokre spływały w nieładzie na twarz. W jego szarych oczach było widać szczerą troskę o Liama. Wychowali się razem, znali dobrze i wyglądali na naprawdę dobrych przyjaciół, chociaż Javierowi otwartości i serdeczności nie można było odmówić nawet do obcych osób, wystarczało aby wydali się chociaż odrobinę interesujący.

Sloan wyglądał na bardzo poruszonego, co dodatkowo podkreśliła stukotem butelka, która wyślizgnęła mu się z ręki.

\-   Na bogów, co? Kilka tygodni w gorączce? Zatruta elfia strzała? Matko jedyna, jedyne co wiedzieliśmy, to że zostałeś ranny na polowaniu, sądziliśmy, że to był wypadek. Bracie, co tam się dzieje na tym pograniczu, jakie elfy? W co do cholery wpakował Cię nasz ojciec? - Resztkę już mówił stojąc, z dyndającym wielkim kutasem godnym następcy rodu. - Liam, Liamie, ja Cię przepraszam, naprawdę Cię przepraszam, nie wiedziałem nic o tym...
\-   Javier przejaskrawia... z pewnością nie było aż tak tragicznie, ale nie był to wynik polowania i owszem, rana po strzale długo nie chciała się zagoić, co prawda grot ominął kość, więc chodzić mogę, ale szybko czuję ból. Nie gniewam się na ciebie, Sloanie... w końcu sam postanowiłem nie obarczać cię swoimi strapieniami. 

Mówiąc to, bardzo starał się nie patrzeć na minę Javiera, który przez chwilę wyglądał na wręcz onieśmielonego widokiem kolejnego przyrodzenia panów de Falco, a później nawet oczarowanym.

\-   Do ojca też nie miej pretensji, po prostu jesteś ważniejszy, nawet jeśli czasem wydaje ci się inaczej.
\-   Człowieku, ja tutaj nawet nie byłem bliski takiej sytuacji w której Ty byłeś i śmiesz bronić, ojca opętanego szaleństwem! Mam mu ochotę skręcić kark za to, że pozwalał mi myśleć, że ty praktycznie na wypoczynek tam wyjechałeś, bawić się. Rozumiesz?! - Krzyknął, czerwony od złości i gorącej wody - Przez to miałem Cię za głupca, obiboka, niedojdę i cholera wie kogo tam jeszcze, tylko dlatego, że ktoś pozwolił mi tak sądzić. Teraz zaś ten ten ten... - chwycił się oburącz za głowę, wziął oddech i usiadł na krawędzi bulgoczącego akwenu. - I co ja mam sądzić o tym wszystkim, co? Ufałem ojcu kompletnie, a teraz on okazuje się być niespełna rozumu i moralności od dawien dawna!

Maksimiliam usiadł obok brata i poklepał go po ramieniu. Javier udawał przez chwilę, że nie uczestniczy w rozmowie, ale ostatecznie postanowił zostawić braci samych, dał tylko Liamowi znak, że poczeka gdzieś indziej. 

\-   Sloanie, powiem ci coś. Może to nieco rozjaśni twój umysł, sądzę że ojciec postradał zmysły z powodu zbyt dużego brzemienia, które spoczęło na jego barkach, myślę, że dowiedział się czegoś, co go przerosło. Odpowiedź znajdziemy niestety, tylko w wyrwie, która powstała, a z której ojciec nasz powrócił oraz z ksiąg, których jeszcze nie przejrzałem do końca... oczywiście istnieje też możliwość iż bogowie odjęli naszemu ojcu rozum dla własnego kaprysu, jednakże pomińmy to, gdyż warto być ostrożnym i wierzyć, że w tym szaleństwie jest odrobina sensu. Pamiętaj jednak, obiecałem ci, że nie zostawię cię z tym samego. Ojciec nie znalazł się jeszcze na łożu śmierci, więc mamy czas. Nie do końca też wiadomo który z nas ma zostać panem, ale to działa obecnie na naszą korzyść. Wielu braci poróżniła by taka sytuacja i wprowadziła wielki zamęt, od waśni do rozlewu krwi, my to zamieszanie użyjemy na naszą korzyść. 
\-   To ja powiem Ci co go przerosło. Jego syn. Nie zatrzymuj mnie, muszę przemyśleć kilka rzeczy. - Powiedział już spokojnie, zimno, niczym człowiek który planuje morderstwo. - Życzę Ci dobrej nocy.







## Żona

** Postacie:** Javier, Liam



Siedząc przy biurku Liam złożył dłonie jak do modlitwy, stykając z sobą opuszki obu palców i dumając nad formą listu. Teraz, myśląc o Priance na chłodno i o tym, co zamierzał zrobić zaczęły nachodzić go wątpliwości oraz wyrzuty sumienia.

\- Dużo razem przeszliśmy z Prianką, jesteśmy obydwoje na dobrej drodze.  -- Tłumaczył się Liam Javierowi, który towarzyszył mu leniwie przerzucając karty ksiąg i popijając mętne, rozcieńczone piwo, w sam raz gaszące pragnienie. Puchaty szlafrok otulał jego blade ciało, a włosy wydawały się być teraz ciemnoczerwone, gdyż ciągle jeszcze pozostawały wilgotne po kąpieli.

\- Tak, tak, mój drogi. Na tej drodze, na której stałeś by ją ratować, na tej samej, na której doznałeś uszczerbku na zdrowiu i nigdy już nie wróci ci tamta sprawność. -- Spojrzał na niego znacząco Javier. -- Powiedz, masz jakiekolwiek miłe wspomnienie związane z tą dziewką?

\- Javierze, proszę cię, nie mów o Priance z taką pogardą, to ciągle moja żona. -- Odparł karcąco hrabia i odwrócił się przez ramię, aby pokazać swoją dezaprobatę wobec słów przyjaciela, ale w głębi duszy wiedział, że Javier ma nieco racji. Dużo łatwiej przyszłoby mu wymienienie pięciu dobrych wspomnień związanych z Irvette, nawet z D'oh, niż z Prianką. -- Nie chcę rujnować Priance życia, jest taka młoda, nie chcę jej odprawiać i tym sprawić, że będzie nieufna i skrzywdzona, nigdy o tym nie rozmawialiśmy...

\- Och, dajże spokój! -- Javier wywrócił oczami i odstawiając drewniany kufel wstał. Podszedł do Liama, położył ręce na jego ramionach, czując jak bardzo ma spięte mięśnie. -- Rozluźnij się. Posłuchaj... jeśli miałoby cię to pocieszyć, to przyjadę na całe lato. Rano będziemy jeździć na łowy, na przejażdżki, wieczorami poczytasz mi ciekawe historie, jeziora będą gorące od słońca, upieczemy dzika i każę posłać po trubadurów, pokażesz mi jak przysposabiać sokoły i jak uczyć charty współpracy z niemi, moje siostry chętnie cię odwiedzą, jesteś ich ulubionym kuzynem, no może nie Cilli, ona akurat woli Sloana, ale reszty jak najbardziej. Zapomnisz o zmartwieniach.



>>>

„Wielce Szanowny Xsiążę,



Ufam iż masz się, Panie, w dobrym zdrowiu i powodzeniu, a łaska bogów sprzyja twym przedsięwzięciom. Na wstępie pokornie proszę o wybaczenie i wyrozumiałość, gdyż sprawy związane z rodziną i dziedziczną ziemią zmusiły mnie do wyjazdu i pozostania w Sterling przez czas jakiś, przeto osobiście zjawić się nie mogę i tych jakże ważnych, niedelikatnych spraw omówić osobiście.

Z ciężkim sercem i trudem wielkim przyszło mi podjąć decyzję co do przyszłości mojej i jaśnie hrabiny Prianki de Falco, Pańskiej bratanicy. Chociaż darzę hrabinę Priankę ogromnym szacunkiem, to jednak parę wspólnych lat nie okazało się dla nas łaskawych. Ubolewam najmocniej nad tem, iż przez cały czas trwania mojego i jaśnie pani Prianki małżeństwa, nie doczekali się potomka, brak dziedzica po latach starań jest mi główną przyczyną, przez którą ponownie musiałem przemyśleć naszą diadę. Nie byłem także w stanie znaleźć z Priankę żadnej płaszczyzny porozumienia, nasze światopoglądy, oczekiwania i ekspresja rozmijają się do tego stopnia iż nie widzę szans na zmianę. Jaśnie pani Prianka także męczy się, towarzyszyło mi to jej poczucie rozczarowania i niechęci dniami oraz miesiącami, które zamieniły się w lata. Pragnę zatem poinformować Was, Wasza Xsiążęca mość, jako opiekuna i krewnego, że z wielkim żalem odprawiam jaśnie panią Priankę, tym sam rozwiązując z nią zawarte małżeńskie przyrzeczenia i obowiązki.

Żywię ponadto nadzieję, iż zrozumiesz Panie, tę decyzję, która nie byłam dla mnie łatwa, ani przyjemna. I że po mimo niepowodzenia, które spłynęło na związek mój z Pańską bratanicą, nadal pozostaniesz, Panie, moim serdecznym jak dotąd przyjacielem.

Decyzję podjąłem po długiej rozwadze, bez konsultacji z jaśnie panią Prianką. O moim postanowieniu informuję listownie i w takiej formie moją decyzję oraz jej powody pozna także jaśnie pani Prianka.  Sprawy dotyczące majątku polecę rozwiązać w osobnym piśmie, zapewniając jednocześnie iż zawsze będę służyć jaśnie pani Priance przyjacielską pomocą.



z wyrazami głębokiego szacunku

hrabia Maximiliam de Falco"

>>>

Rozległo się stukanie do drzwi. Po zaproszeniu, do środka weszła Irvette, blada, zaniepokojonym głosem powiedziała:

\-   Bardzo was przepraszam, że was nawiedzam w nocy, ale mój mąż nie wrócił do komnaty. Nigdzie nie mogę go znaleźć i pomyślałam, że może jest z Wami, ale... - jej wzrok przemknął po pomieszczeniu - widze, że go tutaj nie ma. Wiecie może gdzie się znajduje?

Liam podniósł roztargniony wzrok znad listu, gdzie właśnie stawiał ostatnie litery. Odłożył pióro i zatrzymał zamyślony wzrok na Irvettcie.

\-   Nie. - Odparł w końcu i wstał. - Nie wiem niestety, gdzie się podziewa twój małżonek, Irvetto. Wiem natomiast, że chciał chwili wytchnienia, aby móc przemyśleć parę trapiących go spraw. Chcesz abym go poszukał?
\-   I gdzie go poszukasz w twoim stanie Maksymilanie? Irvette, czy on czasem nie pojawia się na noc? - Zapytał Javier, który pomyślał właśnie o tym, że ich życie erotyczne nie należało do najciekawszych i możliwe, że w niektóre noce umilał sobie wdziękiem innych kobiet.
\-   Nie... znaczy tak, czasem pracował do późna albo gdzieś wyjeżdżał, ale zawsze mnie o tym zawiadamiał. Normalnie bym się tak nie zaniepokoiła, ale teraz, w tych okolicznościach... - Owszem, podejrzewała męża o sypianie na boku z innymi kobietami, ale nie można winić za coś takiego mężczyzny o takim wigorze i wewnętrznej energii. Żałowała tylko w takim momentach, że nie ma takiego apetytu na łóżkowe zabawy co on, ale tak to już jest w małżeństwach...
\-   W porządku - powiedział Liam kładąc dłoń na ramieniu kobiety i uśmiechając się do niej lekko oraz krzepiąco - jeśli się martwisz, mogę się za Sloanem rozejrzeć w paru miejscach. Nie obiecuję, że go znajdę, a nawet jeśli, to tak jak wspomniałem, potrzebuje trochę czasu dla siebie. I... Jeśli chcecie czymś zająć ręce, możecie zalakować z Javierem mój list do żony.

Maksimiliam wziął płaszcz oraz laskę, posłał Javierowi spojrzenie, które być może tylko jemu coś mówiło i wyszedł, by spełnić obietnicę. Miał parę pomysłów gdzie mógłby znaleźć Sloana. Punkty które chciał sprawdzić częściowo były banalne, w innym przypadku nie. Postanowił zacząć od swojej komnaty, biblioteki, ogrodu. To były pierwsze miejsca, tymczasem Irvette pozostawała w komnacie z Javierem, który westchnął głęboko za Maksimiliamem.

-Musi cię lubić, skoro spełnia z takim zapałem niewypowiedziane prośby... - zagaił Irvettę i poprawił poły szlafroka, gdyż w towarzystwie kobiety prezentował się cokolwiek nago. Nonszalancko przysiadł na brzegu biurka i wziął list Maksa.

\- Ja też go lubię i ufam mu Javeirze, a on mi. Dlatego wie, że nie proszę go o to, bo się trochę przestraszyłam. Nie należę do lękliwych kobiet, przecież wiesz. Liam wszystko Ci opowiedział, o całej sytuacji tak? On ze wszystkich to chyba tobie najbardziej zawsze ufał. - Mówiła spokojnie, ale zastanawiała się jednocześnie, czy i ile Javier wie o tym, co wyznała hrabiemu w tamtej chatce.

Wzruszył ramionami.

\-   Znamy się z Liamem od dziecka, Irvetto. - Odparł niby od niechcenia Javier, nawet się nie kryjąc specjalnie z tym, że zaznajamia się z treścią listu napisanego ręką Maksimiliama. - Znam go lepiej, mam wrażenie, niż on sam siebie. Człowiek taki jak on marnuje się na Pograniczu, jego miejsce jest w stolicy...



## Powinniśmy spotkać się częściej
** Postacie:** Cilla (Ann), Sloan


Dziewczyna ciasno oplatała udami jego biodra, poruszając nimi w miłosnym uścisku, jej białą skórę zaczęły pokrywać drobne kropelki potu niczym sznur pereł, a piersi unosiły się w nierównym oddechu, gdy wraz ze spazmem pełnym rozkoszy wygięła ciało. Z błogim wyrazem twarzy i błąkającym się po ustach uśmiechem położyła się obok i wtuliła twarz w szyję starszego mężczyzny.

\ - Powinniśmy spotykać się częściej, Sloanie. -- Mruknęła odgarniając rude pasma włosów, które lepiły się do jej mokrych od potu piersi. Teraz miała chwilę, aby przyjrzeć się mężczyźnie, który przyszedł na schadzkę pełen gniewu i frustracji, ale też pełen niespożytej energii, którą wykorzystali w słuszny, bardzo namiętny i ognisty sposób.



## Rozwód
** Postacie:** Javier, Liam


\-   A czy... możesz powiedzieć o czym pisze do swojej żony? Martwię się, że ich związek nie należy do udanych. - zagadnęła niby to od niechcenia, widząc, że Javier bez żenady czyta prywatny list hrabiego.
\-   Chcesz przeczytać? - Uśmiechnął się szelmowsko rudy mężczyzna i przechylił w jej stronę odrobinę, przyglądał się jej szarymi, bystrymi oczyma przez chwilę i wyciągnął rękę z lisem w jej stronę.
\-   Nie wiem czy mogę... - powiedziała z wahaniem - nie powiesz mu?
\-   Tylko wtedy, gdy i ty zachowasz to w sekrecie. - Odparł konspiracyjnym szeptem po czym dał list Irvettcie. Tak naprawdę widział, że sam list jest nudny i zachowawczy, Maksimiliam nie należał do niedyskretnych osób, nawet dla tych za którymi nie przepadał potrafił być uprzejmy. Jednak myśl, że Irvette przeczyta tą prywatną, intymną korespondencję była dużo bardziej emocjonująca niż sama treść listu, tym bardziej że on, Javier, podczas rozmowy z Liamem wywnioskował co mniej więcej znajdzie się w wiadomości.
\-   On naprawdę, naprawdę to zrobił... - wydusiła z siebie Irvette po przeczytaniu listu i z wrażenia aż usiadła na krześle - Ja nie spodziewałam się, że on to zrobi, ja chciałam go mieć chociaż przez chwilę, tak jak Sloan czasem inne - Strasznie zbladła - Ale co teraz, nie jestem gotowa, ja nie wiem...

Irvette wyglądała na strasznie przybitą sytuacją, jakby dopadły ją konsekwencje, których zupełnie się nie spodziewała. {.c18}Javier potrząsnął głową, ale wyczuł, że temat może być niespodziewanie ciekawy.

\-   O czym ty mówisz? Przecież nie jesteś panną na wydaniu, więc raczej nie zrobił tego dla ciebie. Masz męża, z tego co mi wiadomo, nawet... nawet wydaje mi się, że to jakiś bliski krewny Maksimilamia. - Stwierdził pół żartem, pół serio. Rozgrzał lak i zamknął list przykładając do gorącego laku rodową pieczęć przyjaciela. W myślach postanowił, że przy najbliższej okazji upomni go, żeby był w tym względzie bardziej ostrożny i nie zostawiał pieczęci gdzie popadnie.

Spojrzała na Javiera wrogim spojrzeniem.

\-   Tak sądzisz? A może go o to poprosiłam i tylko dziwię się, że zrobił to tak szybko? Javier, Ty nie wiesz co to jest prawdziwa miłość i nie wiesz jak to jest widzieć ukochaną osobę z kimś innym, a ty musisz być w ramionach kogoś, kto cię nie chce i chodzi do innych kobiet. Lubię cię, ale ty nie jesteś człowiekiem, który może kogokolwiek kochać stale, bo ciebie widzieć można co rusz z kimś innym...
\-   "Ukochaną osobę"? Och, Irvetto, to dopiero byłby skandal! Tylko że... nie bądź zła, Irvetto, ale nie wiem jak delikatnie to określić... trudno mi uwierzyć, że Liam mógłby mieć z tobą romans.
\-   Bo jestem żoną jego brata? Albo, że jestem już nie pierwszej wiosny kobietą? Co niby sprawia, że tak w to wątpisz?
\-   Raczej jest jedną z tych osób, która nie chciałaby ujmować godności tobie, swojemu bratu, a także żonie, chociaż ją z tej listy powoli można wykreślać. - Zapieczętowane listy ułożył na brzegu biurka i chrząknął znacząco. - Późno już, nie uważasz? 

Irvette wstała z krzesła, stanęła prosto.

\-   Nie uważam Javier, nie w sytuacji kiedy mój mąż pewnie pieprzy jakąś służkę. Jednak faktycznie twoja obecność mi już zbrzydła, więc życzę ci milszej nocy, niż mojej.

Drzwi akurat skrzypnęły, bez pukania, gdyż jakby nie było, te komnaty obecnie zajmował hrabia Sokół, który przestępując przez próg zauważył dziwne napięcie wiszące w powietrzu pomiędzy Javierem a Irvettą. 

\-   Coś się stało? - Zapytał odkładając laskę i zdejmując płaszcz. - Irvetto, w każdym razie pragnę cię uspokoić. Ze Sloanem wszystko w porządku, po prostu musi przemyśleć sobie parę spraw, ale wie, że o niego się niepokoisz. Przepraszam cię w jego imieniu - uśmiechnął się miło, pragnąc ją uspokoić. - Idź wypocząć.
\-   Jak dobrze, że poświęcił myślami chwile mojej osobie. - Powiedziała zimno Irvette - Dobranoc Liamie.

To mówiąc, wyszła. {.c18}Javier zasunął zasuwkę w solidnych, dębowych drzwiach i oparł się o nie plecami splatając ręce z przodu i przyglądając się hrabiemu.

\-   Dlaczego tak na mnie patrzysz, Javierze? Jesteś o coś na mnie zły?
\-   Co Ty robisz Liamie? Dlaczego robisz nadzieje tej kobiecie na coś, czego nie możesz jej dać?

Maksimiliam powoli pokręcił głową, odpinając skórzane pasmo, którym był przepasany w talii i powoli rozpinając kaftan.

\-   Nie wiem co masz na myśli. Nie daję Irvettcie złudnych nadziei.
\-   To dlaczego sądzi, że jesteście na najlepszej drodze, aby być razem albo chociaż do płomiennego romansu?

Liam nabrał powietrza w płuca jakby brał oddech przed nurkowaniem, nagle zrozumiał nastawienie Javiera, być może po części także napięcie między nim a Irvette. Hrabia zdjął kaftan i przerzucił go przez oparcie krzesła.

\-   Nie wiem co sądzi Irvette, faktem jest jedynie, iż obiecałem jej, że będzie pierwszą kobietą o której rękę poproszę, jeśli okoliczności będą temu sprzyjały. Javierze, moje słowa były całkowicie szczere, nie było w nich podstępu, ani flirtu... - Zzuł buty i zdjął koszulę obnażając tors i kędziorki włosów na nim. - Zazdroszczę mojemu bratu iż dzieli życie z jedyną kobietą, z którą rozumiem się dobrze i którą darzę przyjaźnią. Sloan nie kocha jej, nie rozumie i nie chce, w najlepszym razie traktuje ją obojętnie jak coś do czego po prostu trzeba się przyzwyczaić. Trochę go za to nienawidzę, gdyż uświadomił mi jak dokładnie wygląda moje własne małżeństwo. Teraz przynajmniej dwie osoby nie będą nieszczęśliwe, ja i Prianka. 
\-   Zatem w liście piszesz o rozwodzie? - Zapytał podstępnie Javier, udając że nie wie co jest treścią listu. - Tylko wyjaśnij mi teraz, co uważasz za sprzyjające okoliczności? Zostanie głową rodu zgodnie z wolą ojca i pozbawienie żony swojego brata? Czy może chcesz te okoliczności stworzyć jeszcze inny sposób? No chyba, że jest to przejaw twojej dyplomacji, w takim razie chylę czoła!

Maksimiliam podszedł do niego, całkiem blisko, tak, że Javier wyraźnie mógł się przyjrzeć niezbyt wyraźnym piegom na twarzy Liama oraz delikatnym zmarszczkom przy oczach, dodającym pogody szarozielonym oczom hrabiego.

\-   Nie wiesz com napisał w liście? - Zapytał, a w jego tonie głosu czaiła się pułapka. 

Wytrzymał jego wzrok przez chwilę i odpowiedział:

\-   Wiem, przeczytałem. Widzę, że szybko pomyślałeś o tym, że to zrobiłem. Ty mnie znasz, ale ja znam ciebie. Rozumiesz się z nią dobrze, jak z przyjaciółką. Nie wydaje mi się jednak, że ty choć przez sekundę pomyślałeś o niej jako o kimś więcej. Gdyby tak było, już byście się przespali i na tym zakończyli całą sprawę, i nigdy do niej nie wracali. Tą obietnicą zrobiłeś bardzo dużo złego, Liamie. - Powiedział i delikatnie dotknął jego ramienia.
\-   Rozumiem. Wszystko naprawię. - Odparł, przyjmując do wiadomości, że gdzieś popełnił błąd. - Zawsze wszystko naprawiam, prędzej czy później. Dzielenie łoża z Irvettą jest dla mnie ostatecznością, ale wcale jej nie pragnę... - mówiąc to, przybliżył swoje usta do jego ust. Pocałował go, właściwie myślał o tym od kiedy zobaczył Javiera na dziedzińcu w Sterling. 
\-   Nareszcie... - Powiedział oddając pocałunek i zrzucając szlafrok. Pomyślał tylko jeszcze, że chyba tylko Irvetta nie będzie zaspokojona tej nocy.



## Pstrągi
** Postacie:** Javier, Liam

Poranne słońce powoli sączyło się przez zielonkawe szyby. Javier spał z lekko rozchylonymi ustami, a Liam przyglądał się mu, rozkoszując się tym rzadkim widokiem i chwilą bez pośpiechu. Uśmiechnął się do swoich myśli, kurze łapki przy oczach stały się na chwilę bardziej wyraźne. Miedziane loki Javiera były pierwszym, co zobaczył po przebudzeniu, a jego spokojny oddech pierwszym, co usłyszał. Starał się tę chwilę i ten widok zachować w pamięci, gdyż tak urocze poranki zdarzały się wyjątkowo rzadko, więc pamięć o ich niezwykłości pozwalała Maksimiliamowi czekać na kolejne spotkania. Javier był uroczym chłopcem, który wyrósł na równie urzekającego mężczyznę i chociaż Liam nigdy nie powiedział mu tego wprost, to uwielbiał go w każdym calu, jego obecność wręcz go upajała szczęściem i zachwytem.

\-   Javierze... - szepnął pochylając się nad nim i całując jego skroń ostrożnie. - Zbudź się najdroższy... zaczyna świtać. 
\-   Naprawdę? - powiedział zaspany Javier. - Przyrzekłybym - ziewnął- że jeszcze przed chwilą całowałem cię na dobranoc. Śniło ci się coś najdroższy? Bo mi dawno się tak spokojnie nie spało jak tej nocy.
\-   Nie wiem, ale wiem za to, że chciałbym tak budzić się po każdej nocy. - Nigdzie mu nie było spieszno, budził się wcześnie i wybywał wtedy na łowy, tym razem nie miał najmniejszego zamiaru nawet wstawać.
\-   Czasami sobie myślę, że byłoby całkiem miło nie musieć uciekać chyłkiem rano z komnaty, nie bawić się w romanse z kobietami i być z tobą bez tego fałszu\...

Przy oczach hrabiego pojawiły się delikatne zmarszczki od uśmiechu, nie był to do końca wesoły uśmiech, raczej mieszanka słodko-gorzkiego grymasu. Poprawił czułym gestem rude loki spadające Javierowi na czoło.

\-   Powinniśmy spełniać swoje role i mieć w poważaniu konwenanse, mój młody przyjacielu. Dziedzicem moich rodzimych ziem ma zostać Sloan, ale w niedalekiej przyszłości Twoim obowiązkiem będzie przedłużenie twojego rodu. Nie wiem, czy w tym wszystkim jest miejsce dla nas. Natomiast może masz rację z tą stolicą, może powinienem pomyśleć o niej poważnie\....
\-   Nie jesteś dziedzicem, a cesarz potrzebuje mądrych ludzi. Nie pora na taką rozmowę, ale nie dzieje się dobrze ani w stolicy ani w otoczeniu cesarza. Kochany, ale warto pomarzyć i pomyśleć o tym, że raz coś ustalone nie będzie na zawsze. Czy zawsze jeździliśmy konno jako ludzie, a może najpierw jednak ktoś tego konia oswoił i dał mu siodło? Kiedyś było inaczej, teraz opowiadamy o tym bajki dla łatwowiernych dzieci. Ode mnie mądrzejsza jest i bardziej wprawna do przejęcia władzy najstarsza siostra. Zamiast tego rządzić będę ja, a ją ród straci na rzecz jakiegoś sapiącego grubasa, który nie zrozumie jaka jest wspaniała. Boli mnie to, najdroższy.
\-   Świat nie jest sprawiedliwy. - Przytaknął Maksimiliam, w głębi serca pragnąc wierzyć, że mogą być razem szczęśliwi, na przekór powinnościom i obowiązkom, w idealnym wyobrażeniu godząc jedno z drugim. - Może razem kiedyś zamieszkamy w stolicy? Nikt tam nie będzie o nic pytał.  

Odsunął grubo pleciony, zielono-żółty koc oraz ciepłą, baranią skórkę, aby móc pochylić się nad Javierem i pocałować jego brzuch oraz delikatnie zarysowaną linię kości biodrowych. Uśmiechnął się do swoich myśli i usiadł przyglądając się Javierowi z góry.

\-   Pamiętasz kiedy pocałowałem cię po raz pierwszy? Było wyjątkowo ciepło, a ja do dziś pamiętam to twoje spojrzenie, oczy wystraszonej łani. Zakochałem się wtedy w tobie do szaleństwa, w tych oczach.

Javier zaskoczony tym wspomnieniem, wyglądał nagle na zawstydzonego.

\-   Pocałował mnie nagle starszy ode mnie mężczyzna, który mi imponował, jak miałem się zachować? - Powiedział zarumieniony i poprawił koc, jakby chciał się nieco schować pod niego. - Skąd miałem wiedzieć, że fascynacja była czymś więcej niż podziwem i chęcią naśladowania? Ty natomiast zdobyłeś się wtedy na największą odwagę jaką widziałem i to mi strasznie się wtedy spodobało.
\-   Szczerze mówiąc, chyba nie myślałem wtedy nad konsekwencjami. Byłem z ciebie dumny i chciałem ci to pokazać, ciebie także rozpierała duma, złowiłeś wtedy swojego pierwszego, ogromnego pstrąga, byłeś cały mokry, stojąc po kolana w lodowatym strumieniu. - Przeczesał swoje kręcące się, jasnobrązowe włosy palcami, wracając myślami do tamtej chwili. - Twój ojciec myślał, że czegoś cię nauczę, a nie, że zacznę uwodzić...
\-   Cóż, czegoś się wtedy nauczyłem... i ciebie też. - Powiedział to i poprowadził rękę Liamia po swoim torsie, przez brzuch aż do wymęczonego przez całą noc penisa który jednak wskazywał na nowe pokłady sił. - Pokażesz mi znowu jak łowi się te wielkie pstrągi?

Maksimiliam roześmiał się.

\-   Jesteś nieprzyzwoity. Wyhodowałem na swej piersi potwora, poza tym... chyba niczego więcej nie mogę cię już nauczyć.

Usiadł na łóżku, pozwalając hrabiemu podziwiać swój apetyczny tors, kiedy przeciągał się.

\-   To co, mam się wykraść czy trzymamy się wersji, że pracowaliśmy do późna nad księgami? - Mówiąc to skinął na stos ksiąg przy stoliku.

Szarość dnia powoli wlewała się do komnaty przez zielonkawe szkło (najlepsze, praktycznie bezbarwne znajdowało się w szklarnii, co wprawiło ród de Falco w mały dług, spłacany jeszcze przez dwa pokolenia, spłacane głównie samymi płodami owej szklarnii).

\-   Powiedz mi też, czego właściwie dowiedziałeś się z ksiąg? Kim były istoty zamieszkujące to miejsce przed ludźmi?
\-   Najchętniej zostałbym w łożu cały dzień, a przynajmniej solidną połowę. Taki podarek życzyłbym sobie od ciebie na najbliższe me święto. - Wyznał, również przeciągając się i zakładając ręce za głową, z tej perspektywy wygodnie mógł przyglądać się sylwetce Javiera. Czuł tępy, delikatnie wyczuwalny ból w udzie, ale nawet owe uczucie nie było w stanie zepsuć tego rozkosznego poranka. - Nie dowiedziałem się zbyt wiele, a jeśli chcesz usłyszeć o owych istotach... musisz się tu zbliżyć. 
\-   Chętnie się zbliżę, ale oby to nie był podstęp - powiedział to zalotnie i zbliżył się do kochanka, nie zauważając lekkiego skrzywienia się na twarzy Maksymiliana.{.c18}

<!-- -->

\-   Większość z tego, co znalazłem to baśnie i legendy, których słuchałem jeszcze będąc dziecięciem, babka opowiadała te historie mnie i Sloanowi. Zawsze występowały w nich pradawne istoty, wysokie i mądre, z zamszowym porożem, którego nie powstydziłby się okazały jeleń. Decydowali o losach świata, gdyż proch starty z rogów pozwalał poznać nadchodzące wydarzenia, w niektórych opowieściach także je zmieniać. - Mówiąc to delikatnie gładził jego skórę, szyję i ramię, gdzie znalazł kolejne piegi, wyraźnie widoczne na białej skórze. - Kiedy władca istot starł swoje poroże w pył, postanowili odejść, predestynując do władzy ten z ludzkich rodów, który najbliższy jest naturze i najlepiej ją rozumie.
\-   Naturze tych istot czy lasach, jeziorach i tym co w nich żyje? - Żachnął się. - Czego ty szukasz, oznak starości na mojej skórze? W każdym razie, pozostaje tylko chyba zrobić eskapadę do jaskini. Ty odpadasz, ale mnie nie uśmiecha się tam iść w pojedynkę. Masz jakiś pomysł? Albo może jednak nie ma po co się tam wybierać?{.c18}

<!-- -->

\-   Oczywiście, że dałbym radzie tej eskapadzie nie gorzej niż ty. - Odpar. - Wybrać mogłaby się D'oh albo mój młody medyk Gregory, zna się na florze i faunie być może, byłby przydatny. Myślę iż przyczyną szaleństwa mogą być jakieś grzyby lub znalezione i rozbite w jaskini mikstury. Doprawdy wolałbym, abyś się tam nie wybierał, nie wiadomo co może czaić się w środku.

Javier już opuścił łóżko i ubierając się kontynuował.

\-   D'oh cóż, też chodzi o lasce, tak jak i Ty Liamie. - Mówił wkładając koszulę. Trzeba przyznać, że ubierał się równie szybko (albo i szybciej) jak się rozbierał. Miał w tym niepokojącą wprawę. - Włóż coś na siebie, nie dawajmy choć cienia podejrzeń. Co do Gregorego, ufasz mu rozumiem na tyle, że nie piśnie słowa nikomu? Chociaż w sumie co nam pozostaje w takiej sytuacji? Jaki on jest?
\-   Ufam mu bardziej niż Sloanowi, ale mniej niż tobie. - Odparł dosyć lakonicznie i niechętnie opuścił łoże oraz z równie małym entuzjazmem odział się, przyglądając się w zwierciadle swojej zarośniętej już nieco twarzy. Kiedy ubrał się, zaczął wybierać ze szkatuły odpowiednią sobie biżuterię. - Niestety, myślę iż powinniście jeszcze dziś rozważyć zejście.
\-   Poważnie chce wiedzieć jaki on jest. To nie tylko kwestia zaufania, ale i tego jak on się zachowa w różnych sytuacjach. Tam może być wszystko, nawet ten cały ich król.
\-   Zatem powinieneś go poznać, Javierze. Może przy śniadaniu? I wiesz co? To doskonały pomysł, jest poniekąd mym obecnie mocno zaniedbanym gościem, powinienem mu zaniechanie wynagrodzić chociaż wspólnym posiłkiem. 

Javier poprawiał właśnie swój kaftan i układał swoje rude włosy przed zwierciadłem zdobionym na brzegach motywami sokołów.

\-   Liamie, a Ty wiesz właściwie, dlaczego Twój ród nazywa się sokolim? Oczywiście nie mówię tutaj o oficjalnej opowieści w której po prostu wywodziliście się ze znamienitego rodu sokolników za co ówczesny władca dał Wam te ziemie i tytuł?

Mówił to, czesząc włosy własnym grzebieniem. Zawsze to robił, mówił, że inne grzebienie nie są tak dobre jak jego. Prawda była taka, że był to prezent od jego nieżyjącej już matki.

\-   Inna wersja nie jest mi znana, mój drogi. - Odparł hrabia, być może nie znał innej wersji, ale możliwe też, że interesowała o ta, o której myślał jego kuzyn. 
\-   Opowieść jest niczym z bajki, ale łatwo można zapomnieć, że kiedyś nie była to nasza kraina ani nasz świat. Żyło tutaj znacznie więcej stworzeń niż teraz i nie chowały się one po lasach. Jednym z takich stworzeń były orły królewskie, tak wielkie, że mógł się dosiadać człowiek, i tak mądre, że nie pozwalały na to. Założyciel rodu znalazł jednak sposób, aby przekonać ich władcę do sojuszu i tak stał się pierwszym władcą niebios. Podobno sojusz nie trwał długo, ale najstarsza księga którą miałem w rękach mówiła o jakiejś strasznej wojnie. Może zatem znajdziemy w szczelinach coś więcej o tym, co się wtedy stało, albo raz na zawsze rozwiejemy co jest bajką a co prawdą?

Światło już śmiało wpadało do komnaty, Javier był już świetnie przygotowany do nowego dnia.

\-   Cóż, w takim razie poznajmy tego Gregorego. Biedny on, zaniedbany, czego będzie go uściskać gorąco i uraczyć paroma banałami o tym, jak jest ważny. No bo jest, w końcu jest medykiem a teraz jeszcze grotołazem.
\-   Wszystko prawda, ale nie przesadzałbym z tymi gorącymi uściskami. - Skwitował rozbawiony, mając Javiera za skończonego łotra, któremu przez urok wybacza się stanowczo zbyt wiele niż powinno. Nim wyszli, uchylił oko, aby zaduch pomieszczenia uleciał.



## Plan jest taki
**Postacie:** Gregory, Liam, 



Służba przygotował doprawdy znakomite śniadanie, wędzone sery, przepiórcze jajka, chleby i kołacze, oraz pachnące przyprawami soczyste szynki były w stanie nasycić wielość zmysłów. Maksimiliam nie wtrącał się nazbyt w rozmowę, ale był w wyraźnie lepszym nastroju niż kiedykolwiek w minionym tygodniu, skubał zamyślony kawałek treściwego chleba z makiem. Jak zwykle w rozmowach brylował Javier, był w stanie całą wyprawę przedstawić jako wspaniałą, popołudniową przygodę.

\-   Gregory. - Przerwał w końcu wywód przyjaciela sam hrabia. - W istocie zależy mi, abyś towarzyszył Javierowi w tej eskapadzie, wierzę w twe możliwości i ufam twej wiedzy, poza tym nie raz dowiodłeś swej lojalności, a nie wszystkim tutaj można zawierzyć. To moja prośba, czy zechcesz towarzyszyć Javierowi w wyprawie i uczynić mi tym uprzejmość? Jeśli proszę o zbyt wiele, powiedz.
\-   Ależ... -zaczął Javier, ale Liam przerwał mu podnosząc dłoń.
\-   Nie. Tak naprawdę nie wiem co może kryć się w skalnych korytarzach, mój ojciec widział tam więcej niż pragnął. Nie chcę Gregorego do niczego zmuszać, to musi być jego własna decyzja, jeśli nie zechce, udam się tam we własnej osobie.

Gregory słuchał tego z kamienną twarzą. Czuł się coraz gorzej odkąd tu przyjechali, w czym nie pomagało mu fakt, że został pozostawiony sam sobie zaraz po przyjeździe i krótkim momencie w którym został poproszony o wysłanie gońcem wiadomości. Zwiedził chyba każdy zakątek tego zamku, a raczej upiornym lokum który tylko w przypływie wyobraźni można było nazwać przytulnym domem. Na przykład odkrył, że stół w głównej wielkiej hali opiera się z jednej strony, tej bliżej tronu, na kamiennym podeście. I ciągle wokół niego jest plama naprawdę, starej starej krwi. Albo ten podział na naprawdę duże przejścia, dla olbrzymów mających tak z mała półtorej razy większe rozmiary niż człowiek. Natomiast mniejsze przesmyki były tak naprawdę tymi o normalnej wielkości, gdzie w większości były oddane służbie. Jakby wszystko było zrobione w ten sposób, że to ludzie byli służącymi jakiejś rasie panów, która górowała nad nimi nie tylko metaforycznie. I ten dziwny chłód, jakby zimne macki ciągnęły się do trzewi...

\-   Panie, wolałbym tam nie schodzić w innej sytuacji, ale rozumiem, że sprawa jest poważna i nie mamy większego wyboru.

Gregory płakał w środku. Nie miał ze sobą księgi, która została na wieży. Bał się trochę tego, jaki miała na niego wpływ, ale z drugiej strony jej zawartość zdecydowanie by tutaj pomogła. Z drugiej strony, to czego się z niej dowiedział nie miał za bardzo ochoty pokazywać ani hrabiemu, ani co dopiero jego przyjacielowi. Bał się, ale co miał zrobić.

\-   Tak zupełnie nie wiadomo co tam może być? Tak nic a nic? Żadnych podejrzeń?
\-   Cóż... - zaczął ostrożnie hrabia, odkładając niedojedzony kawałek kromki. - Może być tam coś z bogatego spektrum roślin i porostów, mogą też być... na przykład pomieszczenia, podobne tym, które zamieszkuje mój ród. Mogą też być, w absolutnej ostateczności, ale nieco wątpię, dawni mieszkańcy tych krain. W każdym razie nie przewidywałbym zagrożenia wprost, takiego z którym trzeba walczyć. Potrzebny natomiast jest bystry wzrok i równie bystry umysł, Gregory. Proszę o pomoc ciebie, gdyż ufam ci mocniej niż większości moich krewnych... Javier jest dobrym towarzyszem, a ty bystrym naukowcem. Uzupełniacie się.
\-   Zatem zrobię trochę naparów leczniczych zanim tam zejdziemy na wypadek gdybym wrócił w stanie, który nie pozwoli mi na sporządzenie ich wtedy. Teraz przepraszam, ale muszę iść się pomodlić, jeżeli Panom to nie przeszkadza. Będę gotowy około południa, natomiast nie posiadam za dużo rzeczy, które przydałyby się przy chodzeniu w dół jaskini.
\-   Dziękuję, Gregory. O potrzebny sprzęt sam się postaram.



## W ogrodzie myśli

**Postacie: ** Irvette, Liam, Fenilla, 

Wewnętrzny ogród był naprawdę piękny, w powietrzu unosił się soczysty zapach zieleni, ziół i kwiatów, poza tym było ciepło i wilgotno. Maksimiliam korzystając z okazji, że wszyscy zajęci byli swoimi sprawami, postanowił zajrzeć właśnie tutaj, był zaprawiony już w boju trudnych rozmów, więc obecność Irvette nie była niemiła.

\-   Dzień dobry, Irvetto. Tak myślałem, że cię tu spotkam. Czyżby to była moja urocza bratanica o której tyle słyszałem? - Zagadnął trochę Irvette, a trochę samą, małą dziewczynkę.
\-   Tak. - Odpowiedziała zaskoczona Irvette. - znaczy\... dzień dobry Liamie, poznaj Fenellę. Fanello to jest twój wujek...

Flanella patrzyła na Maksimiliana z pewną dozą podejrzliwości, niczym śledczy, któremu fakty burzą idealną układankę. Myślała sobie bowiem, że ten oto mężczyzna wcale jakoś nie wygląda na fajniejszego od jej tatusia, który był prawie-już-władcą-zamku i był duży jak góra i podnosił ją do góry, co bardzo lubiła. Ten tutaj nie wyglądał na takiego, który podniesie ją wyżej niż tata, zwłaszcza, że ma laskę. Aa, nie. Co też mama w nim widzi, że tyle o nim ciągle opowiada?

\-   Dzień dobry. - Powiedziała uroczo, grzecznie i dygnęła z gracją 5 letniego dziecka, który rozumie powagę oficjalnych spotkań. - Mama bardzo dużo o panu mówiła.

Irvette spłonęła.

\-   Taaak, Fenella lubi mówić wprost pewne rzeczy. - Przytuliłą dziewczynkę. - Po kim to masz?
\-   Taki blady chłopiec mówił mi, że mam być zawsze szczera, bo inaczej spotka mnie kara. Wyglądał poważnie jak to mówił.

Irvette pomyślała, że znajdzie w końcu tego małego sługę, który sączy piękne, ale nie praktyczne uwagi do serca jej córeczki i podziękuje mu za jego pracę raz na zawsze.

\-   Co Cię tutaj sprowadza?
\-   Miło mi cię poznać, Fenello. Nie jestem "panem", a twoim stryjem i wierz mi, słyszałem o tobie pewnie równie wiele. - Odpowiedział Maksimiliam, oczywiście nie mając pojęcia o rozmyślaniach małego dziecka, ale będąc dziewczynką uprzejmie zainteresowany. - Wszystko okazuje się być prawdą; jesteś piękna i mądra, milady. 
\-   Dobrze, Stryju! Co ci się stało w nogę? - Zaatakowała jeszcze bardziej bezpośrednio, skoro kazał przejść na bardziej swobodną stopę.
\-   To rana wojenna, moja bratanico. - Oparł z błąkającym się po ustach uśmiechem. - Pamiątka z jednej bitwy. Razem z matką zajmujesz się tym ogrodem, czy masz inne rozrywki, panienko? - Odbił pytanie równie zgrabnie, opierając się na lasce, której rękojeść stanowiła rzeźbione drewno, a reszta posrebrzaną podstawę. Na chwilę podczas tej rozmowy przeniósł wzrok na Irvettę i posłał jej lekki uśmiech.
\-   Szkoda, bo to oznacza, że mnie nie podniesiesz. Ale wyleczysz się z tego? - Zapytana odparła - Rozrywka? No nie wiem, to ciężka praca, bo mama mówi, że darmozjady nie dostają jedzenia. Nie chcę być darmozjadem.  - Mówiła nie zauważając paradoksu, który właśnie wygłosiła. - Ale czasem idę do psów i koni - powiedziała konspiracyjnym szeptem. - D'oh jest fajna i szczera. To fajne, bo mnie lubi. Gorzej gdyby nie lubiła i była szczera. A może to też dobrze by było, bo wiedziałabym, że mnie nie lubi i mogłabym ją unikać. Albo pozbyć się intrygą... - zasapała się aż, kiedy tyle mówiła.
\-   Masz rację, to ciężka praca, ale twa matka ma rękę do roślin, zna się na nich jak nikt inny. A poza tym, widzę, że coś nas łączy, Fenello, obydwoje lubimy psy, konie, oraz szczerą D'oh, poznałem ją kiedy miałem tyle lat co ty teraz. Bardzo możliwe, że następnym razem spotkamy się w psiarni. Nie pogniewasz się, jeśli poświęcę teraz trochę czasu na rozmowę z twoją mamą?
\-   Tak, ale co zrobić?  - Powiedziała i pobiegła w inny zakątek ogrodu.
\-   Pomyślę jak wynagrodzić ci twą wspaniałomyślność, milady. -  Odparł nim dziewczynka pobiegła zająć się czymś, co absorbuje dzieci w jej wieku. Usiadł na kamiennej ławeczce, starając się aby grymas na jego twarzy nie był zbyt widoczny. Miejsce wokół rany rwało go, ubiegłej nocy nieco nadwyrężył mięsień w skutek czego od rana towarzyszył mu nieustępliwy chociaż tępy ból. - Masz wspaniałą córkę, Irvetto. 
\-   Chociaż tyle.  - Powiedziała z przekąsem - Tego życia nie wybrałam, mogę chociaż uprawiać ten ogród jak chce, wychowywać córkę jak chce... Chociaż to mi się nie udaje, bo jest bardziej bezpośrednia niż ja. Z jednej strony cieszy to, ale świat chyba nie jest gotowy na bezpośrednie i odważne kobiety. - Zmartwiłą się - Wyglądasz blado, noga Ci dokucza? Jejku, to moja wina, wysłałam Cię na poszukiwania Sloana, przepraszam!
\-   Fen jest uroczym dzieckiem, nie pozwól, aby ktoś gasił jej odwagę i pewność siebie. - Poprosił miłym tonem, wyciągnął dłoń i przesunął palcami po liściach soczystej, najbliżej rosnącej rośliny, uwielbiał jej świeży zapach, szczególnie zimą przebywanie tutaj było szalenie przyjemne. - Moją nogą się nie przejmuj, to nie twoja wina, tutaj po prostu dużo więcej się ruszam. Nic nie jest twoją winą, Irvette. Jesteś wspaniałą osobą.

Ogród był piękny i nie ulegało wątpliwości, że był królestwem Irvette, miała do tego niesamowity zmysł, wszystkie prace z roślinami odbywały się pod jej czujnym okiem.

Uspokoiła się po słowach ukochanego.

\-   Ty też Maksymilanie. Pamiętam jak dużo czasu spędzałeś ze mną tutaj przed moim ślubem i Twoim wyjazdem. Powiedz, tak bardzo lubisz to miejsce, czy robiłeś to ze względu na mnie?
\-   Z obu powodów, Irvetto. - Przeniósł na nią spojrzenie swoich szaro-zielonych oczu, uśmiechnął się miło, a zmarszczki przy oczach stały się na chwilę nieco wyraźniejsze. - Uwielbiałem słuchać z jakim zapałem wszystko planujesz, zwracasz uwagę na kąt padania światła, miejsca zacienione. Wyglądało to tak, jakbyś wszelkie zmiany i plany miała bardzo dokładnie przed oczyma. Poza tym, wybacz że to mówię, może nie powinienem... ale tylko tu wydawałaś się być swobodna. Wtedy Sterling chyba jeszcze trochę cię przerażało. 
\-   Myślisz, że to się zmieniło? Może nie przeraża, ale nie czuję się poza ogrodem swobodnie. Jestem jak na doczepkę, mój mąż szykuje się na bycie następcą, ale sporo upłynie zanim będę faktyczną panią tego zamku, bo {.c18}Kylie jest znacznie lepszego zdrowia niż nasz obecny władca a twój ojciec. Co do roślin, to po prostu czuję czego one potrzebują.
\-   🔹Jeśli o Sterling będziesz dbała tak jak o ten ogród, będziesz wspaniałą panią. Kylie jest też twoją rodziną, nie jest łatwą osobą, to prawda, ale zrozumie twoją rolę w tych włościach.  
\-   Nie mam nic przeciwko niej, Liamie. Rozmawiamy nawet na swobodnej stopie, ale nikt nie szkoli Pani zamku tak jak się szkoli Pana. Właściwie to ciebie nikt nie przeszkolił do bycia Panem na twojej twierdzy, ale dostałeś chociaż Grota do pomocy, a on tyle potrafił....
\-   🔹Porównujesz swoją sytuację do mojej? Ja ciągle jestem synem władcy, Irvetto, nie mniej wykształconym i obytym niż mój starszy brat. Jedyna różnica między nim a mną polega na tym, że jest pierwszym do objęcia władzy. To, że nie chcę ze Sloanem rywalizować nie sprawia, że jestem mniej kompetentny - odparł spokojnie, ale w jego głosie słychać było lekką, prawie niezauważalną irytację. - Dobry sługa to skarb, ale ciągle jest tylko sługą.
\-   Przepraszam, nie chciałam Cię urazić. 

 Milczała chwilę.

\-   Chciałam tylko zauważyć, że ojciec chyba jednak poświęcał Ci tyle czasu ile bratu, skoro już wtedy obmyślił, że to on przejmie władzę. Miło mi, że mi doradzasz z kim powinnam się lepiej porozumieć, ale kiedy wyjedziesz stąd, nie będę miała ani towarzysza ani wiernego sługi, który mógłby mi doradzić. Chyba, że nie cofasz swoich słów...
\-   🔹Wybacz, że się uniosłem. - Hrabia Sokół odetchnął głęboko, faktycznie czując ulatujący powoli gniew. Frustracja czasem powracała, zupełnie jak ból w udzie, kiedy uświadamiał sobie, że dobroć ludzie poczytują czasem za słabość charakteru. - Nie doradzałem ci, w każdym razie nie to było moją intencją, stwierdziłem jedynie, że pani Matka jest mądrą kobietą oraz, że rozumie twoją rolę w tym domu... i nie puściłem mimo uszu twojego pytania, Irvetto. - Odwrócił się bardziej w jej stronę, aby nie miała wątpliwości, że traktuje ją poważnie. - Posłuchaj. To, co ci powiedziałem nie było tylko słowami pocieszenia. Jesteś jedynym elementem życia Sloana, który mu zazdroszczę. Nie docenia cię tak jak powinien, nie darzy cię szacunkiem którym powinien. Nie zasłużył na ciebie, lecz... nie twierdzę, że ja zasługuję. Nie zapewnię ci nigdy komfortu życia, który powinnaś otrzymać, sądzę, że nie jestem też najlepszym mężem. Znasz mnie takim, jakim byłem pięć lat temu, oraz z paru listów. Zastanów się czego chcesz. Chcesz, abym dotrzymał słowa, czy wolisz mego towarzystwa wieczorem?

Irvette milczała wpatrzona w Hrabiego, ale jej wzrok wyrażał radość i umiłowanie, nie zaś pokerową twarz osoby która chciała by nim manipulować.

\-   Kochany - wyszeptała - Chcę Twojego towarzystwa wieczorem. Tego, kolejnego i każdego. Kocham Cię i z Tobą chcę żyć choćby w małym domku daleko stąd. Boli mnie, że tak się nie doceniasz, kiedy ja chcę abyś tylko spojrzał na mnie wzrokiem pełnym uwielbienia i zrozumienia. 

Nie bacząc na to, że córka może to zauważyć, pocałowała Liamia. 🔹Maksimiliam napiął momentalnie mięśnie, nie spodziewając zupełnie takiej jej reakcji Irvette i powoli odsunął się od niej, nie potrafiąc ukryć zaskoczenia.

Irvette patrzyła się na niego zaskoczona.

\-   Liamie, przepraszam, tak bardzo chciałam to zrobić...
\-   🔹Ja... Nie spodziewałem się.
\-   Proponowałeś mi właśnie wspólną noc. Stresuje Cię to, że ktoś nas tutaj zobaczy? I co z tego, przecież obiecałeś, że mnie stąd wyrwiesz.

🔹Odetchnął powoli, spuszczając wzrok i wbijając go w bliżej nieokreślone miejsce.

\-   🔹Dotrzymam słowa. Jesteś dla mnie bardzo ważna, wiesz o tym. Jesteś moją... przyjaciółką. - Mówiąc to, spojrzał na nią. - Najbliższą jaką kiedykolwiek miałem. I... W związku z tym... nie jest mi łatwo - urwał przygryzając wargę zakłopotany.
\-   Przyjaciółką? Tylko przyjaciółką? Nie pożądasz mnie?
\-   🔹Irvette... Posłuchaj, starałem się nigdy w inny sposób o tobie nie myśleć. Na początku owszem, marzyłem o tobie, ale zostałaś żoną mojego brata. Później spodziewałaś się dziecka - wzruszył ramionami. - Ułożyłaś sobie życie tutaj, przynajmniej tak myślałem, więc ja też zająłem się swoim życiem, Twierdzą, Prianką. Schlebia mi twoja atencja, twoja przychylność i zaangażowanie, ale było dla mnie tak niespodziewane... że nie wiedziałem jak zareagować na pocałunek. Nie odtrącam cię.
\-   Może faktycznie... za szybko. - Mówiła niepewnie, dotykając odkryty dekolt swojej ciemno zielonej sukni gdzie ze zdenerwowania wykwitły jej czerwone plamy. - Myślałam, że każdy mężczyzna jest gotowy do miłości o każdej porze i miejscu. Nie mam w tym wiele doświadczenia, zawsze był tylko mój mąż i nikt więcej.

🔹Zaśmiał się, czując że niepotrzebnie się stresuje. Wziął jej dłoń, tą którą poprawiała krawędź sukni i ucałował, ale nie puścił od razu, trzymał jej rękę w dłoniach i gładził.

\-   🔹Przepraszam. Naprawdę. Ciągle absorbują mnie tutejsze sprawy, Javier postanowił zejść do powstałej szczeliny, martwię się o niego, razem z nim zejdzie mój medyk, a ja nie wiem co jeszcze mógłbym zrobić, aby byli bezpieczni.
\-   Dobrze, rozumiem. Tyle robisz dla mnie, a ja jestem niewdzięczna, oskarżam Cię i zrobię Ci wyrzuty. Wybacz.

W tym momencie podeszła do nich Fen z zaciekawionym i nieco psotnym wyrazem twarzy.

\-   Mamo, mamo! Dlaczego pocałowałaś wujka?
\-   
\-   🔹Z wdzięczności. - Odparł hrabia i podniósł się powoli. - W końcu przyjechałem tu, aby pomóc w paru sprawach. Fen, posłuchaj, bardzo się cieszę, że udało mi się ciebie poznać. I wiesz? Zupełnie nie spodziewałem się, że jesteś taka duża, istotnie myślałem, iż poznam małą dziewczynkę, tymczasem wyrosłaś już na całkiem dużą damę. Niestety muszę już iść, ale zapewne się spotkamy dosyć szybko, jednak z suk w psiarni, oszczeniła się z tego co mówiła Do'h. - Uśmiechnął się do dziewczynki. - Wybaczcie zatem. - Dodał tym razem bardziej formalnie i skłonił się na pożegnanie, nieznacznie, ale zgodnie z dobrymi manierami.

## Przygotowania
**Postacie: ** Liam, Gregory, Javier

🔹Hrabia wrócił na chwilę do swoich komnat aby przebrać się przed obiadem. W pomieszczeniach ciągle utrzymywała się ta sama temperatura, zatem nie mógł narzekać na chłód. Obmył twarz w kutej, miedzianej misie, lecz ablucje wcale nie pomagały w oczyszczeniu umysłu od natłoku myśli. W swojej twierdzy spotykał w większości tylko służbę, same znajome twarze i chociaż serdecznie to nie wymagające interakcji, tutaj było zupełnie inaczej. Sokołowi wydawało się nawet, że ilość problemów oraz natłok rozmów sprawiają, że od dnia przyjazdu czuł się chronicznie zmęczony, bez względu na ilość snu.

🔹Zatem dając sobie chwilę czasu dla samego siebie, powoli przygotował parę przedmiotów, które mogłyby okazać się użyteczne podczas zejścia do jaskini, po parę innych musiał posłać służbę, ale skórzany, lekki worek pomieścił całkiem sporo fantów i z nimi dopiero hrabia Sokół zastukał do drzwi Gregorego.

🔹

\-   🔹Przygotowałem parę rzeczy, które mogą okazać się przydatne. - Obwieścił, gdy już wszedł do środka, i chociaż dręczyły go lekkie wyrzuty sumienia, kontynuował. - Tu masz odzienie, w którym będzie ci wygodniej, przyniosłem też mój sztylet... Jest ostry, więc poradzi sobie z pnączami i linami - uprzejmie wcale nie wspomniał o żadnych istotach żywych - możesz go przypasać.

Wziął nóż do ręki i ważył go w dłoni przez chwilę.

\-   Przybyłem tutaj jako medyk, wolałbym aby tak to zostało. Nie powinniśmy iść tam większą grupą?
\-   🔹 Wolałbym wybrać się tam zamiast ciebie.
\-   Panie, ja tylko chciałbym wiedzieć trochę więcej zanim tam zejdę. Przepraszam za śmiałość, ale prosiłbym o więcej szczegółów co tutaj się dzieje skoro wygląda, jakbym ryzykował życie w tajemniczej wyprawie...

🔹Hrabia usiadł na brzegu ławy zbierając myśli i zastanawiając się od czego zacząć. To, co Gregory mógł stwierdzić z całą pewnością, to to, że Maksimiliam chociaż nie uskarża się na ból w nodze, to wygląda na wiecznie zmęczonego, o czym świadczyły cienie pod oczami.

\-   🔹Mogły dość cię, Gregory, słuchy o szaleństwie mego ojca - zaczął szlachcic. - Nie są to opinie bezpodstawne, gdyż faktycznie pan Sterling postradał zmysły, czas jego panowania jest zatem mocno ograniczony. Pewnie wyobrażasz sobie, iż dziedzicem jest Sloan z racji starszeństwa, z tej okazji też zjawiliśmy się tutaj. Myślałem, że przyjdzie mi pożegnać ojca i uczcić dzień, w którym mój brat zostaje władcą. Sprawy jednak skomplikowały się nieco, gdyż ojciec mój chce mnie wyznaczyć na dziedzica. Czas nagli, tym bardziej że za parę dni nasze rodowe ziemie odwiedzi cesarski wysłannik, jeśli szaleństwo mego ojca okaże się tak wyraźne jak tego się obawiam, przejąć władzę ktoś będzie musiał rychło. - Urwał na chwilę aby zaczerpnąć powietrza i westchnąć nad ciężarem spraw. - Nie wspomniałem jeszcze o wyrwie, prawda? Tu pojawia się miejsce, i o ile sprawa do tej pory nie przestawiała się łatwo, teraz komplikuje się jeszcze bardziej. Ojciec mój i jego sługa postradali zmysły dopiero po powrocie ze szczeliny, dowiedzieli się tam czegoś, zobaczyli coś, co odebrało im jasność umysłu. Tak przedstawia się zagrożenie związane ze szczeliną. Istotnym elementem jest absolutne przeświadczenie mego ojca, iż dwudziestemu piątemu dziecicowi Sterling pisana jest rychła śmierć. Doprecyzowując; mnie lub Sloanowi.

🔹Maksimilam spojrzał na Gregorego znacząco. I oparł łokcie na stole, przesuwając w zamyśleniu sztylet.

\-   🔹Nie jest mi spieszno na tamten świat, jak się zapewne domyślisz, nie chcę jednak równie mocno wysyłać tam mego brata Sloana. Nie wiem też gdzie leży prawda, wiem jedynie, że cesarski wysłannik mocno przyspieszy proces wyboru kolejnego władcy. Jeśli przepowiednia, w którą wierzy mój ojciec, okaże się prawdą... cóż. Kogoś z pewnością spotka nieszczęście. Opowiadam ci o tym wszystkim, Gregory, z bardzo prostej, acz smutnej przyczyny, jesteś jedną z nielicznych osób tutaj, której całkowicie ufam.

🔹Gregory milczał chwilę.

\-   Tak, doszły mnie słuchy, bo głównie przebywałem ze służbą w czasie tego pobytu. Nie jestem pewny, czy ta wersja historii pan hrabia opisał teraz jest najgorsza. Jedna mówiła, że ojciec władca jest podmieniony, bo jego miejsce zajął człowiek-jeleń. Przepraszam, że o tym mówię, to nie stosowane\.... W każdym razie, nie ma chwili do stracenia, zabieram się do zejścia na dół. Przepraszam, że tak dopytywałem się, ale wolałem wiedzieć z pierwszej ręki a nie opierać się na moich dzikich domysłach i pomylonych plotkach.

Gregory mówił tak, ale jego żołądek był zawiązany na supeł. Pozostawiony sam sobie w innych okolicznościach bawiłby się doskonale, ale cała ta sytuacja... Deryk, stary służący praktycznie nic nie robił odkąd tutaj przybyli. Spał tak często i długo, że może wreszcie się wyśpi na zapas i będzie znamienitym sługą. Zaś Ann... Ta dziewczyna przyprawiała go o ciarki, pogłębionych jeszcze ostatniej nocy, gdzie widział jak rozmawiała z dziwnym bladym młodocianym służącym. Próbował z nią rozmawiać, ale ona zupełnie nie reagowała, jeżeli naprawdę nie musiała, tak jakby tylko szanowała tylko hrabiego, a resztę mają w zupełnej powadze.



Chwilę później stali razem w trójkę razem z Javierem nad wyrwą w dawnej komnacie Maksymilania. Javier myślał teraz nie tylko o tym, co może ich spotkać, ale też o dziwnych kolejach losu, że to pod komnatą Liama znajdowało się wejście do jakiś starych komnat.

\-   Co tam Liamie, poczekasz tutaj na nas? Najlepiej z jakimś mieczem w ręku, na wypadek gdyby nas coś goniło?

🔹Wystarczyło zaledwie jedno spojrzenie na hrabiego, aby utwierdzić się w przekonaniu, że dręczą go solidne wyrzuty sumienia.

\-   🔹Oczywiście. Cały czas będzie tu ktoś na straży, będę przygotowany na różne ewentualności, ale na bogów, uważajcie na siebie. Nawet, jeśli mielibyście wrócić z niczym, to zdrowie wasze jest dużo dalej cenne niż informacje, które możecie zdobyć.

🔹Szlachcic pogładził w zamyśleniu i zdenerwowaniu swoją ładnie przystrzyżoną bródkę i nic nie mówiąc już, rzucił okiem na ekwipunek Javiera oraz Gregorego, aby upewnić się, że wszystko zabrali.

🔹

## Zejście
**Postacie: **Gregory, Javier


\-   Nie tego się spodziewałem - powiedział Gregory patrząc na porośnięte zielonym mchem korytarz do którego weszli chwilę temu.

Zejście z komnaty, które zostawili za sobą było zasypane gruzem i odłamkami posadzki. Wyglądało to po prostu na brutalne przekucie się z góry na dół. Ktoś musiał wiedzieć, że coś takiego znajduje się dokładnie w tym miejscu, bo grubość podłogi wykluczała sytuację w której np. ktoś zobaczył coś przez szczelinę i zaczął kopać.

Powietrze idące z korytarza było świeże, jakby pod spodem istniał system wentylacji, który albo wytrzymał setki lat nietknięty, albo był ciągle utrzymywany w tym stanie przez kogoś lub coś.

Mech może nie byłby rzeczą niezwykła, ale tutaj tworzył on ścisłą warstwę na ścianach i suficie, jak gdyby ktoś go stale przycinał czy w inny sposób wyrównywał. Gregory nie czuł się tutaj zbyt pewnie, bo o ile wcześniej mógł subtelny dotyk magii zwalić na karb wyobraźni, to tutaj wyraźnie czuł pulsujące jej źródło.

Dotknął delikatnie mchu i wtedy usłyszeli coś jakby westchnienie, jakby ulgi, którą widać u człowieka, którego napoi się wodą, gdy biegł.

🔹Javier oparł dłoń na rękojeści miecza, wolał zachować czujność, chociaż nie czuł się przytłoczony wielkością i kształtem korytarzy, właściwie wydawały mu się swojskie, znajome, w murach u góry spędzał od najmłodszych lat długie godziny, w końcu to dom jego przyjaciela.

\-   🔹Tak, któż mógłby się spodziewać, iż pod szczelną znajduje się cały kolejny kompleks pomieszczeń i korytarzy? - Zapytał w eter rudowłosy szlachcic, czując jak włosy na karku jeżą mu się, chociaż nie odczuwał teraz strachu. - Dziwne miejsce. Chodźmy dalej, szkoda czasu.
\-   Nie słyszał Pan tego westchnienia? - powiedział lekko przestraszony Gregory, czego jednak nie było widać w blasku zapalonych pochodni które nieśli ze sobą. Ich głosy nie niosły się echem ale były tłumione przez mech.
\-   🔹Jeżeli to nie było złudzenie, to powinniśmy się martwić bardziej niż sądziłem.



Ruszyli dalej i w milczeniu wędrowali roślinnym, ciemnym i wilgotnym korytarzem. Trudno było określić upływ czasu pod ziemią, bo to co mogło być zaledwie kwadransem, tutaj trwało niczym godzina. Czasami wydawało im się, że coś słyszeli tuż obok siebie, ale za każdym razem oględziny nic nie wykazywały.



Aż doszli do wielkiego zagęszczenia białych grzybów, których z racji bycia w wąskim korytarzu nie sposób było wyminąć.



\-   I tak po prostu mamy na swojej drodze grzyby? - Zapytał Gregory szeptem. Szeptali już od dłuższego czasu, jakoś tak odruchowo.

🔹Javier sięgnął po chusteczkę i zakrył nią usta, czubkiem miecza trącając jeden z kapeluszy, bacznie się rozglądał przy tym.

\-   🔹Posłuchaj, znasz te grzyby? Powinniśmy spróbować przejść nie dotykając ich, to one mogą być źródłem toksyn, jak sądzisz?
\-   Świetny pomysł - powiedział tylko zgryźliwie - ja ich nie przeskoczę. - Mówił to oświetlając przestrzeń przed nimi, aby ocenić odległość skoku. - Za daleko, tutaj trzeba naprawdę dobrego skoczka albo konia. Z tym, że konia nie mamy a jak to przeskoczysz, to będziesz musiał kontynuować wędrówkę sam, Panie.
\-   🔹Ha! To doprawdy wyśmienity pomysł z tym skokiem, mój drogi, wyśmienity! Tyle, że zamiast wierzyć w mięśnie naszych nóg, posłużymy się twoim kosturem. Co ty na to, Gregory? - zagadnął baron, całkiem dumny z tego, jakby nie patrzeć, wspólnego pomysłu.

🔹Nie czekając na aprobatę bądź dezaprobatę towarzysza, poprosił o jego kij. Skok o tyczce był najlepszym rozwiązaniem, chociaż rozważał także użycie liny zarzuconej na jeden ze stalaktytów, aczkolwiek w tak drążonym korytarzu stalaktyty mogłyby okazać się nie najlepszym wsparciem, ułamane mogły uszkodzić nie tylko grzyby, ale także ciała Javiera oraz Gregorego. Javier był bardzo przywiązany do swojego ciała i nie chciał go opuszczać w tak błahy sposób.

🔹Mężczyzna zacisnął kij w dłoniach, z miną pełną determinacji, ale po chwili determinacji ustąpiło zwątpienie, zdjął swoją torbę przewieszoną przez ramię.

\-   🔹Gdy przeskoczę, rzucisz mi moje i swoje rzeczy. Zbędny balast podczas skoku może o coś zahaczyć. - Zawyrokował, w tym całym zdenerwowaniu zapominając o kurtuazyjnym, acz nic nie znaczącym pytaniu, czy Gregory się na to wszystko raczy zgodzić.

🔹Nim nerwy zaczęły brać górę, a dłonie pocić się za bardzo, Javier wziął krótki rozbieg, koniec kija wbijając w ziemię i przeskakując nad grzybami w ciemniejący korytarz.

\-   🔹Aagh! - Jęknął, po bliższym spotkaniu z podłożem, ale zaraz dodał. - Nic mi nie jest! Wszystko w porządku.

🔹Zanim odebrał rzeczy, rzucone mu przez Gregorego obmacał kostki i łokcie, na jednym z kolan powstało jedynie zadrapanie. Ostrzegł Gregorego przed nierównym podłożem, ale medyk ze skokiem poradził sobie zaskakująco dobrze. Po chwili mogli iść już dalej, a Javier miał nieodparte wrażenie, że od pewnego czasu, pomimo panującej ciszy, czyjaś para oczu bacznie ich obserwuje. Para, lub więcej par. Być może, ta nieznana przestrzeń wzbudzała w nim nieuzasadniony lęk i wcale nie było się czego obawiać.

\-   🔹Hrabia Sokół zawsze mówi o tobie w superlatywach. Dobrze znasz Maksimiliama? - Zapytał w końcu szlachcic, aby nie myśleć o tym, co może czaić się za kolejnym zakrętem.
\-   Jego stan medyczny znam bardzo dobrze. Przybyłem już długo po jego kontuzji i muszę przyznać, że nie dobrze, że nie miał stałego medyka u siebie. Bo ten zabronił mu ruszać się zanim zagoi się jego rana. Teraz o ile nie zdarzy się cud, to za parę lat będzie miał bardzo duże problemy z poruszaniem, nie mówiąc o jeździe konnej. Staram się nauczyć cudów, ale to trudna rzecz... - Zamilkł i czegoś nasłuchiwał.
\-   Rzeka? - Zapytał.

Javier także przystanął i nastawił uszu, faktycznie, słychać było szum w oddali, ulotny dźwięk jednak jednoznacznie kojarzył się z wodą.{.c8 .c12}

\-   🔹Być może... Występują tu podziemne, ciepłe źródła. Pewnie niebawem się przekonamy. - Odparł rudowłosy i patrząc pod nogi, wrócił do poprzedniego tematu.
\-   🔹Pamiętam stan hrabiego od momentu wyjścia z gorączki, wtedy lokalny medyk czuwał już przy nim dniem i nocą, aczkolwiek gdy już mu się polepszyło, jakże trudno było Maksimiliama zatrzymać w łożu. Ciągle go gdzieś rwało, skoro świt, na łowy, na przejażdżkę, do charów... Ech, w sumie to mu się nie dziwię, że w domu nic go nie trzymało, chociaż głupio postąpił, to jednak demonicę, a nie żonę sobie sprowadził, a to to dobry człowiek, nie zasłużył sobie.
\-   Jestem u niego od paru miesięcy, ale Panią Priankę widziałem tylko raz dłużej a tak to jej ciągle nie było w twierdzy. Aż dziwi jak mogła my napsuć tyle krwi, jak jej właściwie ciągle nie było. Nie mam doświadczenia z kobietami praktycznie żadnego, ale chyba nie wiedział co robił, jak ją brał do siebie, Panie. Może mi zresztą coś doradzicie w tej kwestii, bo głupia sprawa, ale dziewczyny bardziej brały mnie za przyjaciela niźli kochanka.
\-   🔹O, wierz mi, mój towarzyszu, nic tak nie pociąga kobiety jak obojętność. - Uśmiechnął się szelmowsko rudzielec i zerknął na Gregorego, właściwie wcześniej nie zastanawiając się ile młodzieniec może sobie liczyć wiosen. - Im bardziej zajętyś swemi sprawami, poświęcasz uwagę, lecz nie za wiele, tym bardziej kobiety do ciebie lgną.

Gregory nie wyglądał najgorzej - miał ciemniejszą karnację, krótkie czarne włosy i jaśniejszy nieco zarost na twarzy. Taki nie powinien mieć problemów z kobietami, ale może ta sympatyczność, która biła od niego powodowała, że zamiast być obiektem westchnień, był ich tylko słuchaczem, kiedy to kobiety wolały użalać mu się nad swoim żywotem, zamiast go wzbogacić z użyciem Gregorego. Wysłuchał już trzy służki z ich problemów, w tym jedną, która nawet mówiła mu, jak kiedyś kochała się z synem władcy, a teraz on nawet nie pamięta jej imienia.

\-   Ale, ale to trochę nieuprzejme. Ja bym wolał, aby mnie nikt nie ignorował.
\-   🔹Och, oczywiście. Ignorowanie panny nie świadczy dobrze o mężczyźnie. Ale powiedzmy, że miałbyś dla nich mniej czasu... - Machnął ręką zmieniając nieco koncepcję, ale też z czystym sercem dzieląc się własnymi doświadczeniami. - Wiesz, Gregory, przystojną masz aparycję, niecodzienną, więc nie jest to kwestią odpychającej powierzowności, to pewne. Jeśli uwaga, którą dajesz panną nie wymaga od nich żadnego wysiłku, to sądzą zapewne, że więcej nie masz do zaoferowania. Może zabrzmi to jak banał, lecz... jeśli zajmiesz się swoimi sprawami, dziewki przyjdą same.
\-   Myślę, że Pan Hrabia akurat źle wyszedł na tej radzie, bo wygląda na takiego, który zajmował się swoimi sprawami i faktycznie jego żona do niego przyszła.

🔹Javier zachichotał, a żeby nie wybuchnąć śmiechem, po prostu zakrył usta. Śmiech przeszedł w zdawkowy kaszel.

\-   🔹Może po prostu od niej uciekał. - Podsunął mu inne rozwiązanie. Ponury nastrój i przytłaczająca atmosfera miejsca nieco zelżała dzięki tej rozmowie. - Gregory, naprawdę nie masz żadnego doświadczenia z kobietami?

🔹Chwilę milczał.

\-   Cóż, raz prawie mi się udało, ale kiedy taka dziewka chwyciłą mojego, no moją męskość, to eee doszedłem - Mówił zawstydzony, ale łatwiej mu było o tym mówić, bo raz że byli sami, dwa że w ciemności a trzy, że Pan Javier wydawał się być bardzo doświadczony. - No i Marlette, no tak się nazywała, śmiała się ze mnie. I mi ciężko nawet się zabrać do roboty, nawet jak w końcu jest jakaś okazja. No wstyd taki.
\-   🔹Ach. - Odparł lekkim tonem szlachcic. - Początki bywają trudne, nie musisz się tym trapić. Jeśli wyjdziemy stąd cało i rychło, to bogowie mi świadkami, wyślę do ciebie obrotną i doświadczoną dziewkę, najlepszą jaką znajdę.

Szmer wody narastał, aż w końcu szum stał się tak silny{.c18 .c8 .c34}, że musiał być już bardzo niedaleko. Kłopot w tym, że przed nimi znajdowała się zielona ściana ruchomych pnączy, które zdawały się wyczuwać ich obecność. Wyglądało to jak zwisające stado węży, blokujące przejście dalej swoimi zielonymi cielskami.

\-   Może zmieńmy to jeśli wyjdziemy stąd cało na wycofajmy się teraz? - Zapytał Gregory.
\-   🔹Może rozpalimy pod nimi ogień? - Zaproponował, nie bacząc na nieśmiałą sugestię Gregorego, właściwie ignorując ją całkowicie. - Albo może one same... - Javier ostrożnie zbliżył się do pnączy z pochodnią w ręce, aby sprawdzić jak dziwne rośli zachowają się.

Pnącza rozchyliły się, ukazując jasną, dużą otwartą przestrzeń za nimi. Wtedy Gregory krzyknął:

\-   Otaczają Cię!

Javier zauważył szybkim rzutem oka wokół siebie, że pnącza owszem odsunęły się, ale wyglądały jakby miały zaatakować go w kark. Faktycznie zachowywały się zdecydowanie bardziej jak węże z własnym rozumem.

🔹Javier okręcił się, wymachując pochodnią, przylgnął do ściany, aby chociaż jeden obszar był chroniony przez skały. Pochodnia syczała, sypiąc iskry, syczały też węże, po chwili, za ich zieloną ścianą znikł Javier.



Gregory zesztywniały stał patrząc się na ruchomą ścianę w samotności, ale kiedy usłyszał swoje imię które nagląco wołał Javier, zrobił dokładnie to co on, przeciskając się przy samej ścianie.

Wtedy też stanął oniemiały i razem z Javierem oglądali w podziwie wielką przestrzeń.



Było jasno, jaśniej niż w jakiejkolwiek komnacie w zamku nad nimi. Światło pochodziło z wielu kul rozmieszczonych po całym tym miejscu. Na ścianach widać było malowidła, zarówno wielkie jak i małe, z użyciem dziwnych znaków, ale część obrazów była jasna, jak gdyby ktoś zawarł historię w kolejnych, następujących po sobie malowidłach. Było na nich widać istoty z porożem, które tutaj były przedstawione jako królowie, posługujących się magią i czczonych przez małe istoty, które wydawały się być ludźmi.



Nie tylko to było w tym miejscu. Na środku było coś, co wyglądało na wielkie okrągłe łoże, przypominające nieco gniazdo. Wokół niego było dużo dziwnych przedmiotów, ale część wyglądała na starodowne stanowiska odczytywania zwojów.



Słychać było wyraźnie strumień albo rzekę, ale jedyne co widzieli to cicho skrzypiące i chlupiące drewniane koło młyńskie, jakby woda biegła tuż pod powierzchnią.



Najdziwniejszy był chyba sufit, który posiadał kilka przezroczystych kopuł wypełnionych wodą, w których pływały ryby. Jednak dopiero śpiew Irvette, wyraźnie dobiegający w jakiś sposób z góry uświadomił im, gdzie, albo raczej pod czym znajduje się to miejsce.



🔹Javierowi zaparło dech w piersiach, przestrzeń, jasność i odkrywające swą niezwykłość tajemnice tego miejsca przytłaczały, aż w końcu wydawało mu się, że nie słyszy wody, tylko własną krew tętniącą w żyłach. Szlachcic zwilżył językiem spierzchnięte od emocji wargi i ogarnął wzrokiem malunki, pragnąc odgadnąć ich sens.

Główna historia na malowidłach wydawała się przedstawiać panowanie i upadek ludzi-jeleni. Panowanie wyglądało ma bardzo krwawe, ze składaniem ofiar z ludzi i zwierząt. Upadek zaś wiązał się z ludźmi na wielkich ptakach, które jakby strącały dawnych władców w przepaść.

Z ust spadających istot wychodziły węże albo mroczne duchy, które wiły się pod posadzkami zamku w którym panowali władcy ludzcy, jednak wężę były trzymane w uścisku przez człowieka-jelenia z jednym tylko porożem. Jednak jeden waż wyślizgał mu się z ręki i docierał do człowieka ze znakiem, wyglądającym jak liczba "25".

\-   🔹Och. - Westchnął Javier opuszczając pochodnię.

🔹

## Nie wiem
*Postacie: *Liam, D'oh

🔹

🔹Zaraz gdy Javier wraz z Gregorym zeszli do szczeliny, hrabia Sokół kazał zaufanym sługom pełnić wartę, sam udał się do sokolarni, aby chociaż na chwilę zająć myśli czymś innym, gdyż nad wyłomem po prostu nie mógł ze zdenerwowania usiedzieć. D'oh radziła sobie z drapieżnymi ptakami doskonale. Potrafiła wytropić odpowiednio silne i zwinne, młode osobniki, czasowo oślepić i doglądać, karmiąc surowym mięsem. Kolejny z sokołów był na tym z etapów, iż wystarczyło zdjąć mu kaptór i rzucić zanęte, aby ją sprawnie pochwycił, ale nie na tyle był przysposobiony, aby całkowicie usunąć postronek uwiązany do nóg.

🔹Hrabia odłożył laskę na bok i ubrał rękawicę, przygotował także wabik, aby sprawdzić jak radzi sobie sokolica. Kiedy zabrał ptaszysko usadowione na rękawicy, pogwizdując zdjął kaptur zwierzęciu, z satysfakcją zauważając że na ten dźwięk sokolica reaguje z ożywieniem i niecierpliwością. Zatem stara Do'h nie zmieniła metod, ciągle uczy ptaki, że gwizd to czas karmienia.

🔹Kiedy zwierzę poszybowało w górę za rzuconym wabikiem, wyglądało doskonale, piękne w swej śmiercionośnej zwinności i smukłości.

\-   Szkoda,  że życie władcy nie jest takie proste co? Tak do celu, bah, powrót. I jeszcze raz. Męczy Cię to, widzę. Chciałbyś prościej.
\-   🔹Zrobię, co będzie konieczne. - Odparł spoglądając przez ramię, po czym kolejnym gwizdnięciem przywołał sokolicę, ta nie ponaglana naprężonym postronkiem, o dziwo przyleciała karnie na rękę mężczyzny, a Maksimiliam założył jej kaptur.
\-   Nie pasowałam do orków. Nie chcieli mnie. Ludzie też mnie nie chcieli. Dopiero tutaj znalazłam dom. Ciężko mi już wstawać rano, więc posłuchaj mnie. Bądź tam gdzie ci dobrze, bo na co ci ból? Masz rękę do zwierząt, lepiej ci z nimi niż z ludźmi. Nie ma cię pięć lat, wracasz na chwilę. I co? Sam wyglądasz, jakbyś miał nie wstać rano. Blady chłopiec się o ciebie też martwi a on się rzadko martwi.

🔹Zamknął sokolicę niespiesznie i zdjął ochronną rękawicę, wzdychając przy tym.

\-   🔹Zatem faktycznie źle muszę wyglądać, skoro nawet służbę trapi me zdrowie.

🔹Uśmiechnął się kącikiem ust i sięgnął po laskę. Przejście paru kroków bez niej nie stanowiło problemu, ale coś mu podpowiadało, że nawet podczas krótkiego truchtu mógłby sobie boleśnie uszkodzić mięśnie.

\-   🔹D'oh, nie mogę zostawić Sloana, ma tak naprawdę tylko mnie. Jaśnie Ojciec postradał zmysły, a Pani Matka... nie sądzę, że trzyma stronę kogokolwiek, prócz swojej własnej. - Podszedł do rosłej ogrzycy i spojrzał na nią, w górę. - Czuję, że w obecnej sytuacji każdy mój wybór będzie zły.
\-   Pani Matka woli pierworodne szczenię, nic nie poradzisz. Ojciec wolał Ciebie, ale oszalał. Tylko stara D'oh zawsze posłucha twojego narzekania. - Wyciągnęła rękę i poklepała go po głowie czule. - Nie działał na instynkt. Tak robią zwierzęta, a tutaj to nie dzicz. Z wilkiem nie pogadasz. - Przerwała i zbliżyła się do niego. - Ty nic nie zrobisz? Nie znaczy, że ktoś inny nie zrobi. I to tak, jak ci się nie podoba. - Powiedziała cicho, tak aby mieć pewność, że tylko on to usłyszy.
\-   🔹Wiem, D'oh. Staram się być o dwa kroki przed przeciwnikiem. Tym razem przeciwników jest więcej i niełatwo odgadnąć komu można zaufać. - Mówiąc to, rozejrzał się.

🔹Byli sami, nieopodal znajdowała się psiarnia, w tamtą stronę skierowali swe kroki, lecz zamiast wstąpić do środka, poszli nieco dalej, za pomieszczenia gospodarcze, gdzie dźwięki lasu były dużo wyraźniejsze, jego bliskość była dużo przyjemniejsza dla D'oh i Liama niż korytarze posiadłości Sterling.

\-   🔹Gdy sprawy tutaj się uspokoją, będziesz chciała wyjechać ze mną? - Zapytał hrabia, stawiając futrzasty, siwy kołnierz i chroniąc się w ten sposób przed mroźnymi powiewami wiatru.
\-   Nie, chcę tutaj umrzeć. Nie zostało mi dużo czasu, nie chcę zużyć go na podróże. Tacy jak ja nie żyją długo. Nawet na poduszkach.

🔹Pokiwał głową, godząc się z jej decyzją, a raczej po prostu przyjmując ją do wiadomości. Wiedział, jak to mówią, że starych drzew się nie przesadza, bo nie przyjmą się w nowym podłożu i uschną jeszcze szybciej, niż było im to pisane przez bogów.

\-   🔹Rozumiem D'oh, w porządku, ale wiedz, że cenię sobie twoją przyjaźń i doceniam wszystko co dla mnie zrobiłaś... Wiesz, Javier zszedł wraz z moim medykiem do szczeliny, muszę już tam wrócić, aby wyglądać ich powrotu.
\-   Uważaj na tego chłopaka, medyka. On ma w sobie coś niedobrego w głębi sobie. Pamiętaj, że złe nawyki nie skarcone się wzmacniają. I na siebie uważaj. I na Javiera. To dobry człowiek, ale nie jest mu chyba pisane spokojne życie. - Popatrzyła na niego ze smutkiem. - Gdybym miała wybrać nowe życie, to chciałabym po śmierć białym wilkiem. Tak mało ich już tutaj zostało.

🔹Maksimiliam zawahał się przez chwilę, jakby zmagając się pomiędzy powinnością, a uczuciami, w końcu jednak uściskał D'oh, jeszcze raz jej za wszystko dziękując zdawkowo i wrócił nad wyrąb w podłodze. Nie przegonił służby, gdyż mogło się okazać, że ich pomoc będzie niezbędna. Nie mógł odgadnąć w jakim stanie wróci Javier i Gregory, oraz czy w ogóle wrócą. Tak naprawdę nie dopuszczał do siebie myśli, że mogliby zginąć. Postanowił poczekać parę godzin a do tego czasu zagłębić się w lekturze. Wybrał parę ksiąg, a służbie nakazał ustawić biurko oraz fotel tak, aby mógł zasiąść przodem do szczeliny i mieć ją ciągle w zasięgu wzroku.

## Nie wiem
*Postacie: *Fenilla, Liam

🔹Pogrążony w lekturze hrabia Sokół wzdrygnął się nagle i podniósł zamyślone spojrzenie, poczuł się obserwowany, a uczucie owo nie było przypadkowe, faktycznie para dużych oczu przypatrywała mu się z ciemnego korytarza. Przebudzeni słudzy, czuwający przy szczelinie razem z najmłodszym de Falco przebudzili się, przyłapani na nieuwadze. Ktoś ich podszedł, a takie rzeczy nie powinny mieć miejsca.

🔹- Stryju, dlaczego nie śpisz? -- Rozległ się delikatny, dziecięcy głos, jedna ze służek odetchnęła z ulgą.

🔹- Powinienem zapytać o to samo, panienko. Czytam. Zaczytałem się. Podejdź do mnie, powiedz, gdzie twoje służki, dlaczego wałęsasz się sama po nocy? -- Zapytał, przypatrując się dziewczynce, której nieco zaspaną buzię otaczały wzburzone włosy. Dziecko wzruszyło ramionami.

🔹- Posnęły, a mnie ciągle budzą wilcy tym wyciem, stryju i księżyc w oczy świeci. Poczytasz mi tę książkę?

🔹- Nie jest ciekawa... ale mogę ci opowiedzieć coś, Fenillo. Chciałabyś?

🔹Dziewczynka pokiwała głową, więc hrabia zdjął ciepły pled, który grzał mu kolana, owinął nim bratanicę i posadził ją na swoich kolanach, upewniając się, że jest jej wygodnie.

🔹- Dobrze, od czego zacząć? Na początku, zanim powstała ziemia, skały, niebo i wody, Orboh długo patrzył w pustkę i poczuł się znużony, postanowił więc zamieszkać w głębi swojego oka, otoczonego kamiennymi powiekami i wypełnionym po brzegi słoną łzą. -- Zaczął opowiadać Maksimiliam, a Fenilla ułożyła głowę na jego ramieniu słuchając i ziewając od czasu do czasu. -- Przepłynął w oku bezkres oceanów ze wschodu na zachód, z góry w dół i z powrotem. Jednak w końcu i to go znużyło. Oddzielił powietrze od wody, a z kamieni uczynił ziemię ścierając ją w swych dłoniach. To, co stworzył bardzo mu się podobało, więc nie poprzestawał w trudach, stworzył słońce i księżyc i wszystkie rośliny, drogocenne kamienie, głębokie jary, wysokie góry, pieczary i mgły, miękki mech i ostry, śnieżny wiatr, kiedy skończył tworzyć świat, przemienił się w wielkiego ptaka o tysiącu piórach i wzbił się w przestworza, aby móc podziwiać swe dzieło z góry. Efekt przerósł jego pierwotne założenia, świat był doskonały, ale ciągle czegoś brakowało. Orboh zrzucił więc swoje pióra, a te spadając z nieba trafiły w różne miejsca; do gęstych borów, przepastnych jaskiń, lodowatych jezior i bystrych rzek, na mokradła rozległe i lepkie, wpadły w mleczne mgły i ciepły słoneczny blask... A z każdego pióra powstała boska istota, mająca część mocny Orboha, który nie był już samotny. Zaczął nazywać każdą istotę, stąd mamy smukłe Wije władające wiatrem, białe Mroże władające śnieżycami i lodem, wielkie Rożbohów z okazałym porożem, władców lasów i skał, albo Rosty, których miechy pełne są nasion, kwiatów i ziół... i wielu innych. Nie nazwał wszystkich, bo zmęczył się i zasnął, zupełnie jak ty.

🔹Maksimiliam wstał ostrożnie i polecił słudze, zanieść dziewczynę do łóżka, ostrożnie nie budząc jej po drodze.



\- Wybaczcie, ale chyba powiedzenie wprost "Widzieliśmy freski na ścianie i faktycznie dwudziestego piątego władcę czeka zagłada i twój Ojciec nie jest szalony, tylko pragmatyczny" nie jest najrozsądniejsze, Panie. - Mówił przejęty Gregory w drodze na powierzchnię. Zabrał ze sobą kilka zwojów i teraz dyszał ciężko, bo twórca każdego z nich zainwestował nie tylko w grubość skóry, ale też w bogate okucia i mocny rdzeń wokół którego je nawinięto.

🔹Javier szedł szybko, aż złapał małą zadyszkę, oglądał się co jakiś czas, bo to co przed nim, już znał, a za nim tym razem coś mogło się wlec.

🔹- A co niby mam powiedzieć? "Wiesz Maks, wszystko w porządku, niech twój brat spokojnie obejmuje tron"? Sloana też nie mam zamiaru przekreślać, są moją rodziną! Nie mam bladego pojęcia co możemy zrobić. Nie możemy posadzić na tronie też przypadkowej osoby bo... bo co jeśli przepowiednia się nie spełni? To dopiero byłby ambaras, sytuacje jest ze wszech miar patowa.

🔹

\-   W takim razie powiemy wszystko jemu i on podejmie decyzje. Dostosujemy się i... - Nagle zatrzymał się. - Co jeżeli to my jesteśmy tymi wężami i jak mu o tym powiemy, to doprowadzimy do zguby Pana Maksymialiana?

🔹Szlachcic stanął w miejscu i popatrzył na Gregorego zdezorientowany.

\-   🔹Ale jeśli mu nie powiemy, to może nie być świadom czyhającego zagrożenia. Pan na Sterling wierzy w tę przepowiednię, szczerze mówiąc... ja też. Możemy powiedzieć o niej wyłącznie Maksimiliamowi. Masz inny pomysł?
\-   Tak. Możemy złapać to stworzenie które żyje w tych podziemiach i zmusić do wyjawienie prawdy. Patrzyliśmy na zapiski na murze i gnamy teraz o nich powiedzieć, ale nie znamy kontekstu. Mam ze sobą zapiski z dołu, możemy tam wrócić i dowiedzieć się więcej. To jest wielkie odkrycie, to jak spojrzenie pod podszewkę świata, tam gdzie dostęp mają tylko bogowie i ich słudzy. I teraz my.

🔹Roześmiał się, ale od razu zamilkł, widząc jego minę.

\-   🔹Ty mówisz poważnie.
\-   Nie ważne czy klątwa jest prawdziwa czy nie. Widzieliście światło wydobywające się z kul, mocniejsze niż jakakolwiek pochodnia czy latarnia, albo te żywe pnącza. To co tutaj widzieliśmy, może zmienić świat, Panie. - Gregory mówił szybko, ruszając gorączkowo rękami. Właściwie to wyglądał, jakby dopadła go właśnie gorączka i rzucało mu się to na umysł. Albo po prostu był podekscytowany a emocje, podziemia oraz blask pochodni igrało z umysłem.
\-   🔹Zakłócanie spokoju prastarym istotom nikomu nie wychodzi nigdy na dobre... wiesz, że moi dziadowie składali krwawe ofiary? I to dużo częściej, niż ma to miejsce obecnie... chcesz się w razie czego złożyć w ofierze? Bo to wersja "po dobroci" przyjść z prezentami, jeśli mamy zamiar robić coś siłą, to ja nie mam wielkich nadziei. Zginiemy niehybnie.

Gregory wyciągnął rękę do przodu i powiedział.

\-   Wystarczy być odpowiednio przygotowanym.

I wtedy jego ręką zapłonęła, a raczej tylko tak było na początku, bo ogień z niej przeniósł się do gorejącej kuli lewitującej nad jego dłonią. Gregory patrzył się na nią zafascynowany. Blask i ciepło biegnącej od niej był bardzo wyraźny, ale zdecydowanie nie było to ciepło domowego ogniska, ale raczej gorąc podpalonego domostwa podczas napadu wrogich sił.

🔹Javier dobył miecza, od razu, wyuczonym gestem, który działał obecnie równie skutecznie jak instynkt.

\-   🔹Do licha! - Zaklął szlachcic, czując że poci mu się wnętrze dłoni, którą zaciskał na rękojeści miecza. - Ostrzegaj chociaż. - Oblizał spierzchnięte wargi i rozejrzał się nerwowo.
\-   Przepraszam, chciałem tylko pokazać że nie jesteśmy bezbronni. - Myślał chwilę. - Może wrócimy do Pana Maksymiliana, powiemy mu o leżach, ale już nie o przepowiedni?  - Mówił to nie tylko dlatego, że to był dobry pomysł. Bardzo, ale to bardzo chciał już stąd uciec i to z powodu właśnie tej kuli ognia, którą właśnie zdołał ugasić. Bał się, bo spodziewał się tylko małego płomienia na swojej ręce, a nie małego słońca. Bał się też tym bardziej, że nie wiedział skąd u niego ten pomysł schwytania i zaatakowania mieszkańca podziemi. Nigdy nie takich pomysłów, aby krzywdzić innych a teraz, kiedy będąc w obliczu potęgi na wyciągnięcie ręki, coś z głębi jego umysłu podpowiadało... nie, pchało go w kierunku, którego się nie spodziewał. Zwłaszcza, że nie chciał się ujawniać ze swoimi umiejętnościami bez potrzeby przed obcą osobą.
\-   🔹O ilości informacji przekazanych Maksimiliamowi zdecyduję na miejscu. Zobaczymy jak zareaguje na inne rewelacje z naszej podróży.

## Raport
*Postacie: *Javier, Liam, Gregory

🔹Siedzieli bezpiecznej, ciepłej sali, przy solidnym stole, pojąc się winem i siląc się wieczerzą, jednak Javier czuł, że żołądek ma zaciśnięty do wielkości pięści, więc kawałek chleba, skubał długo i bez przekonania, przepełniony niepokojem wyniesionym z trzewi podziemi. Gregory uzupełniał jego opowieść kolejnymi szczegółami, opowiedzieli o pułapkach i nawet o tym, jak udało im się je pokonać, o wielkiej sali, w której najprawdopodobniej odprawiano niegdyś pradawne rytuały, a także o malowidłach przedstawiające Rożbohów.

🔹Maksimiliam słuchał ich uważnie, blady i niewyspany, zmartwiony ale także... rozczarowany.

\-   🔹To wszystko? Cała wyprawa poskutkowała jeno odkryciem paru korytarzy, dlatego zaryzykowaliście zdrowiem? - Potrząsnął głową z niedowierzaniem. - Skąd szaleństwo Pana ojca i obłęd jego sługi?
\-   Spotkaliśmy po drodze skupisko grzybów purchawek, które za lada dotknięciem wybuchały, pewnie jakimś trującym parszystwem. Mam próbkę ze sobą, mogę sprawdzić, czy stworzę z tego antidotum.

🔹Javier przytaknął, zatrzymując spojrzenie swoich bystrych oczu na Gregorym, zastanawiał się w głębi duszy na ile jest on pomocą a na ile realnym zagrożeniem. Zaskoczył go w podziemiach magicznymi zdolnościami, a takim osobom lepiej nie ufać, gdyby nie miał nic do ukrycia, zapewne od początku byłoby jasnym, że potrafi posługiwać się nieludzką mocą, pokonać przeszkody. Wzdrygnął się gwałtownie, aż Gregory i Maksimiliam przerwali rozmowę spoglądając na niego.

\-   🔹Przepraszam... marzą mi się termy, zmęczenie mię wzieło i dreszcze. Wybaczcie. - Przeczesał rude loki i wstał od stołu.
\-   🔹Javierze... Wszystko w porządku?
\-   🔹Tak, tak. Muszę odpocząć...
\-   🔹Zajrzę do ciebie niedługo.

🔹- Będę zobowiązany. - Zapewnił Javier, zostawiając hrabiego z Gregorym. Maksimiliam natomiast nalał Gregoremu i sobie okowity, medykowi z uprzejmości i bez pytania, a sobie bo potrzebował. Jednym haustem opróżnił z płynu dno kubeczka, czując od razu jak ostry, ognisty smak rozpala przełyk i gęstą cieczą spływa w dół. Palcami rozmasował swoje skronie czując w nich pulsujący ból, teraz gdy miał zamknięte oczy, Gregory mógł przypatrzeć się jego zmęczonej twarzy, cieniom pod oczami i zmarszczkom odciskającym powoli swoje piętno na czole.

🔹- Jesteśmy tu zaledwie od tygodnia, a ja mam wrażenie jakoby mijał już kolejny miesiąc... Chciałbym już wrócić do domu, uporać się z tutejszymi sprawami i wrócić do swych włości. - Jęknął, właściwie zwierzając się Gregoremu ze swoich myśli. - Musi tam być teraz cicho, o ile Grot uporał się z zapasami na przednówku i dogląda zapasy we wsi.

Gregory spojrzał na arrasy wiszące na ścianach, przedstawiające sceny z życia okolicznej ludności. Zastanowiło go to, bo może nie widział wielu dworów, ale władcy nie słyneli z tego, że aż tak interesują się losami swoich ludów, aby ich dziejami ozdabiać ściany. Może mu się tylko wydawało, w końcu czuł, że ma gorączkę. Będzie musiał użyć jakiegoś lekarstwa z zapasów i zwinie się w kłębek w łóżku.

\-   Ja też chciałbym już wracać Panie. Tutaj jest dziwnie. Dziwniej niż dziwnie. Trzeba jak najszybciej załatwić tutejsze sprawy i uciekać... znaczy, wyjechać stąd. - Dotknął swoje spocone czoło - Wybaczycie, ale chyba mnie zawiało tam na dole, straszne przeciągi tam były. Muszę wziąć coś na gorączkę ze swoich rzeczy i położyć się spać.

🔹Maksimiliam wstał i położył dłoń na ramieniu Gregorego.

\-   🔹Jeszcze raz dziękuję, że zgodziłeś się towarzyszyć Javierowi. Udaj się na spoczynek, wyślę do ciebie moje sługi, aby pomogli ci zadbać o zdrowie i komfort. Wyśpij się, Gregory.

🔹Szlachcic pożegnał się z nim i zgodnie z obietnicą zszedł do komnat kąpielowych, służący pomógł swemu panu zdjąć odzienie i zaprowadził aż na próg term. Javier relaksował się w samotności, ale widok przyjaciela wyraźnie poprawił mu humor. Opuścił go stres i przepastny niepokój, ale zamiast tego pojawiło się zwyczajne zmęczenie.

\-   🔹Nic ci nie jest? Obydwoje wyglądacie okropnie, Javierze. - Liam podszedł do niego, brodząc w wodzie, i dłońmi objął twarz mężczyzny, przyglądając się mu z troską.

Javier ujął dłonie swoimi dłońmi, odsunął od swojej twarzy i wpatrywał się w toń pod nimi milcząc.

\-   Liamie, nie zrobisz niczego głupiego, obiecujesz? Nic takiego, co nie przedyskutujesz ze mną, dobrze?
\-   Dobrze... - odparł ostrożnie Sokół. - Co masz na myśli? Coś się stało?
\-   Pod ogrodami znajduje się wielka komnata, a raczej jaskinia. Oświetlona magicznymi kulami, i ta sama magia pozwala, chyba, na podglądanie tego co widać z oczek wodnych i podsłuchiwanie tego co dzieje się w ogrodzie. Aby się tam dostać musieliśmy minąć nie tylko grzyby, ale też wężorośliny lękające się tylko ognia. W tej komnacie znajduje się wielkie malowidło przedstawiające upadek Rożbohów z ręki założyciela twojego rodu i klątwy, którą rzucili na odchodne. Ta klątwa miała by dosięgnąć dwudziestego piątego władcę Sterling. Był tam też opiekun zamku, Rożboh z tylko z połową poroża, ale on miał nie dać rady powstrzymać przeznaczenia. - Wziął oddech, spojrzał prosto w oczy Liamowi i pokazał tym samym jak płyną mu łzy. - Błagam, pamiętaj, że ja cię kocham, nie rób nic pochopnie.
\-   🔹Och, Najdroższy, nie martw się o mnie. - Poprosił opierając swoje czoło o jego czoło i opuszkami palców ocierając jego łzy. - Nie zrobię nic nierozsądnego, obiecuję... Prawdę powiedziawszy takich wieści się spodziewałem, wiem że mój ojciec wrócił z podziemi właśnie z takim przekonaniem o nieuchronności klątwy. Podejrzewałem dlaczego mnie chciał wyznaczyć na swojego następcę... nie mógł poświęcić Sloana.

🔹Maksimiliam zajął miejsce obok Javiera i wsłuchiwał się w szmer i bulgot wody, skapującą ze stropu skroploną parę. Miał parę pomysłów na rozwiązanie zaistniałej sytuacji, ale każdy obarczony był naprawdę wysokim ryzykiem i żaden nie dawał gwarancji powodzenia. Hrabia westchnął głęboko i zanurzył całą twarz w wodzie, chciało mu się krzyczeć ze złości i z bezradności, ale nie miał zamiaru uskarżać się Javierowi, i tak już martwił się o niego wystarczająco mocno.

\-   🔹Kiedy ma przybyć ten cesarski wysłannik? Muszę się przygotować na to spotkanie. Nie wiem też na ile mogę ufać Sloanowi.
\-   Trudno powiedzieć może dwa, trzy dni. Miałem dostać wiadomość gołębiem jak tylko dotrze on do włości rodu xxx^\a\(#cmnt1){#cmnt_ref1}^. Nie martw się Sloanem, pomyśl raczej o swoim medyku Gregorym. A raczej o swoim magu Gregorym. Dlaczego mi nie powiedziałeś, że ma zdolności magiczne i to chyba niemałe?

🔹Przetarł twarz dłońmi, strząsając z niej nadmiar wody i popatrzył na towarzysza zdziwiony.

\-   🔹O czym ty mówisz, Javierze? Gregory nie ma prawie żadnych magicznych zdolności, dopiero się uczy. Lepszym jest medykiem, gdyż magiem prawie żadnym.

Javier równie zaskoczony patrzył na Liamia.

\-   Tam w podziemiach nakłaniał mnie, aby nic ci nie mówić o przepowiedni, a jedynie zapolować i uwięzić stworzenie, które tam mieszka i wydobyć z niego informacje. Kiedy ja wykpiłem pomysł atakowania stworzenia, któremu jeszcze niedawno składaliśmy ofiary, ten zapalił w oka mgnieniu kulę ognia w swojej ręce. To było, muszę przyznać, straszne, prawie go zaatakowałem w obronnym odruchu. Ta kula... ona była strasznie gorącą i jasna, jakbyś z zimowego wieczoru wszedł w środek płonącego domu. Widziałem działania kilku magów, czy to certyfikowanych ze służb specjalnych i tych nielegalnych. To była magia najwyższej klasy, po kilkunastu latach szkoleń i sporym talencie naturalnym.
\-   🔹Jeszcze tego mi brakowało, maga w samym tyglu zmian. - Jęknął Maksimiliam. - Nie wiem skąd u niego takie zdolności, nie posądzałbym go o zatajenie swoich mocy, ale to, o czym opowiadasz znacznie przerasta to, co o Gregorym wiem.
\-   Właściwie to skąd on jest, co faktycznie o nim wiesz? To jest moc z którą mógłbyś rzucić wyzwanie naprawdę wielu osobom... - Zamilkł, bo uświadomił sobie, że powiedzenie, że wyzwanie mógłby rzucić nawet Cesarzowi zmroziła mu krew w żyłach. Takich rzeczy nie mówi się nawet w głębokich, nieużywanych sztolniach. - Zaufajmy Sloanowi. On naprawdę był wściekły na manipulacje ojca, który w jego mniemaniu zdradził i ciebie i jego. To nie jest typ człowieka, który będzie rządził po trupach. To co mnie niepokoi, to fakt, że nie wiemy co twoja matka, Kylie robi w tej całej sprawie, bo to nie jest kobieta która tańczy jak jej zagrać. Co gorsza, nie wiem co w tym wszystkim robi opiekun zamku.

🔹Kwaśny uśmiech pojawił się na ustach Maksmilama, miał wątpliwości co do szlachetności swojego brata, znał go dłużej i lepiej niż Javier, ale też nigdy nie przyjaźnili się jakoś szczególnie mocno, pomimo pokrewieństwa krwi. Jednak nie mógł zostawać z tak trudnymi decyzjami zupełnie sam.

## Ćwiczenia na tarasie
**Postacie: ** Sloan, Liam, Blady chłopiec

🔹Kolejny dzień na zamku Sterling mijał spokojnie, śnieg skrzył się w jasnym i ostrym słońcu, a niebo nie zapowiadało kolejnych. Z kamiennego tarasu na samym szczycie zamczyska widać było miateczko na podgrodziu, trakt, jezioro i górzyste, leśne obszary. Stąd też bliżej było do słońca i nieba, w tym miejscu, według legendy, prastary ptak uwił gniazdo i stąd postanowił władać ziemią. Tego dnia Sloan ćwiczył swoje mięśnie, szybkość cięć mieczem i swój refleks. Chłodny wiatr owiewał rozgrzane mięśnie, a słońce świeciło czasem w oczy.

🔹Maksimiliam usiadł na kamiennym murku, bokiem do brata i bokiem do przepaści i zapierającego dech widoku. Słońce grzało mu twarz i było to niewątpliwie miłe uczucie. W milczeniu obserwował technikę i siłę swojego brata, zazdroszcząc mu w duchu sprawności i siły. Atuty, którymi przeważał nad Sloanem zostały potępione przez wypadek i ranę, którą otrzymał podczas pościgu za zbrodniczym komando. Już nie był tak zwinny, chociaż co prawda nadal pozostawał wyśmienitym łowcą, łucznikiem oraz jeźdźcem.

\-   Tylko mi tutaj nie próbuj skakać, nie zdążę po takim treningu dobiec do ciebie i cię złapać! - Krzyknął zasapany Sloan. Wsunął miecz do pochwy przypasanej u swego pasa i zbliżył się do brata.

Stanął nad nim i popatrzył się na tereny otaczające zamek.

\-   Piękne prawda? Nawet pomimo, że widziałem mniej niż ty i pomimo, że widzieliśmy razem stolicę za młodu. Ta kraina jest taka surowa, taka piękna. I trzeba być tak samo surowym jak ona, bo inaczej cię pokona. Tylko, że ja czuję się i tak pokonany, ale nie przez nią, tylko przez naszego ojca.

🔹Z przekory Maksimiliam chciał zapewnić brata, że widział stokroć piękniejsze miejsca, ale po prawdzie uważał, zupełnie tak jak Sloan, że Sterling jest jedynym tak wyjątkowym miejscem na świecie, pełnym surowego uroku, zapierających dech jarów, skostniałych na kamień gór, lodowatych potoków i pachnącym igliwiem lasów.

\-   🔹Gdybym został twoim panem, potrafiłbyś się z tym pogodzić, Sloanie? - Zapytał Sokół z twarzą ciągle zwróconą w stronę słońca, zapytał spokojnie, bez emocji, jakby ciągle rozmawiali o pięknie Sterling albo o pogodzie.

Sloanowi stężała twarz.

\-   Jeżeli żartujesz, to wiedz, że mnie ten żart nie bawi. Jeżeli nie, to chyba zapominasz gdzie siedzisz.

🔹Maksimiliam odwrócił twarz w stronę górującego nad nim brata i spojrzał mu w oczy. Sloan wyglądał jak personifikacja siły i determinacji, Javier mylił się co do starszego z braci de Falco. Sloan był w stanie płacić wysoką cenę w zamian za osiągnięcie celu. Może nie każdą cenę, ale władza jest jednak czymś, co warte jest wyrzeczeń.

\-   🔹Nie rzucam ci wyzwania, Sloanie, ale też nie żartuję. Zastanawiam się tylko...
\-   Bracie, proszę, nie pogrywaj sobie teraz. Rozumiem, że gimastykowanie językiem jest dobre w innych częściach kraju ważne, ale tutaj i w tym momencie, lepiej jest, abyś mówił wprost a żartował sobie co najwyżej ze sługi, co się w ptasie gówno przewrócił, co zresztą stało się przed dwoma chwilami, bo z jakiegoś powodu całe stado ptaków najwyraźniej nasrało w jednym miejscu. Inna rzecz, że nie zrzuciłbym cię w przepaść nigdy i nie wyzwał bym też kalekę na pojedynek. - Sloan mówił i mówił, jak nigdy dotąd tak długo. Chyba czas, który się nie widzieli poświęcił na przemyślenie wielu spraw.  - Nawet gdybyś stwierdził, że ojca trzeba zgładzić, to bym ci pomógł w tym. Gdybyś stwierdził, że faktycznie najlepszym pomysłem jest, abyś został następcą, to zgodziłbym się. Ale do licha, zaczynanie rozmowy ze mną w taki sposób to z twojej strony jest naprawdę okrutne!

🔹- To prawda. - Odparł i zaśmiał się, uświadamiając sobie jak niepoważne było to, co zasugerował Sloanowi. Właściwie myślał o nim ciągle jak o nieco starszym bracie, nie zaś jak o wojowniku i następcy dynastii, którym w istocie był. - To prawda, masz rację. Wybacz mi moje słowa, bracie, to było dziecinne. Posłuchaj... sprawy są nieco bardziej skomplikowane, niż mogło to się nam wydawać jeszcze parę dni wcześniej.

🔹Maksimiliam wstał opierając się o laskę i rozejrzał się, upewniając się, że są absolutnie sami i żadna wścibska para oczu ani uszu nie będzie świadkiem ich rozmowy.

🔹- Ojca przytłoczył ciężar odpowiedzialności, postradał zmysły gdyż sądził, że aby któryś z nas objął władzę, drugiego będzie musiał... poświęcić. Uwierzył w przepowiednię przedstawioną na freskach w ciągnących się pod Sterling korytarzach, zgodnie z nią, dwudziesty piąty władca tych ziem przypłaci życiem za to, że nasi przodkowie przeciwstawili się Rożbohom i przegnali ich ze swych leży, zajmując ich kamienne komnaty oraz sale. Nie wiem, czy przepowiednia jest prawdziwa, ale nie powinniśmy się tego dowiadywać na własnej skórze. Jeśli nie jest to legenda, to jednak ktoś zadał sobie trudu, aby wszystko sfingować, skądinąd nadal zagrożenie istnieje i jest niemałe.

\- Moment, moment. Co to za brednie, że akurat dwudziesty piąty władca ma zostać ukarany?! Co... - Chciał coś powiedzieć, podniósł dziwacznie kilka razy ręce do góry, w geście chyba szukania odpowiedzi albo oburzenia. W końcu zrezygnował i usiadł ciężko obok Maksymiliana i tylko mamrotał do siebie w kółko. - To jakieś brednie, bzdura.

🔹- Być może bzdura, Sloanie, ale tak jak mówiłem, ktoś zadał sobie trud, aby nasz ojciec w to uwierzył. Nieistotne na chwilę obecną, czy jest to faktyczna przepowiednia, czy planowany zamach. Coś jest na rzeczy, a żaden z nas w pojedynkę sobie nie poradzi. - Razem damy radę i z tym, Sloanie. - Maksimiliam poklepał brata po ramieniu.

\-   W takich chwilach Liamie myślę sobie, że byłbyś lepszym władcą niż ja. - Powiedział cicho.
\-   🔹Z pewnością byłbym twoim dobrym doradcą... jeśli jakiś szczegół przyjdzie ci do głowy, jakikolwiek drobiazg związany ze sprawą, powiedz mi o nim proszę. Być może dzięki takim drobiazgom uda mi się się w łatwiejszy sposób uporać z naszą sprawą. Mam parę pomysłów, ale nie chciałbym ich wykorzystywać, gdyż daleko im do ideału. - Machnął ręką. - Dajmy na razie temu spokój, jako że jesteśmy sami, mam do ciebie jeszcze jedną prośbę, Sloanie... Dotyczy ona trzech osób, ciebie, Irvette... i Cilli. Ujmujesz wszystkim godności chodząc na te schadzki. Nie rób tego, proszę. Jestem tu zaledwie tydzień, a wiem o twoim romansie, wyobraź sobie jak bardzo jesteś w tym niedyskretny.

Sloan milczał chwilę, zastanawiając się jak wpadł na nich nocną, tajną schadzkę, która szczęśliwie wypadała w momencie, kiedy szedł wzburzony zamordować ojca gołymi rękami i przypomniało mu się, że chyba dzisiaj mieli się spotkać. Jeszcze nigdy ich zbliżenie nie było tak udane jak tej nocy, nie tylko dlatego, że całą swoją wściekłość przelał w pełne pasji akt, ale też Cilla była tak miękka, tak przylegająca do niego jak najlepiej wykonany ubiór na świecie, tak domyślając się jego pragnień i wielokrotnie sama znajdując uniesienie. Jeżeli to miała być ich pożegnalna noc z powodu nakrycia, to chyba nawet lepiej. Nie sądził, że to uniesienie mogło by się kiedykolwiek jeszcze powtórzyć.

\-   🔹Masz rację, skończę z tym. - Odpowiedział, wspominając tamte chwile.
\-   Słusznie. Romansami nie przedłużysz rodu, przynajmniej nie tak, jak powinieneś. Zamiast tego powinieneś zająć się żoną, to dobra i mądra kobieta, której nie doceniasz.

Kiwnął głową, ale w głębi zastanawiał się dlaczego tak łatwo zgodził się odrzucić nieziemską przyjemność. Owszem, rozsądek stał po stronie Maksimiliania, ale ten romans nie był z rozsądku. Penis boleśnie uwierał go w spodniach od ledwo wspomnienia tej nocy, więc dlaczego kiedy godził się z bratem w tej kwestii, jednocześnie myślał, że tak trzeba? Czy to Liam był taki przekonywujący? Spojrzał się zamyślony w stronę schodów i zauważył na nich ruch, ktoś się nagle cofnął znikając za załomem, o parę sekund za szybko, aby można było rozpoznać osobę.

\-   Liamie, ktoś nas obserwował!
\-   🔹Cóż, miejmy nadzieję, że nie słyszał... - Odparł oglądając w stronę miejsca na które spoglądał brat. - Bądź bardzo ostrożny w tym, co robisz i mówisz, szczególnie w ogrodzie, prawdopodobnie komuś łatwiej jest podsłuchać każdą tam odbywającą się rozmowę. Za parę dni przybędzie cesarski posłaniec, nie my jesteśmy przyczyną jego przybycia, ale możemy spodziewać się jego wizyty.
\-   Wiesz co, strasznie chyba zmęczył mnie ten trening i wieści od ciebie, bo strasznie zachciało mi się spać. Swoją drogą - ziewnął. - nie widziałem ani ojca ani matki od jakiegoś czasu.
\-   Lepiej będzie, jeśli sprawdzisz co z nimi, jak się czują i w jakich są nastrojach. - Odparł mu z namysłem młodszy brat. Gdy doszli do podstawy schodów, Sloan osunął się na nie, nie mogąc z nich wstać.

<!-- -->

\-   Psia mać, jak mi się chcę spać - ziewnął potężnie - nie... mogę... tego... powstrzymać...

I zasnął.

\-   🔹Sloan! Na bogów, co się z tobą dzieje? - Liam przestraszył się szczerze, próbując nieco ocucić brata, rozglądał się też przy tym wokoło, aby chociaż służbę wezwać do pomocy.
\-   🔹  Sloan!

W takiej sytuacji zastała ich służąca Hrabiego, Ann, która zaraz do nich podbiegła. Sloan leżał nieprzytomny, ale oddychał równo, jakby po prostu zdecydował, że tutaj będzie mu najwygodniej się przespać.

\-   Co się stało, wezwać po kogoś Panie? - Spytała Maksymiliana.
\-   🔹Pobiegnij po kogoś, Ann. Kogoś, kto pomoże mi zabrać Sloana do jego komnat. I powiedz Gregoremu, że chcę się z nim zobaczyć. Oraz Irvette, znajdź także panią Irvettę.
\-   Niestety, ale jeżeli chodzi o Gregorego, to szukałam Pana, bo on leży ze straszną gorączką i majaczy o zakazanej wiedzy.

## Cilla
**Postacie: ** Irvette, Sloan, Liam

🔹Sloan leżał na łożu, było solidne i zasłane skórami. Wyprosili służbę, więc wewnątrz została tylko Irvetta i Maksimiliam, przynajmniej na razie. Ona, chodziła niespokojna, a Liam stał ze skrzyżowanymi ramionami, założonymi przed sobą. Jego czoło zaznaczyły wyraźne zmarszczki, gdyż próbował coś sensownego wymyślić, ale tak naprawdę czuł, że wpada w otchłań coraz większego chaosu.

Irvette patrzyła trochę zbyt obojętnym wzrokiem na śpiącego męża. W jej spojrzeniu nie było widać troski, tak dla niej typowej. Właściwie, to wyglądała nie najlepiej, ale raczej z powodu niewyspania się i może płaczu, jak można było zgadywać po jej spuchniętych oczach.

\-   Byliście tam pod spodem. - Stwierdziła. - Wiem, bo służba mi powiedziała, Służba, a nie Ty, Mąż czy Javier. W czasie kiedy Wy tam byliście, ja przejmowałam się tym, że obecni władcy nie pojawiają się poza swoimi komnatami i tylko odbierają jedzenie od służby. Ta sama służa przygotowuje zamek do wizyty wysłannika cesarza, bo jak plotka niesie, to ciebie oficjalnie mianuje podczas jego wizyty następcą. I Fen zupełnie się mnie nie słucha, ciągle bawi się z jakimś dzieciakiem, który ją podburza przeciwko mnie, ale nie mogę się dowiedzieć skąd on się tutaj wziął i nigdzie go nie potrafię znaleźć ani ja ani nikt ze służby, przynajmniej tej skłonnej mi pomóc.

Skończyła mówić i wypiła haustem wino z pucharu.

\-   🔹Irvette... Javier był w szczelinie. On i mój medyk, Gregory. Dowiedzieli się tam, właściwie niczego więcej, czego nie wiedziałabyś ty. Potwierdzili jedynie twoje przypuszczenia. Sloan nic o tym nie wiedział. - Odparł, bardzo starając się, aby ton jego głosu nie brzmiał na znużony, chociaż w istocie czuł się wszystkim, całym tym Sterling i jego sprawami wymęczony. Spotkałem Fenillę dwie noce temu, wałęsającą się  samotnie korytarzami...

🔹Mówiąc, wodził za Irvettą wzrokiem, widział jej opuchnięte oczy, ale uprzejmie udawał, że tego nie dostrzega.

\-   🔹Coraz częściej myślę, że zostanie dziedzicem Sterling jest jedynym wyjściem. Wszystko zaczyna mi się wymykać spod kontroli, ty jesteś na skraju wyczerpania, Sloanem zawładną jakiś czar, mój medyk leży w malignie, ojciec postradał zmysły raczej bezpowrotnie, Javier ryzykuje zdrowiem w ogóle tu przebywając... tylko okoliczne rody mogą na całym zajściu zyskać. No i nie wiem jaką rolę w tym wszystkim odgrywa Pani Matka.
\-   Cillo - wyjęczał cicho przez sen Sloan. - Cillo.

Irvette ciągle trzymając puchar gwałtownie podeszła do męża i zdzieliła go nim mocno w głowę. Efekt był dość zaskakujący, bo Sloan natychmiast zerwał się z pozycji leżącej, w pozycji klęczącej na łóżku wyglądał jakby szykował się do odparcia kolejnych ataków.

\-   Mężu, co chcesz mi powiedzieć na temat Cilli? - Mówiła cicho, słowami podszytymi wściekłością niczym zwierzę, które całe naprężone skrada się do ofiary.

🔹Maksimilam wstrząśniętym wydarzeniami, które potoczyły się tak nagle i niespodziewanie przyłożył powoli dłoń do ust po czym wygładził zgrabnym ruchem wąsy oraz brodę. Sloan wyglądał natomiast na jeszcze bardziej zdezorientowanego, nie pamiętając co się wcześniej stało, nie wiedząc co robi w alkowie, na dodatek w towarzystwie brata oraz własnej żony.

\-   🔹Co? O czym ty mówisz?! - Zbaraniały wzrok przerzucił z jej postaci na postać Liama, może próbując rozeznać się w sytuacji,  a może nawet szukając wsparcia.
\-   Zemdlałeś i jęczałeś imię tej młodej dziewki, która kilkakrotnie była w odwiedziny u nas razem z Javierem. - Mówiła siląc się na spokój Irvette. - A ona wodziła za tobą wzrokiem napalonej suki, ale ty mnie uspakajałeś, że to, że ktoś tak na Ciebie patrzy, to nie twoja wina.

🔹Sloan usiadł na brzegu łoża i roztarł dłońmi zaspane oczy, ciągle czując jakby miał pod powiekami piasek.

\-   🔹Przestań. - Warknął krótko, ale dobitnie. - Roisz sobie jakieś bzdury. Jeśli o mnie chodzi, noga Cilli może tu więcej nie postanąć... co my właściwie tu robimy... we troje?
\-   Wasi rodzice zamknęli się w komnatach, trwa przygotowanie na sukcesję podczas wizyty wysłannika cesarskiego. Ty mdlejesz, przynosi cię tutaj twój brat. Jęczysz imię tej małej ladacznicy, więc cię obudziłam. Taka ze mnie dobra, przykłada żona. Ach i jak będziesz następcą, to umrzesz, co mi właściwie bardzo by teraz odpowiadało.

🔹Maksimilam nie odezwał się, ale przytaknął kwając głową, może nie na tyle wyraźnie, aby ktoś zwrócił na to uwagę, ale nie mógł się z Irvettą nie zgodzić.

\-   🔹Istotnie, Sloanie, powinieneś zajrzeć do rodziców, sprawdzić co z nimi. Tobie chętniej otworzą niż... komukolwiek.

Sloan wygramolił się z łóżka, trzymając się za prawą stronę twarzy, gdzie widać było już olbrzymi siniak.

\-   To ja już do nich pójdę. - Mówiąc to, opuścił komnatę.

Irvette patrząc się na ścianie, spytała.

\-   Wiedziałeś? Powiedz jeszcze, że jak go znalazłeś, to był z nią?
\-   🔹Irvette... - zaczął spolegliwym tonem. - To nie czas na taką rozmowę. Naprawdę. Proszę cię.
\-   Męski świat co? Zatem, o czym chcesz rozmawiać? O tym jak ci ciężko i jak ci trzeba pomóc?

🔹Pokręcił głową.

\-   🔹Nie to miałem na myśli. Sloan jest głupcem, nie zamierzam temu zaprzeczać, lecz jego... lub moje, może nasze życie jest obecnie niezbyt pewne. Nie jesteś w tym wszystkim mniej ważna, w ostatecznym rozrachunku możesz zrobić co ci się żywnie podoba, Irvetto.
\-   Nie jest głupcem, Maksymilanie, tylko niewiernym mężem który kpi sobie ze mnie. I potrzebujemy go, aby poradzić sobie z tym całym bałaganem. Tylko, że widzę, że mnie traktujesz jak damę do ratowania a jego jako partnera do działania. I to naprawdę boli.
\-   🔹Irvette. Posłuchaj. To on ma zostać władcą, nie ty. To on powinien działać, ty możesz go wspierać, i prawdę mówiąc, powinnaś. Tak samo ja. Nasza chęć przebywania tutaj, czy chęć uczestniczenia w całej sprawie, nie ma znaczenia. Władca jest tylko wtedy silny, gdy ma na kim polegać. Pomówmy teraz z Javierem, dobrze?
\-   Musisz mieć ostatnie słowo co? Ale tak, pomówmy z Javierem. Wyślę kogoś po niego i po jedzenie, bo chyba trochę będziemy musieli tutaj posiedzieć.



## Drzwi zamknięte!
**Postacie: ** Sloan, Liam, Fenilla (Lawenda)


W zamku trwały przygotowania na przyjazd wysłannika cesarskiego. Obsługa to zwykle było tylko kilkanaście osób nie licząc małego garnizonu, ale Majordomus dobierał według uznania więcej osób z miasteczka w takich sytuacjach. Nagle puste zwykle korytarze pełne były osób, które spieszyły się, aby sprzątać, prać i gotować potrawy. Wszystko jakby szykował się raczej przyjazd cesarza, albo organizowano ślub czy może pogrzeb.



I tak Sloan dotarł do części w której mieszkali jego rodzice. Przed korytarzem do ich komnaty stało na warcie dwóch strażników, którzy zerwali się z krzeseł na jego widok. Wyglądali, jakby ich pilnowanie polegało na tym, że nudzili się i zagrywali się w kości. Pytanie jednak stanowiło, co tutaj robili, bo pierwszy raz widział strażników w tym miejscu.

🔹Sloan obrzucił ich spojrzeniem i raźnym krokiem podszedł do zdobionych, dwuskrzydłowych drzwi.

\-   🔹Państwo oczekują jakiejś zapowiedzi? - Rzucił pytaniem w stronę strażników, a sam ciężką pięścią zapukał do drzwi, perspektywa wtargnięcia do komnat rodziców zawsze powodowała dyskomfort.

Strażnicy byli bardzo zaskoczeni sytuacją, jakby kazano im pilnować drzwi, ale nie wiedzieli co mają zrobić z synem władcy. Jednak przełamali swoje zdumienie i jeden z nich, ze ściętymi czarnymi włosami położył rękę na jego torsie.

\-   Władca i jego małżonka nie życzą sobie nikogo, nawet Was! - Powiedział nagle głośno, jakby przełamywał strach. - Przepraszam.
\-   🔹Szczerze wątpię, że nie życzą sobie zobaczyć swojego pierworodnego, a Waszego niebawem władcę.
\-   Z tego co wiemy, następcą będzie Maksymilian. - Chwycił go za ramię, kiedy bezskutecznie naciskał klamkę zamkniętych drzwi. - Gdyby życzyli sobie wizyty, posłali by bo Was Panie...
\-   Jeżeli poradzisz sobie ze strażnikami, to pomogę ci otworzyć drzwi. - Powiedziało blade dziecko, które stało w drugim końcu korytarza. To samo, które usługiwało im podczas pojednawczego spotkania z bratem w termach, zaraz przed tym jak Sloan chciał zabić swojego ojca gołymi rękami i kiedy nagle spotkał swoją kochankę i spędził z nią burzliwe chwile. - Masz pewne masę pytań, ale odpowiedzi znajdziesz za drzwiami.
\-   🔹Kim ty w ogóle jesteś? - Potrząsnął głową, stawiając to pierwsze, nadrzędne obecnie pytanie. - Ty. - Wskazał dłonią jednego ze strażników. - Czyś ty postradał rozum? Jestem dziedzicem, ja i Liam, jakkolwiek. Ja cię szkoliłem, ja cię przyjąłem do straży, znam cię od dzieciaka. Rozkazuję ci się opamiętać. Ktoś próbuje zamordować członków mojej rodziny, a drugą część podtruwa. Pojmij tego chłopaka i pilnujcie tych przeklętych drzwi! Muszę natychmiast porozmawiać z matką i ojcem, bez względu na to, czy zdecydowali się na popołudniową drzemkę czy nie! - Sloan emanował gniewem, ale trzymał go na wodzy na tyle, aby nie wpaść w szał, chociaż był już tego bliski.
\-   Panie, nie mamy kluczy, drzwi są zamknięte. - Mówił wstrząśnięty podkomendy. - I o jakiego chłopaka chodzi? To tylko Fen, tylko jakaś taka blada. - Kontynuował - Są zamknięci od środka, tylko otwierają drzwi jak przynosimy im jedzenie o wyznaczonej porze i wtedy musimy odejść od drzwi i czekać, aż Jaśnie Małżonka weźmie tacę i zamknie się znowu w środku\...

🔹Położył dłoń na rękojeści miecza, odwracając się w stronę bladego dziecka i idąc w jego stronę.

\-   🔹Masz teraz bardzo krótką chwilę na powiedzenie czegoś, co mnie przekona.
\-   Mnie tutaj nawet nie ma, ale ta dziewczynka pozwoliła mi używać swojego ciała abym mógł z tobą porozmawiać. Jeżeli jednak chcesz ją skrzywdzisz, użyję siły. A wtedy nie wejdziemy łatwo do środka. Nie wiem jaki jest stan twojego ojca, ale jest szansa uratować twoją matkę.^\b\(#cmnt2){#cmnt_ref2}^

Sloan opuścił powoli dłoń, odsuwając ją od swojej broni. Włosy na karku zjeżyły mu się, ale obrót wydarzeń sprawił, że nawet nie zwrócił na to uwagi. 

\-   Kim jesteś. Mów.
\-   Pokonanym przez twojego przodka, tym który postanowił chronić was przed naszymi reliktami i tym, który ostatecznie zawiódł. Nazywacie nas ludźmi jeleniami albo Rożbohami. Nie mam imienia w waszym języku, możesz nazywać mnie bladym chłopcem.
\-   🔹Dlaczego nam pomagasz, czego chcesz w zamian? - Pytał, czując jak krew huczy mu w skroniach i mając w głębokim poważaniu, co mogą pomyśleć strażnicy.
\-   Niczego oprócz pewności, że nie zginie więcej osób. Nasze przewidywania co do trwałości konstrukcji nie zawiodły, co gorsza nigdy bym nie pomyślał, że chwilę po krytycznych uszkodzeniach do biblioteki zejdzie sam władca. Nie mogłem nic zrobić, nie byłem wtedy przygotowany. Chcę naprawić to, co mogę naprawić. Jednak nie otworzę drzwi, dopóki nie będę czuł się bezpieczny. To mój warunek. Odpraw straż.



## Na zawsze razem
**Postacie: ** Javier, Liam, Irvette


\-   Byłem u Gregorego. Leży w osobnym pokoju teraz, mają na niego oko. - Powiedział Javier. - Natomiast zniknęły zwoje, które wziął ze sobą. Zajmowała się nim Ann, mówiła, że nigdy ich nawet nie widziała. I jeszcze coś. Jutro przybędzie wysłannik.

Spojrzał się na Irvette i Liama, po czym napił się wina. 🔹Maksimiliam był nieobecny myślami, przekładał laskę z jednej dłoni do drugiej, zapewne nawet sobie z tego nie zdając sprawy, od dwóch dni także nie jadł, żołądek miał ściśnięty do wielkości pięści.

\-   Liam, Liam! - Javier powtarzał imię hrabiego.
\-   🔹Przepraszam. - Otrząsnął się i popatrzył na nich już bardziej przytomnie. - Nie powinienem był zostawiać Sloana samego teraz. - Powiedział i skierował się do drzwi.

I wtedy przez te same drzwi wszedł blady Sloan, mówiąc:

\-   Ojciec nie żyje.

Stał tak w drzwiach, nie wchodząc głębiej.

\-   Maksimiliam, według słów naszej matki, ojciec w testamencie zapisał ciebie jako dziedzica Stirling. A po tym jak umrzesz lub oszalejesz, dziedzicem zostanę ja... - Milczał chwilę. - Matka oszalała. To przez księgę z którą ojciec wyszedł. To coś w tych księgach, zwojach. Im nie szkodzi, ale nas zabija. Będzie jeszcze gorzej, mówił, że biblioteka jest rozszczelniona, że to przesiąknie do zamku i dalej do miasta. I że oni... Oni uznają nic za pierwszą liczbę, a nie jeden jako pierwszą. Cały czas chodziło o dwudzieste czwarte pokolenie.

🔹W komnacie zapadła cisza, wszyscy patrzyli po sobie. Wydarzenia nie zwalniały, przeciwnie, wszystko nabierało obrotów. Maksimiliam czuł, że krew odpływa mu z twarzy, był oszołomiony obrotem spraw, jakby przespał ostatnie parę dni, a wszystko wydarzyło się bez jego udziału. A najgorsze było to, że perspektywa zostania władcą Sterling przerażała go zdecydowanie najmniej ze wszystkich wieści przyniesionych przez brata.

\-   🔹Sloanie... - zaczął, czując, że głos więźnie mu w gardle, na szczęście była to tylko chwilowa niemoc. - Bracie. Skąd się o tym dowiedziałeś, kto ci powiedział? Poza tym, nie wiesz jeszcze, że jutro przybędzie cesarski wysłannik, chociaż przybywa nie z naszego powodu, to jednak nam także złoży wizytę.
\-   🔹Tym nie zaprzątajcie sobie na razie głowy - wtrącił się Javier. - Delegat, który się zjawi, nie jest mi obcy. Postaram się, aby wizyta nie była dla was uciążliwą.

🔹Liam skinął głową w podzięce.

Sloan skrócił im część z wchodzeniem sprzed komnaty.

\-   Fenilla, a raczej Rożboh w której niej był spojrzał się po prostu na drzwi a one jakby\... jakby zgnieść liść w dłoni albo papier. I jeszcze bardziej i bardziej, aż po prostu znikły. Wszystko odbyło się w kompletnej ciszy. I wtedy uderzył nas straszny smród, tak że nie miałem czasu bać się tego, co zobaczyłem.

Podszedł do stołu chwycił butelkę z winem i napił się z niej dużym haustem. W swoich ruchach był jakby skostniały i niezręczny, jakby ciało go zawodziło, ale umysł zmuszał je do ruchu.

\-   Ojciec. - Zaczął, patrząc się przez okno w niebo. - Ojciec był w stanie zaawansowanego rozkładu, jakby jego ciało wyciągnięto z ziemi. Trzymał na kolanach wielką księgę, jakby czytał ją do samego końca. Matka rzuciła się na mnie...

Głos mu się załamywał.

\-   Nasza Matka jest w złym stanie. Jej twarz... - głos załamał mu się kompletnie a na twarzy pojawiły się łzy. Chyba nikt z zebranych nie widział go, aby kiedykolwiek płakał - nie poznałem jej, nie miała skóry na twarzy.

Ukrył twarz w dłoniach i płakał cicho, wzdrygając się co chwilę, wsparty o stół.🔹 Javier splótł ramiona, jakby sam się próbował objąć, po jego szeroko otwartych oczach można było wysnuć wniosek, że jest zszokowany. Irvetta przyłożyła dłoń do ust, ale widząc łzy Sloana podbiegła do niego i przytuliła go, albo raczej się do jego wielkiego ramienia.

\-   🔹Och, Sloan, to straszne! To straszne - szeptała.

🔹Słowa Sloana były tak poruszające i wstrząsające swą zaskakującą treścią, że nikt nawet nie zauważył, kiedy w komnacie zabrakło Maksimiliam, drzwi były uchylone, a jego laska spoczywała na parapecie okna.

🔹

🔹Rozległy się nagle jakieś krzyki i hałasy, niosące się echem wśród kamiennych korytarzy. Javier rzucił się do wyjścia, Irvetta także podbiegła do drzwi, ale widząc, że jej małżonek pozostał w miejscu, zawahała się i przymknęła drzwi, wracając do Sloana, który dużo bardziej potrzebował teraz jej towarzystwa, nawet jeśli sobie z tego nie zdawał sprawy.

🔹Służba kotłowała się w korytarzu, toteż Javierowi trudno było się przedostać, nawet jeśli krzyczał, aby się usunięto z drogi, i odpychał tarasujące przejście osoby.  Zza okutych drzwi słychać było szamotaninę i łomot, aż w końcu wszystko ucichło. Na nic zdawało się łomotanie do drzwi i próby ich wyważenia, dopiero po paru kwadransach drzwi się uchyliły, otworzone od wewnątrz, framugi trzymał się zakrwawiony Liam, wyglądał jakby coś całkiem wyssało z niego energię, poza tym miał naderwane ramię tuniki oraz zadrapany policzek, po jego bladych dłoniach spływała gęstniejąca powoli krew.

\-   🔹Wyrwała mi... sztylet. To się stało tak szybko.
\-   Liamie, co się stało? Czy tam była wasza matka? Co z nią, co się stało szybko?

🔹Poruszył ustami starając się coś odpowiedzieć, ale głos ugrzązł mu w gardle, w końcu pokiwał głową.

Minął go i wszedł do środka. W środku, na łóżku, lub raczej na prostyn sienniku leżał potwór. Ten potwór trzymał sztylet, który tkwił w piersi. Tym potworem, niepodobnym do człowieka, była Kylie z rodów d'Mounier, żona Durmmunda de Falco, która skonała w szaleństwie na jakimś łóżku należącym do kogoś ze służby. Wyszedł z komnaty, zażądał zamknięcia pokoju i przekazania mu klucza. Służba w szoku spełniała jego żądania. Wrócił do Liamia i zaciągnął go z powrotem do pokoju. W połowie drogi dołączyła do nich Irvette, której pytania szybko uciął do momentu gdy znaleźli się  z powrotem w pokoju z Sloanem.

🔹- Rano, o wschodzie słońca zapłonie stos pogrzebowy... Muszę się tym zająć... - podzielił się znów myślami, których nie mógł wysłowić po drodze, gdyż Javier go nie słuchał.

Javier za drzwiami osunął się na podłogę i ukrył twarz w dłoniach. To go kompletnie przerastało. Jego ukochany zamiast stać się wolny, stanie się władcą tego obrzydliwego, strasznego i przeklętego przez bogów zamku. Na oczach jego ukochanego zginęła jego własna matka, tuż po tym, jak dowiedział się, że jego ojciec nie żyje. Teraz przyjedzie jeszcze jego przygodny kochanek, który już w normalnej sytuacji byłby kłopotliwy dla nich, ale teraz, gdy jest gdzieś jakiś testament napisany przez szaleńca, który nawet jak ukryją może spowodować olbrzymie implikacje jak sądy nad oboma braćmi.

\-   To jest jakieś piekło.

🔹Maksimiliam podszedł do młodego barona i położył dłonie na jego ramionach, pochylając się nad nim, i chociaż Liam wyglądał jak cień samego siebie, trudno było zaprzeczyć, że nie poddawał się rozpaczy.

\-   🔹Javierze, przepraszam cię z całego serca, że musisz w tym uczestniczyć. Wróć do swoich włości. Jutro o poranku ja i Sloan oddamy ciała rodziców bogom, odbędzie się ceremonia pogrzebowa, lecz... jeśli się nie zjawisz, nikt nie będzie czuł urazy. - Mówił cicho, najbardziej rzeczowo jak potrafił. - Przepraszam. Muszę... zrobić jeszcze wiele rzeczy.

Irvette patrzyła zdezorientowana, kiedy jej ukochany Liam zgarnął z parapetu laską i wyszedł z komnaty. 

## Płomień
**Postacie: ** Ann, Liam

Zaraz po wyjściu natknął się na Ann, swoją służącą, które stanęła mu na drodze.

\-   Czy potrzebujecie pomocy, mój panie?
\-   🔹Tak. - Zatrzymał się. Patrząc na nią bez emocji, natłok myśli skutecznie je odsuwał w zakamarki umysłu. - Najpierw...
\-   🔹Przygotować i pomóc ci założyć czyste odzienie, panie? - Podsunęła szybko służąca, widząc w jakim stanie jest hrabia, i raczej zgadując, że o swojej powierzchowności nie pomyślałby w pierwszej kolejności, chociaż wyglądał strasznie i nikt nie powinien go takim oglądać.
\-   🔹Tak. - Przytaknął kolejny raz Maksimiliam i skierował się do swoich komnat.

Razem weszli do komnaty hrabiego i kiedy ten po zamknięciu drzwi odwrócił się do niej, aby mogła go rozebrać, zobaczył nagą Cillę. Bardzo wydoroślała od ostatniego ich spotkania, nie licząc momentu, gdy zobaczył jak jego brat spędza z nią upojne chwile.

\-   Cóż, mówiłam Blademu Dziecku, że nie warto się dla was tak starać, kiedy potem w wyniku swoich waśni czy interesów potraficie zabić swoich własnych rodziców. Ale mnie interesuje, powiedzmy z nudów, dlaczego to zrobiłeś.

🔹Czuł, jak w jednej chwili zaschło mu w gardle, ale wyprostował się, zaciskając dłoń na lasce, którą się wspierał podczas chodzenia.

🔹- Z litości. - Odparł i zacisnął wargi zwilżając je czubkiem języka. Utkwił spojrzenie swoich zielonych oczu w postaci. - Aby ukrócić jej cierpienia, bowiem straciła wszystko. Jesteś Rożbohem? Czego od nas chcą pradawne istoty?

\- Jestem Płomieniem. Przyszedłem tutaj po mojego ukochanego, który dawno powinien do nas dołączyć za miejscem, które wy nazywacie pograniczem. Czekałem na niego długo, nawet jak na nas, czekając aż skończy się wami opiekować. Sądził, że z powodu naszych czynów w przeszłości wobec was, musimy was chronić. Teraz odpoczywa, a jako że siedzicie na gnieździe os, usilnie planuje was z tego wyciągnąć. - Mówiąc to istota poruszała się w uwodzący sposób, nie jakby chciała go uwieść, ale jakby takie zachowanie było jej integralną częścią. Jego lub jej włosy były jak miedziane druty, prawie jak ogień bo zresztą naprawdę bił z nich blask, który rozświetlał komnatę.

\-   🔹To dom moich przodków, i mój, od pokoleń. Też pragnę uporać się z gniazdem os.

\- A wcześniej dom nasz, więc uważaj co mówisz, bo ja nie mam takich oporów co on. Zawsze możesz po prostu spalić ten zamek i w ten sposób powstrzymać zagrożenie z biblioteki. No chyba, że chcesz podpalić tylko ją i jakoś kontrolować ogień, ale coś mi się wydaje, że jeszcze nie macie takiej technologii.

\- 🔹Nie, nie potrafiłbym... ale ty potrafisz. - Odparł ostrożnie. - Wzniecić i kontrolować żar, uwolnić mój lud od tego zagrożenia, którego istoty nie pojmuję. - Maksimiliam ukląkł przed Płomieniem, odsuwając od siebie laskę. - Błagam...

\- Nie. Nie chcę wam pomagać. Nie obchodzą mnie takie nudne rzeczy jak powinność, honor, opieka nad innymi. Mnie obchodzi to, co kim kieruje i co kogo napędza. Dlatego kocham Bladego Chłopca, bo on pomimo lat, nigdy nie zszedł z drogi, kierując się tym, w co wierzy. Dlatego czekałem na niego jak na nikogo nigdy. Teraz dopełniły się przewidywania i on może odejść, pozostając sobą.

🔹- Mówisz, że chce pomóc mojemu rodowi, że zbiera siły i głowi się nad tym. Pomóż mi, odejdziecie stąd rychło... - mówił, ciągle klęcząc, chciał odruchowo przełknąć ślinę, ale gardło miał wyschnięte niemal na wiór. - Też chcę już stąd odejść, oddać władzę w ręce brata, i też czekam na kogoś... stanowczo zbyt długo. Tylko, obawiam się, że ja... będę czekał na ukochaną osobę do końca świata.

\- Spal zamek, brat zbuduje nowy na jego zgliszczach. Wy, ludzie zawsze tak robicie. Ty zaś zakuwasz się w niewidzialne kajdany o których rozbicie prosisz istoty, które wiedzą, że nie ważne jak długo będziesz żył, to liczy się tylko dla nich miłość. Czekaj przykuty jeżeli wierzysz, że te kajdany są ważne. Jeżeli nie, spal świat dla tego, kogo kochasz. - Mówiąc to głos Płomienia stawał się coraz głośniejszy i nieludzki a aura paląca już nawet nie skórę, ale oczy, nozdrza i gardło. Jej postać wydawała się być większa. - Kochać oznacza płonąc! Spalać się samemu, ale czasem tylko tlić się w oczekiwaniu na lepszy moment. Pokaż! Udowodnij mi! Wtedy ci pomogę!

🔹Czuł bijący od tej istoty żar, i czuł jak pieką go policzki, właściwie to było dziwne uczucie, bo nic go nie rozpalało, tylko poczuł własne łzy, wypływające bezwiednie z jego oczu.

\-   🔹Spalam się za każdym razem, gdy muszę wybierać między miłością a obowiązkami, umiera kawałek mnie. Ale zawsze wybiorę to, co muszę, zamiast tego, co chcę. Bo oprócz niego, kocham też mojego brata i moich poddanych, moją rodzinę, bo wszystko co mam, zawdzięczam im, nawet jeśli muszę płacić najwyższą cenę. - Nieporadnie zgarnął laskę z podłogi i wspierając się nią, wstał, chociaż czuł jakby jego nogi zrobione były z ołowiu, albo wcale nie były jego własnymi. Nie patrzył już na Płomień, świetlista i gorejąca postać przepełniona była blaskiem, który ranił jego oczy.
\-   Zatem nie szukaj pomocy w płomieniu który wszystko spala, ale w wodzie, która się do wszystkiego dopasowuje. Nie proś mnie o pomoc, tylko Bladego dziecka. Ja cenię tylko ogień we wnętrzu, on zrozumie ciebie. - Skończywszy mówić, buchnął wielkim płomieniem, który zalał całe pomieszczenie. Kiedy hrabia zaczął znowu widzieć, stała przed nim zakłopotana Ann, tak jak ją widział na korytarzu.
\-   Przepraszam, mam nadzieje że nic nie zrobił wam złego, Panie. - Powiedziała niepewnie.

🔹Maksimiliam zaczerpnął haust powietrza, jakby bardzo długo przebywał pod wodą. Jakby w ostatniej chwili udało mu się odzyskać oddech, chciał postąpić naprzód, ale zachwiał się osłabiony i niepewny swoich ruchów, Ann widząc to, podbiegła do hrabiego i pomogła mu usiąść na brzegu łoża. Liam czuł, że drży, że właściwie trzęsie się i nie może nad tym zapanować. Ann coś mówiła do niego, ale nie rozumiał słów, trząsł się i płakał, skulony na łożu, z głową na kolanach służki, nie wiedział nawet kiedy zasnął z wyczerpania.

## Przygotowania
**Postacie: ** Liam, Javier, 

🔹Słońce przebijało się przez gęste chmury, a rano z ciemnego nieba siąpił śnieg z deszczem. Wiosna przychodziła w te strony bardzo ostrożnie i niechętnie, tylko gdzieniegdzie spomiędzy plam śniegu wyglądały pierwsze, białe kwiaty, zdeterminowane aby zakwitnąć, łakomo wysuwając się w stronę nieczęstych, słonecznych promieni padających na ziemię. Wiosny jednak można było się spodziewać, obwieszczali ją pierzaści heroldzi, leśne ptactwo pogwizdujące w konarach starych drzew oraz gałęziach wysmukłych sosen.

🔹Hrabia przespał wieczór, całą noc, oraz sporą część poranka, wykończony ostatnimi wydarzeniami leżał rozebrany przez Ann, wsłuchując się w rozbijające się o okna krople deszczu. Przez chwilę, będąc zawieszonym gdzieś pomiędzy snem a jawą, wydawało mu się, że jest na Pograniczu. To wrażenie uleciało jednak szybko wraz z rozlegającym się od strony drzwi pukaniem.

\-   Panie, baron Javier d'Monier chcę się z tobą wiedzieć. Czy możemy go wpuścić? - Głos brzmiał twardo, jakby gościa zapowiadał jakiś wojskowy.



Po usłyszeniu zgody, Javier wszedł do środka i przez moment było widać dwóch strażników na zewnątrz, którzy pełnili wartę przed komnatą. Kiedy drzwi się zamknęły, Javier podbiegł cicho do łóżka i utulił hrabiego.

\-   Pewnie musisz się okropnie czuć, ale Irvette zarządziła, żebyś zaczął przygotowywać się do uroczystości pożegnalnej.

🔹Rozbudzony Maksimiliam westchnął głęboko, przytulił na chwilę policzek do ramienia Javiera, a skroń po drugiej stronie swej twarzy rozmasował opuszkami palcy, czując ćmiący umysł, pulsujący ból.

\-   🔹Słusznie. Niewiele udało mi się przygotować wczoraj, Irvette wszystkim się zajeła? - Dopytał się, wstając z niechęcią i wielkim trudem, bardziej psychicznym niż fizycznym. Na szczęście Ann przygotowała dwa zestawy odzienia, do wyboru. Jedno białe i ceremonialne, drugie w rodowych barwach de Falco.
\-   Tak. Twój brat jest w strzępach, w przeciwieństwie do ciebie, nie trafił do łóżka o własnych siłach. Nie wiem czy to bardziej ze strony śmierci rodziców, czy w kwestii że gdzieś jest testament który mówi, że jesteś spadkobiercą. Chciałem się zająć sytuacją, ale Irvette okazała się być bardzo zaradna i właściwie byłem niepotrzebny, nie licząc tego, że zadbałem, aby nikt nie dotykał ciała waszej matki. Wynieśliśmy łóżko na włóczniach... - Zauważył, jak mówienie o matce sprawiało ból Liamowi, więc nagle zmienił temat. - Gregory ma się lepiej, ale sługa który wyruszył razem z waszym ojcem umarł we śnie. Wygląda, że młody organizm przy krótkim zetknięciu z tym urokiem, może go zwalczyć. A może to, że jest magiem pomogło.

Pogłaskał hrabiego po włosach. Javier był niemal szary na twarzy. Mówił cicho, referował wydarzenia niczym stan ksiąg rozrachunkowych, czy cokolwiek innego, co nie pochłania człowieka emocjonalnie.

\-   Odziej się, musicie mieć to już za sobą. - Popatrzył na ubrania niechętnie. -  Pomogą ci się ubrać. Barwy rodowe będą bardziej wskazane.
\-   🔹Będę zobowiązany. - Odparł Maksimiliam. W głowie ciągle mu huczało, nie czuł się najlepiej, a ból głowy wcale mu w zebraniu się nie pomagał. Przynajmniej Gregory miał się już nieco lepiej, to była chyba jedyna pozytywna informacja jaką usłyszał ostatnimi czasy.

## Pogrzeb
**Postacie: ** Liam, Javier, Sloan, Irvette, Hastriel

🔹Na zewnątrz było pochmurno, ale przynajmniej przestało padać. Stosy pogrzebowe stały ułożone solidnie, purpurowe proporce łomotały na wietrze, a ludność Sterling zebrała się na obszernym placu, na którym przygotowano ceremonię. Tego dnia nikt nie pracował, prócz służby i straży, wszelkie warsztaty i gospody były zamknięte, a poddani posępni, bliżsi swoim cieniom niż ludziom. Od wrót zamku Sterling aż do placu stał szpaler ludzi, wszyscy towarzyszyli rodzinie de Falco w pogrzebowej uroczystości. Sloan i Irvette byli już gotowi, odziani w odświętne szaty. Prócz nich w holu czekała bliższa i dalsza rodzina, siostry Javiera, oraz siostry i bracia jego matki Kyle, wśród kamiennych korytarzy niósł się szep rozmów, czasem cichy a czasem głośniejszy płacz. Wiele osób było wyraźnie zdruzgotanych wieścią o odejścu zarówno Pana jak i Pani na Sterling.

🔹Maksimiliam przywitał się z niektórymi, stryjowi Orchowi uścisnął ręką i przyjął kondolencje, połowy słów nie rozumiał, pulsujący w skroniach ból przebijał się ponad wszystko inne. W końcu podszedł do brata i Irvette, skinął głową na przywitanie, Irvettcie zdawkowo podziękował i zajął symboliczne miejsce po prawej stronie brata. W takich chwilach powinna towarzyszyć mu małżonka, dla wielu członków tej skądinąd tradycyjnej społeczności, jej brak wyglądał dziwinie i nieneturalnie, ale Liam nie zwrócił na to uwagi. Prianka nigdy nie uczestniczyła w żadnych ważnych dla niego uroczystościach, więc wszystkie ceremonialne funkcje pełnił sam.

🔹 Gdy wyszli, zimny wiatr, który przywitał Maksymiliama na chwilę ukoił ból głowy, ale małą to było pociechą. Idąc w stronę pogrzebowego stosu towarzyszyła im muzyka, która w tym wypadku bardziej przypominała zawodzenie wiatru, niż żałobne tony. Hrabia powiódł wzrokiem po zebranych na placu żałobnikach, prócz lokalnej ludności i dalszej rodziny, która przybyła na ostatnie pożegnanie, zauważył także grupę odzianych w czerń dygnitarzy, niektórzy w pełnym uzbrojeniu. Czerwone proporce nie pozostawiały wątpliwości co do cesarskiego namiestnictwa, ale w obliczu zaistniałych okoliczności i rozpoczynającej się przykrej, chociaż ważnej uroczystości, nawet wysłannicy cesarza musieli zaczekać. Stali po przeciwnej stronie placu, pomiędzy lokalną ludnością a przybyłą z okolic rodziną. Maksimiliam złowił spojrzenie szlachcica, którego strój pozwalał sądzić, że jest najistotniejszy. Oczy miał czarne, tak jak i włosy, które chociaż zaczesane do tyłu, obecnie szarpał wiatr, kpiąc sobie z czyjegokolwiek dostojeństwa. Hrabia Sokół skinął w jego stronę głową, na co szlachcic odpowiedział tym samym gestem. Wiatr potrząsał także sokolimi piórami, które wszyte zostały w płaszcz otulający spoczywającego na szczycie stosu Dormunda, głowę rodu de Falco. Obok niego, owinięta całunem, spoczywała Kyle. Rozpalono w tym momencie czary. W kutych elementach odzienia Sloana odbijał się poświęcony ogień, płonący w wysokich czarach napełnionych wonnym olejem, żywiczny aromat przy każdym podmuchu docierał do ich nozdrzy.

Całą ceremonię prowadziła Irvette. Zwyczajowo pożegnaniem władcy zajmowała się jego żona, zaś gdy nie było to możliwe, honory czyniła najstarsza córka a dopiero potem brano pod uwagę synów. Tak więc jej przewodnictwo było pewnym odstępswem od normy, ale nie jakimś szokującym i niezwykłym. To ona dawała znać, kiedy i gdzie miały odbywać się poszczególne czynności, wypełniane przez sługi. I tak krok po kroku dotarli do momentu przemówienia, które żegnało ich tutaj w cielesnych powłokach. I które miał wygłosić Sloan.

\-   Wszyscy znaliśmy mojego ojca jako sprawiedliwego władcę oraz moją matkę, która wspierała zdrowie najuboższych. - Zaczął, nawiązując tymi słowami do ufundowanej przez nią lecznicy dla najbiedniejszych. - Jednak nie oddałbym sprawiedliwości temu, co się wydarzyło tutaj, w ostatnich czasach, gdybym na tym zakończył. Moi rodzicie poświęcili się dla mnie, aby uchronić mnie przed klątwą z zamierzchłych czasów rzuconą przez pokonanych Rożbohów, które zamiast dosięgnąć mnie, dopadła ich. Szaleństwo które ich dotknęło, sprawiło, że na władcę mój ojciec chciał mianować mojego brata. Podobno spisał nawet testament, o którym tuż przed śmiercią powiedziała mi matka. Jednak nie znaleźliśmy żadnych śladów na to, aby on istniał. Jednak powiadam, jeżeli Maksimiliam zgłasza swoje roszczenia do władzy, powołując się na słowa rodziców, które obaj słyszeliśmy, to ustąpię wobec niego. Jeżeli nie zrobi tego w ciągu tygodnia, oznaczać to będzie, że nie rości sobie żadnych praw do tego tytułu i  głową rodu de Falco zostanę ja. Do tego jednak czasu obaj decydujemy wspólnie i zgodnie.

Jego głos był donośny, jakby był urodzonym władcą, zdolnym przemawiać do setek ludzi zgromadzonych na uroczystości. Nikt nie wiedział, że większość rzeczy była wprawiona w ruch dzięki Maksimiliamowi, który stał teraz z boku.

🔹Chociaż wszystkie oczy skierowane były w stronę młodszego z braci de Falco, Maksimiliam potrafił zachować twarz, chociaż uczucie upokorzenia boleśnie ukłuło go podczas niektórych fragmentów przemowy Sloana, szczególnie dotkliwy był prosty, acz jasny przekaz, że następcą może być ukochany dziedzic, lub wybrany przez szaleńca pomazaniec. Liam powoli, bardzo powoli nabrał powietrze w usta i wypuścił równie powoli, pulsujący w skroniach ból nie ustąpił, ale pozwoliło mu to zachować względny spokój. Gdy Sloan skończył przemowę Sokół położył dłoń na jego ramieniu, po czym wziął dwie pochodnie od stojącej przy ogniu Irvette, jedną z nich podał bratu, drugą zatrzymując dla siebie.

🔹 Obydwoje podeszli do stojącego na środku placu pokaźnego stosu pogrzebowego, z dwóch stron podpalili go. Wył nie tylko wiatr, ale tylko on rozniecał żar i kazał językom ognia tańczyć.

🔹 Po zakończeniu tej części, przy stosie zostali wyłącznie odziani na biało, wyznaczeni do ceremonii ludzie, aby starannie zebrać popiół, gdy ten już ostygnie. Ludzie mogli zająć się modlitwami i spędzeniem czasu z bliskimi, gdyż tego dnia i przez trzy najbliższe nikt nie miał obowiązku pracować w kramach i warsztatach. Czas na opłakiwanie władcy był prawem każdego poddanego, gdyż tragedia nie dotykała tylko rodziny, ale całej społeczności.

🔹 Rodzina rodu de Falco udała się do zamku Sterling, aby usiąść w kamienne sali przy wielkich, zakrzywionych w podkowę stołach. Wspólny posiłek poświęcony był czci pana i pani zamku, za znajdującymi się na środku siedziskami, na podwyższeniu, ustawiono dwa drewniane trony, przykryte woalem, gdzie zgodnie z wyobrażeniem zasiąść mieli zmarli i uczestniczyć ostatni raz w ucztowaniu z najbliższymi. Miejsce przy stole miało ogromne znaczenie, dwa najważniejsze miejsca, przed tronami zajęli synowie, po stronie Sloana Irvette, po stronie Maksimiliama, teoretycznie także powinna zasiąść małżonka, lecz z uwagi na niezwykłą i czyniącą zaszczyt delegacje wprost ze stolicy, miejsce u szczytu stołu ofiarowano posłowi, wysłannikowi cesarza.

\-   🔹Witajcie, Panowie Falco - zaczął poseł witając się ze Sloanem i Liamem - Jestem Hastriel var Hess, emisariusz miłościwie panującego Cesarza Berengara. Przyjmijcie moje kondolencje z powodu tragedii, która dotyka wasz ród. Jednocześnie, oczywiście doceniam, iż miałem zaszczyt uczestniczyć w waszych lokalnych ceremoniałach.
\-   Witaj, Panie var Hess. Dziękuje za te słowa, ale gdyby nie ten zbieg okoliczności, to prosiłbym o przesunięcie waszej wizyty o kilka dni. Rozmowa, która nie jest formalnością, nie jest czymś czego oczekujemy w tym momencie. - Mówił szorstko Sloan. - Oczywiście to zaszczyt być w kręgu zainteresowań Cesarza.

🔹Maksimiliam uścisnął dłoń emisariusza i przywitał się na tyle szybko, aby var Hess nie miał obowiązku zareagować na słowa Sloana, po prawdzia miał nadzieję, że podtekst zawarty w wypowiedzi brata umknął uwadze posła.

🔹- Natomiast ja chętnie dowiem się jak mają się sprawy w stolicy. Naszemu ojcu zależało, abyśmy interesowali się sprawami cesarstwa, toteż zabierał nas ze sobą od małego. Jaśnie państwo, a moi i brata rodzice, Drummond i Kyle de Falco z pewnością byliby zaszczyceni pańską wizytą, zatem to dogodny moment, aby pomówić w towarzystwie ich duchowej obecność. Proszę. - Wskazał miejsce, które mógł zająć Hastriel var Hess.

🔹- Raz zdaje się miałem przyjemność spotkać pańskiego ojca. - Odparł var Hess zwracając się do Maksimiliama. Emisariusz odpiął srebrną broszę z osadzonym weń rubinem i oddał pelerynę z czarnego futra słudze, gdyż sale Sterling były na tyle ciepłe iż można było poradzić sobie bez wierzchniego odzienia. - Niestety było to zalewie jedno... formalne... spotkanie. Chociaż Sterling jest oddalone od stolicy nieprzychylnie długim dystansem, to jednak nie umykają nam sprawy możnych z tych stron. O panu także słyszałem wiele. - Dodał, zasiadając przy stole nieopodal Maksimiliama.

🔹- Och, zapewniam, że to nie jest prawda. Nawet w połowie nie jestem tak zły, jak mówią. Jestem dużo gorszy.

🔹Hastriel var Hess roześmiał się, pozytywnie zaskoczony żartem. Chwila rozmowy z oboma braćmi ukazała ich jako zupełnie różne osoby, których związek krwi mogły zdradzać tylko niektóre i to drobne elementy urody, bo poza nimi byli zupełnie różni z charakteru.

🔹 Na stołach zaczęły pojawiać się ciepłe posiłki, pieczone mięsa z okolicznych lasów, gulasz z dzika, kiełbasy z sarny, faszerowane kuropatwy, macerowane w ziołach dzikie kaczki i lokalne wyroby. Nie zabrakło także pękatych dzbanów pełnych kwaskowatego piwa, doskonale ważonego w dolinach surowych gór.

\- Zatem muszę rozwiać pańskie nadzieje,  panie hrabio, słyszałem bardzo wiele dobrego. - Odparł poseł, posyłając jednocześnie spojrzenie Javierowi, który właśnie zbliżył się do stołu. - O, baron Mounier. Witaj, właśnie o tobie mówiłem.

Javier, który właśnie podszedł do stołu, uśmiechnął się lekko jakby był zadowolony z tego kogo spotkał, ale nie mógł w obecnej sytuacji wyrazić większej radości.

\-   Panie var Hess, pomimo okoliczności...
\-   Javierze, pomińmy formalności, za dobrze się znamy, aby się tytułować. Cieszę się, że cię widzę. - Wstał, aby się przywitać, lewą ręką ścisnął mu ramię zaś prawą rękę. - Szkoda bardzo, że spotykamy się w takich okolicznościach, lecz służba cesarstwu czasami zmusza do pojawiania się w nieodpowiednich momentach. - Tutaj skinął głową w stronę Sloana. - Obiecuję, że moja wizyta nie będzie długa.

\- A, no tak, naturalnie. - Odparł Javier, uśmiechając się uprzejmie, ale samymi ustami. Wyglądał na nieco spiętego, ale w obecnych okolicznościach zapewne każdy czuł się nieswojo.  - Jeśli ty i twoi ludzie potrzebujecie odpocząć przed powrotem i uzupełnić zapasy, to zapraszam do naszej posiadłości, służę po...

\- Wiem. Rozważę to. - Przerwał mu var Hess. - Usiądź, proszę. Właśnie rozmawiałem z panem hrabią o stolicy. Wierzę, że po zakończeniu żałoby wybierze się pan do serca cesarstwa. Wiele się zmieniło od pańskich, zapewne nie tak dawnych, młodych lat, lecz zmiany w samym tyglu kultury następują zaskakująco szybko. 

\- O ile pozwolą mi moje obowiązki, to nie zawaham się odwiedzić stolicę - powściągliwe odparł hrabia Sokół. - Usłyszałbym jednak więcej o tych przemianach. Javier opowiadał mi o stolicy, ale zawsze byłem zdania, że lepiej o sytuacji dowiedzieć się z dwóch a nie tylko jednego źródła. - Zakończył zdanie lekkim uśmiechem.

\- Cóż, Cesarstwo się rozrasta, wzbogaca o nowe tereny i szlaki handlowe. Jest piękniejsze i potężniejsze niż kiedykolwiek wcześniej\... 

\- I kosztuje to niemało. - Wtrącił Sloan. - Podobno zajęliście większość złoża rodu xxx. Oczywiście, należało im się w sytuacji, kiedy zataili odkrycie nowego złoża.

⚫- Widzę, iż wieści rozchodzą się, jak zwykle, szybko. - Odparł niezobowiązująco var Hess, sięgając po kubek piwa, z którego wyrobu ponoć słynęła okolica. - Natomiast pozwoli pan, że nieco sprostuję ogólną zależność. Cesarstwo otacza czułą protekcją rody, które pragną powiększać majątki, udostępniając przestrzeń i szlaki handlowe, niejednokrość zabezpieczając interesy zabiegami w różnej, acz skutecznej, postaci. Ręka Cesarza jest niezwykle szczodra, ale kąsać rękę, która karmi jest nader nierozsądnie. Zatem aby każdy o tym doskonale pamiętał, nagrody cesarskie są hojne, natomiast kary niezwykle dotkliwe i surowe.

\- Niemniej, cieszy mnie ta interwencja. W sytuacji toczącej się wojny na północnej granicy, przybycie wysłannika samego Cesarza zawsze wprawia w zakłopotanie. - Starszy brat westchnął i napił się wina z pucharu. - Tych których spotkała kara nie zachowywali się rozsądnie niestety. Pieniądze i płynąca z nich władza jest korumpująca. Wszystko jest tutaj spokojne, a nagle ktoś ma za dużo złota i próbuje podważyć porządek. Także dziękuje za sprawiedliwe działanie.

⚫Var Hess uśmiechnął się powściągliwie i skinął głową, w odpowiedzi na podziękowanie.

\-   ⚫Działanie w służbie Cesarstwa to zawsze zaszczyt. - Odparł czarnowłosy mężczyzna o ostrych rysach, patrząc zawsze wprost na swojego rozmówcę. - Bez względu na to, na jakich rubieżach owa służba przebiega... ale wszystko jest lepsze niż Pogranicze, prawda, panie hrabio?

⚫Maksimiliam przygryzł wargę, jakby zastanawiał się nad odpowiedzią, gdyż zdawał sobie doskonale sprawę z tego, że nie wszystkie tereny podlegające Cesarstwu chętnie się mu i jego prawom poddają, a ich znikome znaczenie jest jedynym powodem względnego spokoju ze strony imperium. Chociaż i to pewnie, prędzej czy później ulegnie zmianie.

\-   ⚫Zgodziłbym się z panem - odparł szlachcic prostując się w krześle - jednak jak sam słusznie zauważasz, niesienie prawa i kaganka oświaty przynosi zaszczyt temu, kto weń wkłada swój trud. Poza tym, tamtejsze bory są niezwykle gęste, rozłożyste i obfite w zwierzynę, a polowanie w nich to sama rozkosz. Zdobycz tam złowiona spokojnie mogłaby zdobić cesarski stół podczas biesiad i uczt.

⚫- Hm... Warto to rozważyć, panie hrabio. Chętnie zgłębił bym temat, i może nadarzy się ku temu okazja, jako że zamierzam skorzystać z gościnności Javiera? W każdym razie, niechaj nie czuje pan presji, chociaż byłbym niezwykle rad z wizyty. - Mężczyzna był niezwykle kurtuazyjny w mowie, ale już nie w intonacji, sposób w jaki mówił mocno sugestywny, co oczywiście nie było niczym dziwnym, osoby takie jak on po prostu przywykły do wydawania poleceń.

⚫Emisariusz zakołysał kubkiem z piwem i spojrzał w jego toń.

\-   ⚫Panowie, piję wasze zdrowie, tym zaiste doskonałym trunkiem.  - Tak też uczynił, a puste naczynie postawił na stole i przesunął nieco do przodu, wstając jednocześnie.

⚫- Nie będę już dłużej zakłócał rodzinnej uroczystości żałobnej. - Mówiąc to, wykonał delikatny, ale przybliżony do ukłonu gest, w stronę zakrytych woalami tronów, gdzie według mieszkańców Sterling ostatni raz zasiadali zmarli, milczący ale obecni. Prawdą było, że w głębi serca nieco gardził dzikimi terenami, które zamieszkiwała lokalna szlachta, a może nawet nie samymi terenami, co mentalnością oraz zaściankowością ich lokatorów, jednak był człowiekiem obytym i wykształconym, więc szanował tradycje. Nawet, jeśli w jego stronach obrzędy wyglądały całkowicie inaczej.

⚫ Hastriel var Hess pożegnał się zdawkowo ze wszystkimi, a nim dotarł do wyjścia z sali, już byli przy nim jego ludzie, postawni, solidni i pożądnie uzgrojeni, a także służba z jego futrzanym płaszczem. Jeśli tak nosił się emisariusz cesarski, to jak musiał nosić się sam Cesarz?

\-   Muszę przyznać - powiedział Sloan, kiedy emisariusz odszedł - że pomimo śmierci naszych rodziców i marazmu, ten kutas obudził we mnie wiele emocji, raczej ciepłych.

Javier parsknął, ale nie odezwał się. Cały czas był strapiony sytuacją, na którą nie miał całkowicie ochoty.

\-   ⚫Może nie należało zaczynać rozmowy od sugestii, że nie jest mile widziany, chyba sam doskonale zdawał sobie z tego sprawę. - Odparł Maksimiliam, raczej obojętnym tonem, gdyż samopoczucie posła nie było czymś na szczycie jego priorytetów. - W każdym razie, nieistotne, mało prawdopodobnym jest, że jeszcze kiedyś tu przyjdzie. Zauważyłeś jak wiele osób przybyło, pomimo niespodziewanej wieści? - Dodał ciszej i upił z kubka spory łyk trunku.

⚫Chociaż wszystkie trapiące Sterling sprawy nie zostały rozwiązane, to hrabia Sokół czuł chwilową ulgę, pomieszaną z wyrzutami sumienia. Starał się nie myśleć o tragicznym końcu jaki spotkał władcę Sterling a jego ojca, ale pomimo tragedii, czuł że wszystko ma szanse wrócić tu do względnej normy. W zamyśleniu powiódł wzrokiem w stronę Irvette, teraz to ona zostanie panią na zamku, skończą się wszystkie ograniczenia, którym była podległa.

Kiedy Liam i pozostali mężczyźni pogrążeni byli w smętnym piciu trunków, Irvetta wyglądała jakby była w swoim żywiole. Tym, o który nikt ją nie podejrzewał, znając ją jako radosną i miłą dziewczynę. Ta dziewczyna wyglądała, jakby dojrzała i świetnie radziła sobie w kwestii zarządzania całą ucztą, tak jak wcześniej zapewne organizacją pogrzebu. Można by się zastanawiać, czy to ona przypadkiem nie skorzystała najbardziej z tego, co tutaj zaszło. I nad tym, kto naprawdę będzie głową rodu.

\-   Cóż - Westchnął Sloan - Może nie było to najmądrzejsze. Ale jestem już zmęczony. Ostatni czas to okres ciągłych problemów. Dzisiaj rano zauważyłem pierwsze siwe włosy na głowie.
\-   ⚫I tak późno.
\-   Jakby tego było mało, z tego co rozumiemy z tej sytuacji, to siedzimy na tlących węglach, które lada moment mogą buchnąć ogniem. Spróbuj chociaż nie przyjmować tej oferty i nie przejmować tronu, dobrze?
\-   ⚫Tak? To powiedz mi co, Niedługo Władco, planujesz w związku z tymi rozżarzonymi węglami uczynić? Otrzymałeś jakieś wskazówki od bogów może? - Odparł nawet nie siląc się na złośliwość, sama się jakoś pojawiła niechcący. Miał już dosyć Sterling. Westchnął w końcu i dolał piwa, nie czekając na reakcję sług, najpierw napełnił kubek brata, a dopiero później swój.
\-   Nie wiem, może to kurestwo spalić i zatkać wszystkie dziury? Ryzyko jest takie, że ogień się rozprzestrzeni i spali cały zamek. Można by zalać to wszystko wodą, może z ogrodu? Ale wtedy bardzo intratny interes szlag trafi, jak odprowadzić wodę gdzie indziej. Można niby też zamurować wejścia, spalić księgi opisujące że coś może być pod spodem albo napisać nowe, opisujące co tam jest, żeby jakiemuś mojemu potomkowi nie strzeliło do głowy tam kopać. Tylko, czy to wyjście jest jedynym? Z opisu wynika, że powietrze tam było całkiem czyste, więc musi być inne. Albo magia to robiła. - Napił się mocno z kubka. - Także tyle moich pomysłów, może bogowie mi je podpowiedzieli, ale w takim razie zrobili to nienachalnie.

⚫Młodszy de Falco milczał pijąc i obserwując gości pogrzebowej uczy, sam nie miał dużo lepszego pomysłu, ale chciał chociaż spróbować odnaleźć Rożboha o którym mówił Płomień. Był sprzymierzeńcem rodu, więc odwołanie się do jego instancji nie mogło być gorszym pomysłem, niż bierne czekanie na rozwój wydarzeń lub wprowadzanie ryzykownych metod.

\-   ⚫Daj mi dwa dni, spróbuję coś jeszcze wymyślić, bracie... Swoją drogą, Irvette radzi sobie dobrze jako pani, chociaż widzę iż Cillę posadziła najdalej krańca stołu jak to tylko było możliwe.
\-   Cóż, albo czuła się stłamszona przez życie pod obcasem naszej matki, która potrafiła osiągnąć cokolwiek chciała. Albo nauczyła się tego podczas pracy w ogrodzie i po prostu przeniosła to co potrafiła na większą skalę. A może... - Przerwał, bo pomyślał, że może w jakiś sposób za wszystkim była Irvette, albo chociaż za śmiercią matki. Zastanowił się też nad tym, bo jego żona wolała zdecydowanie rozmowy z Liamem, więc jakoś nie czuł, że w nim znajdzie sprzymierzeńca do rozmów o żonie-zabójczyni teściowych. Napił się tylko znowu z kubka.
\-   Co do dwóch dni, to wiesz. Nic nas teraz nie ściga, chyba, że nagle coś rozpali mój umysł i zmusi mnie do działania. O Cilli lepiej już nie rozmawiajmy, dziewczyna ma głowę na karku żeby nie rzucać się w tej sytuacji w oczy. Myślę, że wie, że Irvetta wie. Zresztą jak tak patrzę teraz na żonę, to jakaś wydaje się być bardziej żywiołowa. - Powiedział to i spojrzał na Liama, jakby oczekując jakiegoś znaku, że ten spał z jego żoną. Kiedyś pomysł wydawał by mu się strasznie śmieszny, ale w obecnej sytuacji to równie dobrze i smoki mogą zacząć latać nad zamkiem.

⚫Liam przeniósł spojrzenie ze Sloana na Irvettę. Rozmawiała z rodziną, uprzejmie, nienachalnie poświęcając każdemu uwagę, właściwie chwilę czasu dla zgromadzonych powinien znaleźć zarówno Sloan jak i Maksimiliam, ale nikt od nich tego nie wymagał, dlatego Irvette wiodła w tym elemencie prym.

\-   ⚫Czy ja wiem. - Mruknął Sokół.

## Zależy mi na tobie
**Postacie: ** Javier, Liam

⚫Javier pozostawał przez większość uroczystości tak cicho, że Liam niemal z zaskoczeniem zauważył, iż kuzyn ciągle siedzi z nimi przy stole, blady i nieobecny, więc pochylił się w jego stronę.

\-   ⚫Javierze? Dobrze się czujesz?
\-   Tak sobie, ale możliwe że chłodne powietrze dobrze mi zrobi. - Powiedział, dając oczywisty znak, żeby razem opuścili salę.

Kiedy wyszli na dziedziniec, zauważyli służbę, która usilnie czyściła coś nieprzyjemnego z podłoża. Dotarło do ich uszu epitety na temat wielkości ptaka, który tutaj nafajdał.

\-   Liamie, wiesz że bardzo mi na tobie zależy, prawda? - Zapytał, kiedy odeszli od zasięgu jakikolwiek uszu. - Nawet pomimo tego, że nie możemy mówić nikomu prawdy.

⚫Powietrze było chłodne, ale niebo jaśniało, a chmury w końcu odsłaniały jego błękit. Maksimiliam razem z Javierem odeszli kawałek, w stronę budynków gospodarczych oraz sokolarni, zaraz za nimi rozprzestrzeniał się sośnina, a pierwsze wiosenne promienie słońca pobudzały soki drzew i w powietrzu zaczął unosić się delikatny, żywiczny aromat. Chociaż wiosna w tych stronach nie bywała przesadnie ciepła, to można było podejrzewać, że niebawem nastaną naprawdę piękne dni, skłaniające do dłuższych spacerów i wypraw na łowy.

\-   ⚫Oczywiście, Javierze. Mnie na tobie także. - Odparł Liam jedną dłonią poprawiając zapięcie peleryny, a drugą opierając się o laskę. -  Bardzo doceniam twoje wsparcie w minionym... nie, nie przerywaj, proszę... naprawdę chcę ci podziękować. Wziąłeś na siebie sporą odpowiedzialność. Nie wybaczyłbym sobie, gdyby coś ci się stało w tej rozpadlinie, podczas wyprawy. Jesteś najlepszym, co mnie w życiu spotkało.
\-   Nie no, oprócz rozwodu z Prianką rzecz jasna. Przepraszam, przeczytałem twój list. - Rozpogodził się nieco, słysząc miłe słowa. - Chodzi o to, że ja i Hastriel zapoznaliśmy się całkiem blisko. I ja czuję się teraz z perspektywy czasu, jakbym cię zdradził.

⚫Liam potrząsnął głową, wyraz zaskoczenia wyraźnie odcisnął się na jego twarzy.

\-   ⚫Nie rozumiem...
\-   Tutaj jesteś Gamon... Panie Maksymilanie - D'oh, która nadeszła od strony budynków gospodarczych, szybko zauważyła, że jej ulubieniec nie jest sam. - Przepraszam najmocniej że przeszkadzam, ale blady chłopiec doszedł do siebie i chce się z tobą widzieć.
\-   ⚫Och. Tak... Wspaniale, już idę. Ja... Tak.

⚫Maksimiliam przez chwilę wyglądał na zdezorientowanego, nie wiedział, czy nadejście D'oh miało miejsce w odpowiednim czasie, z drugiej jednak strony... czuł, że wcale nie chce ciągnąć Javiera za język. Toteż przeprosił go i zostawił samego, idąc do wskazanych przez D'oh komnat. Bardzo chciał pomówić z bladym chłopcem, ale odwiedzenie Gregorego także było na liście priorytetów. Słowa Javiera zupełnie go wybiły, co mógł mieć na myśli? Jaki rodzaj zażyłości? Liam poczuł ukłucie zazdrości, ale oto zatrzymała się D'oh przed jednym z drzwi.

\-   Jest raczej spokojny i ciekawy ciebie, ale nie rób niczego głupiego, bo jest bardzo potężny. Chociaż niepozorny. Może jak jadowity owad? Nie, jest bardziej taki puszysty niż one. Może jeż? No, wchodzimy!

Mówiąc to, otworzyła drzwi.

## Rożboh
**Postacie: ** Lawenda, Liam

W pomieszczenie było bardzo duszno i było pogrążone w ciemności, roświetlone tylko przez coś, co na pierwszy rzut oka było świecą, ale światło wydobywające się z tego nie było chybotliwe. Było tutaj łóżko, a raczej stos siana nakryty wieloma kocami. W najodleglejszym kącie jakieś przedmioty unosiły się w powietrzu, wydając dźwięk jakby skwierczenia albo trzaskania. Najważniejsze było jednak to, że na krześle siedziało blade dziecko, które dopiero po chwili zareagowało, przestając się patrzyć na jakieś dziwne znaki namalowane na ścianie za pomocą kredy.

\-   Witaj potomku zabójców mojego świata. Bardzo chciałem cię poznać.

⚫- Bądź pozdrowiony, Rożbohu. Ja także chciałem cię poznać, wiele o tobie słyszałem. - Odparł, ostrożnie wchodząc do środka. Teraz, gdy tak patrzył na jego bladą postać, wydawało mu się, że widział go w zamku, zawsze kątem oka. - Podobno mamy ze sobą wiele wspólnego.

\- Nie lubię tego miana. Byłem zmuszony współpracować z osobą twojej krwi, ale przyznam, że nie jest zbyt skłonny do współpracy. Gdybym może szybciej z tobą porozmawiał, twoi rodziciele jeszcze by żyli. A nie żyją z mojej winy. - Wskazał unoszące się rzeczy w rogu, na które teraz padło światło z długiego cylindra. Była to wielka księga i kilka zwojów, które wyglądały, jakby unosiły się w mgle, która jednak jakaś niewidzialna siła trzymała wewnątrz przezroczystego słupa. - To ich zabiło. Nie wiedziałem, że nasze środki do niszczenia - tutaj padło jakieś bardzo dziwne słowo, nie podobne do niczego co znał Liam - przez tyle czasu zmienią się na tyle, że zaczną szkodzić ludziom. To zabawne, nie uważasz? Że coś, co było bardzo niewinną rzeczą, zmieniło się w coś co może zagrażać nie tylko tobie i ludziom w zamku, ale też całemu rodzajowi ludzkiemu?

⚫- Myślę, że takich rzeczy jest wiele, aczkolwiek wolałbym uniknąć zagłady... Poza tym, nie mogłeś wiedzieć, że coś co w zamyśle bronią nie było, w którymś momencie się nią stała. - Maksimiliam mając teraz świadomość co znajduje się w pomieszczeniu stracił poczucie jakiegokolwiek bezpieczeństwa. - Chciałbym prosić cię o pomoc w zatrzymaniu serii nieszczęść, które nawiedziły Sterling...

\- Tylko, że nie wiem jak tego dokonać. Nie mogę tam pójść, bo zabiję dziecko, które pozwoliło mi się nosić. To niesamowicie zmniejsza pole manewru. Mam trochę mocy na działanie na odległość, ale jeżeli użyje jej za dużo, również zabiję to ciało. 

⚫Maksimiliam oparł drugą dłoń na rękojeści laski, przez chwilę analizując wszystkie możliwości. Popatrzył przelotem na D'oh, ale ten pomysł wydał mu się nader ujmujący godności, więc zaniechał.

\-   ⚫Powiedz, proszę, co mogłoby zadziałać? Zalanie wodą biblioteki może poskutkować zarażeniem wód gruntowych, natomiast ogień trudno kontrolować.
\-   Brawo! Przemyślałeś to, to nam zaoszczędzi sporo czasu. Cóż, woda owszem wydaje się nie być najlepszym pomysłem, ale gdyby ją wzbogacić tutejszymi solami, to mogła by mieć dobre właściwości odkażające. Tylko że potrzebujemy ich naprawdę dużo. Nie wiem jakimi siłami dysponujecie, ale wymagana ilość to będzie... - zamilkł, myśląc - tyle ile zmieściłoby się w tym pomieszczeniu razy pięć. Ale to spowoduje trudno do określenia zmiany w okolicznej przyrodzie. Można też spalić bibliotekę a potem ją zalać. Trudno będzie jednak powiedzieć, kiedy ogień spali wystarczająco wiele, aby już wpuścić tam wodę. W bibliotece jest podziemna rzeka, można ją wykorzystać do zalania w obu przypadkach, ale problem w tym, że jest tam na dole. Mogę chronić nosiciela, ale w takim małym ciele, jeżeli zaczerpnę za dużo, zabije go.
\-   ⚫A wyniesienie ksiąg? Spalenie ich w jakimś odleglejszym miejscu? To nie wchodzi w rachubę? - Liam ciągle zastanawiał się głośno, szukając najbezpieczniejszej opcji.
\-   One zabijają... Jak mam cię nazywać? - Przerwał nagle swoją myśl.
\-   ⚫Słucham? - Zamrugał zaskoczony hrabia. - Wybacz ten ubytek moich manier. Jestem Maksimiliam de Falco, możesz mówić do mnie Liam, albo Sokół. Jak wolisz... Płomień nazywa cię Bladym Chłopcem, bo chyba nie mogę wymówić twego imienia poprawnie.

Zaśmiał się.

\-   Typowe dla Płomienia. Lubię bardzo lawendę, którą uprawiacie w ogrodzie. Mów mi Lawenda. Wracając jednak do ksiąg. Wiesz dlaczego znacie nas pod postacią wielkich stworzeń, podobnych do jeleni, ale większych? Bo takie ciała miały olbrzymie moce życiowe. Setki lat temu zabiliście je nam i nasze olbrzymie możliwości już nie powrócą. Nie mam opcji, aby chronić gromady osób, które weszły by tam, pracowały wiele dni, a potem wywiozły... Czekaj, tam zeszła jedna osoba oprócz tkacza rzeczywistości i chyba nie zachorowała. Kto to był?

Maksimiliamowi zrobiło się nagle gorąco, nie zwrócił na to wcześniej uwagi, ale istotnie, jedna osoba nie zachorowała.{.c18 .c8 .c26}

\-   Tak, masz rację, był tam mój kuzyn Javier d'Mounier. On, razem z moim medykiem, ten jednak zaniemógł, a do jego śmierci już nie mogę dopuścić.{.c26 .c18 .c8}
\-   Jest szansa, że on jest odporny na to, co tam się znajduje. Oczywiście, to szansa, może z jakiś innych przyczyn nie zachorował. Gdyby jednak był, to może twój pomysł, Sokole, mógły zadziałać. Oczywiście, trwałoby to długo, zatem cała jego rodzina musiałaby pomóc nosić zawartość biblioteki.

⚫- Moją matkę nie ominęło szaleństwo, ona nie była odporna. - Maksimiliam przysiadł na krześle, czując że znów przytłacza go odpowiedzialność. Jednocześnie gdzieś głęboko w sercu kiełkowała irytacja i świadomość, że walczy o swoją sprawę tylko w niewielkim stopniu, że jego rolą było opuścić Sterling i wracać tylko na ważne ceremonie. To Sloan powinien był nad wszystkim trzymać pieczę.

\- Dlaczego chcesz ocalić to miejsce? - Zapytał Lawenda. - Nie wydajesz mi się być władcą tego miejsca, chociaż postępujesz jak taki.

⚫- Nie jestem władcą, ale jestem potomkiem wielkiego rodu, to miejsce to esencja ich trudu oraz potęgi.

\- Powiem mniej uprzejmie, w pewnych sytuacjach jestem w stanie wiedzieć, o czym myślisz. I teraz widzę, że uginasz się pod brzemieniem, które wziąłeś na siebie dobrowolnie. Tak jak ja.

⚫Liam zgarbił się nieco na krześle i pochylił w stronę niepozornie wyglądającego wyrostka, wewnątrz którego przebywała niegdyś niezwykle potężna, prastara istota. Rożboh było słowem, które budziło lęk, Rożbohami straszono do tej pory małe, niesforne dzieci.

\-   ⚫To prawda. - Przyznał niechętnie. - Zgodziłem się dźwigać ten ciężar, tylko dlatego, że zdaje się, nikt inny nie chce lub nie potrafi. Nie mogę znieść myśli, że przez moje zaniechanie coś mogłoby stać się najbliższym mi osobom. Mojemu bratu, Irvettcie, Javierowi czy D'oh, a przede wszystkim poddanym, którzy pokładają zaufanie w moim rodzie. Do tej pory udawało nam się chronić ich przed zagrożeniami z zewnątrz, tym razem niebezpieczeństwo wyziera z głębi trzewi Sterling.
\-   Zatem możesz mnie przyjąć do siebie w takiej sytuacji. Mogę to zrobić tylko wtedy, gdy ktoś naprawdę płonie by zrobić coś, czego sam nie może dokonać. Tak było z tym dzieckiem. Był złodziejem, który kradł z waszej spiżarni, bo znalazł stare przejście. Niestety, zawaliło się ono, ale on nie myślał wtedy o sobie, tylko o rodzeństwie, które przymiera z głodu po śmierci ojca. Poprosiłem o pomoc D'oh, zapewniła im już pomoc. To zabawne zresztą, bo to tąpnięcie spowodował twój ojciec, który szukał dawnych podziemi. I dzięki temu mogłem wam pomagać.

⚫Jęknął cicho i ukrył twarz w dłoniach, potarł ją. Czuł, że ból głowy, który pojawił się rano, teraz wyraźniej pulsuje mu w skroniach. Wypity trunek wcale nie polepszył samopoczucia, przeciwnie, sprawił iż ból tylko się uwydatnił, a decyzje wydawały się jeszcze trudniejsze. Marzył obecnie tylko o tym, aby coś zjeść i się wyspać. I spędzić więcej czasu z ukochanym.

\-   ⚫Muszę to przemyśleć. - Wstał, chyba pierwszy raz zadowolony z tego, że ma pod ręką laską, była wyjątkowo dobrym wsparciem. - Dziękuję, że pomagasz. Ty też powinieneś odpocząć, a chłopcu którego obecnie zasiedlasz, i jego rodzinie, nie zabraknie nigdy chleba.

⚫Maksimiliam skłonił się, lekkim skinieniem głowy i wyszedł. Pierwotnie swe kroki zamierzał skierować prosto do swoich komnat, jednak po drodze znajdował się pokój Gregorego, z którego służka wynosiła właśnie misę z wodą. Hrabia wyciągnął dłoń i przytrzymał drzwi, po czym wsunął się do środka. Słudzy, którzy czuwali przy medyku, na widok pana de Falco wstali natychmiast.

\-   ⚫Jak się ma mój medyk? - Zapytał podchodząc do łóżka. Położył dłoń na jego czole które, ciągle było rozpalone.
\-   ⚫ Nienajlepiej, panie. Dużo majaczy, ale czasem odzyskuje przytomność. - Odparł młody sługa.

⚫- Zmieńcie mu zimne okłady i przygotujcie wywar z kory brzozy... Gregory? Słyszysz mnie? Dzień dobry, musisz teraz dużo walczyć o zdrowie - mówiąc to odgarnął z jego czoła mokre od potu włosy.

\- Widzę... płomienie... - Wyszeptał Gregory, który był tak rozpalony, że czuć było ciepło bijące od niego nawet bez dotykania. - Wielkie kule ognia, a wokół nich okruchy. Poza nimi... ciemność. Boję się. - Jego wargi były straszne suche gdy wyszeptał - wody\...

⚫- Do licha, przecież on się ugotuje we własnej gorączce! Przynieście dużo zimnej wody. Już dobrze, Gregory, już dobrze. Masz. Tu jest woda, tylko powoli, ostrożnie.

⚫Maksimiliam spojrzał po sługach, nie chcieli źle, wyglądali nawet na przestraszonych, a przynajmniej odrobina strachu czaiła się w ich oczach, ale nie dbali o Gregorego tak, jak powinni.

\-   ⚫Pilnujcie, aby nie wysychały mu usta, możecie zwilżać je mokrym brzegiem materiału. Zmieniajcie często kompresy na jego czole i napójcie go wywarem z brzozy... załóżcie mu też suche odzenie, jest mokry, jakby kto go z wody wyłowił.
\-   Ciekawe - powiedziała Ann, a raczej Płomień jej ustami. - Ma taką gorączkę, że powinien już dawno umrzeć.

Płomień stał na progu pomieszczenia, wpatrując się z ciekawością na krzątanie przy chorym Gregorym, któremu słudzy właśnie zmieniali opatrunki na takie, które były namoczone zimną wodą.

\-   Mam teorię, że on to przeżyje. Organizm spali chorobę, ale coś go chroni przed śmiercią wskutek gorączki. Fascynujące. Wiesz może co? - Zapytał hrabiego.
\-   ⚫Mogę tylko podejrzewać. Ale tak, możliwe, że wiem. - Odparł Płomieniowi, natomiast zwracając się do sług wydał im, nim wyszedł, krótkie polecenie. - Czuwajcie nad nim. To, że tu nie bywam parę razy dziennie, nie znaczy, że życie tego człowieka nie jest mi drogie.

⚫

⚫Liam wyszedł na korytarz, razem z Ann, a raczej zaciekawionym Płomieniem.

\-   ⚫Powiedz mi, Płomieniu. - Zwrócił się do niej, gdy już dotarli do jego własnych komnat. - Czy, zakładając że wszystko skończy się dobrze, uważasz że powinienem zachować na służbie Ann? Podatność na zasiedlenie przez prastare istoty wydaje się w szerszej perspektywie nieco niepokojące.
\-   Jeżeli chcesz mieć naprawdę dobrą kochankę, to oczywiście. Poza tym wie wiele z tego co ja się dowiedziałem, więc czasami myślę, że dla naszego dobra, powinna nie przeżyć. Ale tak naprawdę nie chcę tego, gdybym chciał dla niej złego, to natychmiast musiałbym opuścić ją. Zresztą, z tym jest jak z miłość, kochanie jednej osoby nie sprawi, że nagle zacznie kochać więcej osób. No chyba, że fizycznie. Była w tobie zakochana, wiesz? Jednak bycie ze mną rozbudziło w niej prawdziwe pragnienie, którego jedna osoba nie będzie mogła zaspokoić.

⚫-  Nie, nie wiedziałem... Nie miałem pojęcia. - Odparł nieco zaskoczony tą informacją. Patrzył teraz na Ann, chociaż rozmawiał z Płomieniem. To była dziwna sytuacja, na szczęście weszli do komnaty, gdzie Maksimiliam natychmiast otworzył okno i obmył dłonie oraz twarz. Nadal czuł się zmęczony, brudny i bolała go głowa.

⚫- I tak nie miałoby to racji powodzenia.

\- Każdy ma swoje upodobania. Ją ciągnie do osób z władzą, ciebie do kogoś innego, i tyle. Najgorsze, jak nie pozwalamy pragnieniom się spełnić. Ja niestety cierpię, bo w ciele dziecka mój ukochany nie ma żadnych możliwości w tym kierunku.

⚫Hrabia Sokół zaśmiał się. W komnacie Maksimiliama, dzięki uchylonym skrzydłom okna, po chwili zrobiło się nieco chłodniej i można było odetchnąć zimnym powietrzem. Ból głowy nieznacznie dzięki temu zelżał, a Liam pozbył się wierzchniego odzienia.

⚫- Nie sądziłem nigdy, że poznam jakiegoś przedwiecznego Rożboha, co więcej, nigdy nie wpadłbym na to, iż tenże podzieli się ze mną tematem swych namiętności... A wiesz, mówiąc iż nic by nie wyszło z relacji mojej i Ann, miałem bardziej na myśli różnicę naszego statusu, niż cokolwiek innego. Ja jestem panem, a ona sługą. Przyjemność to dla ludzi czasem zbyt mało, by dzielić z kimś łoże.

⚫Myśli hrabiego pobiegły mimowolnie w stronę Prianki, nagle uderzyło go to, że ciągle nosi na palcu ślubny pierścień. Zdejmując go zastanawiał się, czy powinien go odesłać w liście? Nie wiedział, pierwszy raz rozwiązywał umowę ślubną. To małżeństwo było pasmem pomyłek, gorzkim łykiem nieszczęść. Właściwie chciał jedynie, aby Prianka dała mu syna, mógłby wybaczyć jej histerie, szemrane towarzystwo, mógłby wybaczyć wiele, ale bez tego jednego, spajającego elementu, nie było sensu dalej próbować.

\- Z tego co wiem od Ann, to jej nadzieje rozbudziły się właśnie przez Priankę. Ludzie szeptali, że niemożliwe jest aby szlachcianka zachowywała się jak osoba niższego stanu. Twoja matka pomagała ludziom, ale zakładając lecznicę, a nie, biegając po chłopskich chatach. Ann pamięta natomiast jak ochoczo Prianka opisywała życie z tobą. I tylko dlatego, że Ann chciała słuchać. - {.c8 .c18}⚫Opowiadał Rożboh.

⚫ Będąc w postaci Ann wyglądał dosyć niepozornie, tylko roziskrzone spojrzenie i płynne, zgrabne ruchy pozwalały sądzić hrabiemu, że ma do czynienia z kimś innym, aniżeli ze swoją służką. Oczywiście Ann nie była niezdarna, jak na człowieka, wcale nie brakowało jej wdzięku, ale zapewne nie zdawała sobie wcześniej sprawy, że ze swego ciała może wykrzesać znacznie więcej.

⚫- Tak, wiem o tym, że moja małżonka była bardzo... przyjacielska.  - Odparł powściągliwie Liam.  - Jej brak powściągliwości i żywiołowość... to nie zawsze były złe cechy. Mógłbyś ją zrozumieć lepiej niż ja, tak sądzę. Płomieniu, pomóż mnie i twojemu ukochanemu, wtedy będziecie mogli szybciej stąd odejść. On jest słaby, a ja nie potrafię władać żywiołami. Nie ma niczego, co chciałbyś w zamian?

\- Nie masz nic, co mogłoby mnie zainteresować. Wszystko czego potrzebuję to Blady Chłopiec, pomimo że jak mówiłem, nie ma obecnie możliwości na więcej niż rozmowy.  - Spojrzał przenikliwie w oczy hrabiemu i uśmiechnął się, jakby odkrył coś w ich głębi. - Zaproponował ci, że cię posiądzie tak? No proszę, proszę. \- Mruknął, a temperatura w powietrzu zaczęła się podnosić.

\- Chcesz pozwolić mu wejść w ciebie, bo wierzysz że zdoła mnie przekonać do udzielenia pomocy twojemu rodowi? Wy, ludzie, jesteście naprawdę zdumiewający, nie dziwię się, że nas wtedy pokonaliście. I mało tego, coraz bardziej ty sam jesteś dla mnie ekscytujący. - Dokończył wypowiedź i dotknął rozgrzanymi opuszkami piersi Liama.

⚫Liam wzdrygnął się, ale dotyk nie był tak nieprzyjemny jak się tego spodziewał.

\-   ⚫Masz jakiś własny wygląd? - Zapytał wiedziony ciekawością, zaschło mu też w ustach, ale wątpił iż przyczyną tego jest zdenerwowanie.
\-   Tak, ale nie jesteś w stanie go dostrzec ani zrozumieć. Kiedy nawiedzamy kogoś, też nas to zmienia. Może po wiekach przebywania w ludziach nasz prawdziwy wygląd byłby bardzo podobny do waszego, ale nie wiem tego na pewno. Denerwujesz się?

⚫- Nie boję się, ale nie spodziewałem się twojego dotyku. Pamiętaj, że ja ciągle widzę moją służkę, to bardzo dziwne uczucie widzieć ją i wiedząc jednocześnie, że to nie ona.

\- Cóż, na to nic nie poradzę. Jeżeli się zgodzisz, to poznasz ją o wiele bliżej. Będziesz bardziej jak obserwator, to może być dla ciebie bardzo ciekawe przeżycie. Ann lubi tak patrzeć, ale w wyniku jej natury często działamy razem. Nazywa się to dwujestestwo. Też bardzo ciekawe przeżycie.

⚫- Jeśli po tym mi pomożesz odegnać zagrożenie, jakie czai się na mój ród oraz poddanych, to zgadzam się.

⚫Zgodził się hrabia Sokół, a właściwie usłyszał swoje własny słowa wypowiadane przez swoje własne usta. Cały pomysł był tak nierealny, że przez chwilę czuł jakby faktycznie patrzył na wszystko z boku.

\-   ⚫Najpierw jednak muszę się posilić i wyspać, każde ciało ma ograniczenia.

<!-- -->

\-   No to wiesz co robić chyba. Tylko nie mów mu i nie myśl o tym, co tutaj ustaliśmy. Przynajmniej na początku. - Puścił albo puściła oko do niego, po czym wyszła niczym młoda podekscytowana dziewczyna, która spodziewa się wspólnej nocy ze swoim ukochanym.

## To był zaszczyt hrabio
**Postacie: ** Ann, Liam

⚫Mężczyzna siedział na skraju łoża, kropelki potu ciągle ochładzały jego rozgrzaną skórę, a te, które wsiąkły we włosy, skręcały się na karku w jeszcze zgrabniejsze loczki. Czuł się jakby był upojony nieco zbyt dużą ilością słodkiego wina, niby było błogo, ale czuł również, że kręci mu się w głowie od doznań, wysiłku i nadmiaru emocji.

⚫Rożboh nie kłamał, obserwowanie siebie, jakby z oddalenia było niezwykle intrygujące, jednocześnie wszystkie przyjemności ciągle dawały się odczuwać wyraźnie. Było ich w tym miłosnym uścisku więcej niż dwoje, a jednocześnie przecież to pradawne istoty wykonywały każdy gest, doskonale wiedząc co uczynić i jak. Teraz Maksimiliam i Ann zostali zupełnie sami.

- Hrabio Maksimilamie, to był zaszczyt kochać się z Panem. Odkąd mam Płomień w sobie, każde uniesienie było o wiele lepsze od normalnego, ale to teraz\... - przeciągnęła się niczym kocica. Kochali się w pokoju Lawendy w blasku tej dziwnej lampy. Teraz była bardzo przygaszona, ale z łatwością było widać lekko świecące w półmroku oczy, które wpatrywały się w niego z nutą pożądania. - Było czymś najlepszym, co spotkało mnie w życiu. Byli bardzo, bardzo - przeciągała słowa zalotnie - wyposzczeni.

⚫Maksimiliam przeczesał palcami swoje wilgotne włosy i odwrócił się w stronę służki, która dziwnym zrządzeniem losu stała się teraz jego kochanką. Jej powierzchowność wydawała mu się wybitnie miła dla oka, a przecież wcześniej wcale nie dostrzegał jej urody.

⚫- Istotnie. - Odparł, nie mogąc się powstrzymać od lekkiego uśmiechu, który błąkał się po jego wargach. - Byli bardzo siebie spragnieni i pełni namiętności.

⚫Wyciągnął dłoń w jej stronę, leniwie przeciągnął opuszkami palców wzdłuż ramienia kobiety, jej talii i biodra. Powoli. Czując pod palcami jej miękką, ciepłą skórę i odczuwając jej fizyczną obecność w niezwykle intensywny sposób, bo niemal każdym zmysłem.

\-   Jeszcze przed chwilą głupio mi było spytać, czy hrabia ma ochotę na jeszcze jeden raz, ale w takiej sytuacji, grzechem byłoby nie zadać tego pytania. - Mówiąc to włożyła rękę w jego wilgotne włosy, podczas gdy w tej samej chwili dotknęła jego piersi swoimi sutkami. - Może spróbujemy czy podoba nam się to też, gdy w pełni kontrolujemy sytuację?

⚫Echo minionych niedawno przeżyć mających miejsce w tym łoży, powracało przy każdym dotyku przyjemnym dreszczem rozchodzącym się po całym ciele.

\-   ⚫Ann...  - szepnął Liam, pomiędzy paroma niespiesznymi, ale szalenie przyjemnymi pocałunkami. Wydawało mu się jakby, dzięki oddaniu kontroli Rożbohom, wszedł na nowy poziom odczuwania, pełnego intensywnych, niezwykłych doznań.  - \...nie teraz. Na pewno już nie dziś. Chociaż rozumiem twoją ciekawość... po prawdzie też ją podzielam.

⚫Wstał z siennika, szukając swego rozrzuconego prawie po całej komnacie odzienia. Myślał, że godząc się na ten układ z Płomieniem, będzie musiał zdobyć się na coś upokarzającego, niegodnego jego szlachetnie urodzonej osoby. Tymczasem wcale nie czuł się zbrukany, jedyne co mogło powodować jakiekolwiek wyrzuty sumienia, to fakt, że czuł się wybitnie dobrze, może nawet lepiej niż przy Javierze, ale tak naprawdę były to dwa zupełnie różne rodzaje miłości - ta, wypełniona wyłącznie rozkoszą, a ta do Javiera także uczuciami.



## Strach przed decyzją
**Postacie: **Liam, Irvetta

⚫Regenerujący sen nie trwał wcale długo, był pozbawiony marzeń sennych, za to głęboki i oczyszczający umysł. Hrabia Sokół obudził się przed świtem, doprowadził się do porządku, tym razem nawet nie wołając do tego sług. Spodziewał się że po tak intensywnym wysiłku rana w nodze będzie pulsowała tępym bólem, jednak wcale nie było tak źle. Czuł co prawda mięśnie, jak po nieco zbyt dużym, ale nie na tyle, aby odpuścić sobie to, co miał w planach.

⚫ Naciągnął cięciwę, wstrzymał oddech, wycelował i puścił lotkę. Strzała pomknęła do celu, rozcinając powietrze śmiercionośnym grotem. To, że nie mógł, tak sprawnie jak kiedyś, walczyć mieczem, nie oznaczało, że powinien całkiem zrezygnować z obrony. Celność i zręczność były zdolnościami, które hrabia Sokół pielęgnował z dużym zaangażowaniem. Nie czuł się na siłach aby wyruszać teraz na łowy, ale strzelanie do tarczy także było nienajgorszym treningiem.

⚫Sięgnął do kołczanu po kolejną strzałę, las szumiał nieopodal, wiatr nieco uspokoił się, w powietrzu pachniało wczesną wiosną i te wszystkie drobiazgi uderzyły go właśnie są tak bardzo znajomą naturą, że nie zwracał wcześniej uwagi na to, iż jest w domu. W znajomym miejscu, gdzie dorastał, gdzie znał każdy zakamarek. Gdzie ćwiczył z nim, albo ze Sloanem ciosy i uniki, gdzie D'oh uczyła go tropienia i zajmowania się myśliwskimi zwierzętami.

Zbliżała się pora wspólnego posiłku z Irvette, o którym poinformowała go służba, kiedy wychodził na zewnątrz. Pewnie miała to zrobić zaraz po jego obudzeniu, ale nie byli gotowi na tak wcześnie stającego szlachcica. Mieli go zjeść tylko we dwoje, ale nad wszystkim wisiała jakby cień sugestii tego, jak zmieniły się relacje. Wcześniej przez bycie bratem następcy rodu i długiego pobytu w zamku, był niejako wyżej niż Irvette. Czy teraz na spotkaniu chce powiedzieć że dawne miłostki są przeszłością, czy nadal podtrzymuje zainteresowanie hrabią? ⚫Zastanawiając się nad tym, oraz nad kwestią niestosowności jaką mogło być śniadanie wyłącznie z bratową, przebrał się do wspólnego posiłku.

⚫- Irvetto. - Hrabia widząc ją uśmiechnął się i skłonił. - Miło, że znalazłaś czas na wspólny posiłek. Sloan do nas nie dołączy? - Zapytał podchodząc do stołu.

Irvetta ubrana była w butelkowo zieloną suknię, ale bez żadnych ozdób pomijając drobne zdobienia na brzegu rękawów przy dłoniach. Posiłek mieli zjeść w jednej z naw z pięknym widokiem na ogród, niedaleko małego strumyku spływającego barwnie ze ściany. Tym samym, który w kilku wersjach powstrzymania tego, co jest pod ogrodem, miał zostać powiększonym i użytym do zalania terenów poniżej.

\-   Nie, nie dołączy. - Westchnęła cicho. - Jak sobie radzisz z tym wszystkim Liamie?

⚫- Radzę sobie, Irvetto. Dziękuję. - Odparł odsuwając dla niej krzesło i samemu siadając na swoim. - Dzisiaj dopiero uzmysłowiłem sobie, że jestem w domu... I że zmieniło się tutaj niemal wszystko, chociaż mury stoją jak stały. Ubolewam nad stratą ojca - przyznał szczerze - i nawet nie nad samym faktem jego odejścia, bowiem jadąc tu byłem przekonany iż jego dni są policzone, i że mogę się pożegnać... Nie pożegnałem się.

\- Może mi powiesz, co byś mu powiedział? Może trochę ci to pomoże? Nie jest to to samo, ale jak przybyłam tutaj, nie mogłam nikomu powiedzieć, jak się naprawdę czuję, nawet tobie, chociaż tyle potrafiłeś wtedy zrozumieć. - Uśmiechnęła się na wspomnienie czasów, gdzie coś, co wydawało się dramatem, z biegiem czasu okazało się niczym otarcie na kolanie dziecka, które się przewróciło. Wtedy straszne, dzisiaj tak bardzo nieistotne.

⚫- Nie wiem co bym mu powiedział, Irvetto. Natomiast widzę teraz, z perspektywy czasu, jak mocno przygniótł go ciężar obowiązków i odpowiedzialność za Sterling... - westchnął głęboko. - Musisz wiedzieć, że bardzo doceniam to co zrobiłaś przez ostatni tydzień. Nie byłem w stanie przygotować ceremonii, wszystko pamiętam jak przez mgłę i nawet nie wiem kiedy przyszło mi zasnąć.

\- Dziwnie to zabrzmi, ale dało mi to wszystko poczucie sensu, że coś ode mnie zależy i mogę działać. Cena za to była olbrzymia, ale - ściszyła głos - nareszcie czuję się jak u siebie. To okropne uczucie.

⚫Liam tylko pokiwał głową, dając znać, że doskonale ją rozumie, po czym napełnił jej kubek chłodnym już naparem z sosnowych pędów i miodu. Śniadali w bardzo miłym dla zmysłów otoczeniu, szemrzący nieopodal strumyk, zieleń i towarzystwo Irvette naprawdę mogły koić zmysły.

\-   ⚫Mówiłem, że tak się stanie, kiedy... nie będzie już nad tobą Kylie.
\-   Tylko, jeżeli słowa pocieszenia okazują się przepowiednią tego, co wydarzy się tydzień potem, mam prawo być zaniepokojona. Natomiast cieszę się, że wreszcie rozmawiamy. Javier stał się strasznie osowiały, z mężem moim ciężko się rozmawia, bo ciągle jest gdzie indziej myślami. Ty byłeś wyczerpany, ale widzę, że po tym długim śnie wyglądasz lepiej niż w dniu w którym tutaj przyjechałeś. Chyba. - Zaśmiała się dziewczęco, jak zawsze to robiła. - Chyba, że sobie kogoś tutaj przygruchałeś, żeby potem lepiej sypiać?

⚫- Albo widzę kres problemów nękających Sterling. - Odparł równe lekkim tonem. - To prawda. Kamień z serca, pomimo tylu strasznych w gruncie rzeczy wydarzeń. Niestety, obiecałem emisariuszowi ze stolicy, że znajdę czas na rozmowę i odwiedzę go, gdyż rezyduje obecnie u Javiera. Zaproponowałbym, abyś wybrała wraz ze mną, lecz zgaduję iż prawdopodobieństwo spotkania... którejś z sióstr Javiera nie sprawiłoby radości twej osobie.

\- Ah, nie przejmuje się tą głupotą i dziecinnością mojego męża. Już ta mała zołza tutaj nie wejdzie, kiedy ja mam coś tutaj do powiedzenia. Pojechałabym tam z wielką chęcią, ale tutaj jest masa rzeczy do zrobienia i to w normalny dzień, a co dopiero po takich wydarzeniach. Zresztą, poradziliście sobie z tym, co jest w podziemiach? Nie chcę być nieuprzejma, ale wiedza o tym zagrożeniu nie pozwala mi zasnąć, więc albo czekasz na coś, albo już masz przygotowane rozwiązanie, prawda?

-⚫ Tak, Irvetto. - Sięgnął po jej rękę i nieznacznie się pochylił aby ucałować wierzch jej dłoni. - Jestem niemal pewien rozwiązania, i to na tyle mocno, aby sypiać w nocy, więc ty też bądź spokojna.

⚫- Och\... - Jej policzki natychmiast zaróżowiły się od tego gestu.

⚫Liam uśmiechnął się lekko, widząc jak Irvetta pąsowieje. Chociaż nie chciał jej zawstydzać, to jednak wyglądała naprawdę słodko, i jak na dorosłą kobietę miała w sobie wiele dziewczęcego wdzięku. Maksimiliam sięgnął po kubek, część posiłku spędzili w milczeniu lub rozmawiając o mało istotnych, ale przyjemnych sprawach. Powspominali też parę przygód z przeszłości, niektóre nie należały do dobrych, ale czas zatarł złe odczucia z nimi związane i teraz jawiły się jako niewiele znaczące anegdotki.

\-   ⚫Jak się czuje Fenilla? Nie widziałem jej od paru już dni.
\-   Głównie czas spędzała w ogrodzie, mówiła że tutaj jest najspokojniej. Jest jej też smutno, bo zniknął jej blady towarzysz. Byłam na niego zła, ale jak patrzę teraz, jak się snuje z ponurą miną, to mi jej żal. Jakby zabolało ją to znacznie mocniej, niż śmierć dziadków. Może ją zabierzesz ze sobą w podróż? Mogłoby ją to nieco rozruszyć.

⚫- Mógłbym w sumie. Jeśli zechce.

\- Ale ja mam jedno pytanie. Co z nami? Czy wtedy mnie tylko podnosiłeś na duchu, czy faktycznie myślisz o mnie poważnie? Sytuacja się zmieniła, ale to ciebie darzę o wiele cieplejszym uczuciem niż mojego męża.

⚫- Mówiłem poważnie, lecz... to wcale nie jest dobry pomysł... -  rzucił tym gorzkim stwierdzeniem.

\- Wiem Liamie. Kiedyś staniemy przed obliczem bogów którzy spojrzą na nasze życie i ocenią je. Nigdy zatem nie unikniemy osądu nad naszymi czynami, wiemy to dobrze. - Odchyliła się na krześle, patrząc się gdzieś na bok. - Chciałabym wiedzieć, kim jesteś dla mnie. I jeszcze ta cała zagrywka twojego brata, który zagrał właściwie o wszystko, bo sądzi, że mu się nie postawisz. Po co to wszystko, po co te słowa i zapewnienia? Mógłbyś wziąć tę władzę, a po niej odebrać mnie jemu. Co sprawia, że tego nie robisz? Nieobliczalność Sloana?

⚫Maksimiliam pokręcił głową.

⚫- Irvetto, nie chcę stracić brata. Ani domu. Znam Sloana prawie już czterdzieści lat, i chociaż nie jest pozbawiony cnót, to ma ich dosyć skąpą ilość... ugodowość nie jest jedną z nich, pokora, ani wyrozumiałość też nie. Za to nie można mu odmówić determinacji. Ty sobie poradzisz bez Sloana, a Fen bez ojca? - Westchnął głęboko. - Nie podjąłem jeszcze ostatecznej decyzji w sprawie sukcesji, ale jak widzisz rozważam wszystkie możliwości. Dziękuję za wspólny posiłek. \- Mówiąc to odsunął swoje krzesło.

\- Zatem ze strachu przed decyzją, stracisz mnie - powiedziała cicho do siebie, kiedy już odszedł, a łzy płynęły po jej policzkach jak strumyk po pobliskich skałach.


## W drodze do Javiera
**Postacie: ** Fen, Liam

\-   ⚫Ile właściwie masz lat, Fen? - Zapytał hrabia, przyglądając się swojej bratanicy. Powóz turkotał a wspinające się dopiero po niebie słońce wpadało co jakiś czas do środka, długimi, złotymi promieniami, o ile udawało im się przebić pomiędzy leśne poszycie.
\-   Sześć lat, stryju - powiedziała radośnie. - Będę tyle miala na wiosnę, a to zaraz, więc sześć,  a ty?

⚫Uśmiechnął się, a zmarszczki przy oczach stały się przez to wyraźniejsze.

\-   ⚫To zależy do ilu potrafisz liczyć, Fen. Bo zabrakłoby twoich i moich palcy u rąk, aby to zliczyć.
\-   ⚫No tak, czyli sto! Chociaż nie, to za dużo, sto to ma pewnie D'oh. No to pewnie z osiemdziesiąt, bo jeszcze nie w grobie, ale sporo już za sobą. - mówiłą dumna ze swojej logiki.
\-   ⚫To bardzo sensowne - przyznał rozbawiony jej wnioskami. - Połowa osiemdziesiątki. Mniej więcej tyle mam wiosen... albo zim, bo urodziłem się w zimie. Na dworze hulały śnieżne zawieje i wyły wilki, a twój ojciec był młodszy niż ty teraz.
\-   ⚫Też był taki ponury jak teraz? Bo to dziwne, ty stryju nie jesteś ponury, a on ciągle jakby zjadł coś niedobrego i się boczy.

⚫Liam starał się nie roześmiać, za to udał, że mocno się nad jej pytaniem zastanawia.

\-   ⚫Może jest niedźwiedziem, niedźwiedzie są mrukliwe. - Puścił do niej oko. - To by nawet pasowało, twój ojciec jest silny jak niedźwiedź i równie odważny. Fen, jedziemy do domostwa twojego wuja, Javiera, jego siostry bardzo się ucieszą z twojej wizyty, najmłodsza jest tylko trochę od ciebie starsza, z tego co pamiętam.
\-   No ja pamiętam lepiej, bo jest o dwa lata starsza. - Była dumna z tego, że pamięta coś lepiej od stryja, który był przecież taki mądry. - Wiem gdzie jedziemy, mama mi wszystko powiedziała, łącznie z tym, że będziesz rozmawiał z ważnym panem od cesarza i mam nie przeszkadzać. Cesarz jest groźny, prawda?
\-   ⚫Jest potężny, a to oznacza, że może być groźny. - Mężczyzna pochylił się w jej stronę i pogładził ją po głowie. - Pamiętaj, aby nigdy nie mówić źle o cesarzu. I nie z powodu strachu, tylko z uwagi na szacunek na który zasługuje.

⚫Podróż powozem była dłuższa niż konna, ale nie stawała się przez to bardziej uciążliwa. Tym bardziej, że pogoda zaczynała dopisywać i pomimo leżącego tu i ówdzie śniegu, poszycie lasu zaczynało się zielenić a pierwsze kwiaty wystawiały ku słońcu główki.

## Cesarz pewnych rzeczy nie daruje
**Postacie:**Hastriel, Liam, Javier

⚫Hastriel cofnął się zaledwie o jeden krok, ale tyle wystarczyło aby stracił równowagę. Niefortunnie, bo akurat w momencie gdy samura, czując się osaczoną i zmęczoną pogonią, ruszyła na przypadkowy cel, rozpaczliwie próbując umknąć przez przeznaczeniem. Hastriel var Hess stał się celem najłatwiejszym, a przestraszony dzik zaczął szarżować z głośnym kwikiem. Wszystko działo się niesamowicie szybko. Coś, co z początku miało być wyprawą na zające rozrywką i uzupełnieniem zapasów w jednym, mięsiwem na ogień dla obstawy cesarskiego emisariusza, nagle stało się przybierającą nieprzyjemny obrót walką.

⚫Maksimiliam, niewiele myśląc, wypuścił strzałę, ta pomknęła prosto do celu i przeszyła oczodół dzika, którego ciosy skierowany były już wprost na Hastriela. Zwierzę zawyło oślepione i skręciło gwałtownie, tym razem w stronę Liama, ten wyszarpnął miecz i wbił go w ciało dzika najmocniej ja mógł, uskoczył tylko trochę, ale laska którą miał wsuniętą w kołczan od pewnego czasu, wypadła i pod kopytami dzika zmieniła się w drzazgi i zgniecione srebrne wstawki.

⚫W całym zamieszaniu Javier coś krzyczał, trzech uzbrojonych mężczyzn rzuciło się by dobić zmęczone, na wpół oślepione i zranione zwierzę.

Hastriel dochodził do siebie przez chwilę, leżąc na ziemi. Przestraszył się nie na żarty, bo jego kuzyn zginął w podobnej sytuacji, tylko, że tam nie było pod ręką tak wspaniałego łowcy. Myślał tylko, że chociaż nie jest to popularny kandydat, to zrobi wszystko, aby to on został Wielkim Łowczym. Może i jest praktycznie nieznany w stolicy, ale potrafi mówić gładko a w dodatku powinien być bardziej lojalny, niż dowolny dworzanin. Choć oczywiście, łatwo być bardziej godny zaufania niż dowolna z tych żmij. Gdy ktoś z jego przybocznych pomógł mu wstać, podszedł do Liama i z szacunkiem powiedział:

\-   Dziękuję. Sytuacja była poważna, ale wy zrobiliście co należało i to bezbłędnie, panie hrabio. 
\-   ⚫Ja także jestem wielce rad... - odparł Liam chwytając rękę kuzyna, gdyż to właśnie Javier pomógł mu wstać. Grymas bólu przemknął przez oblicze hrabiego, ale najmniejszego zamiaru nie miał się uskarżać. - Nie darowałbym sobie, gdyby coś się panu stało na polowaniu\...

⚫"Cesarz też by nie darował" dodał w myślach Javier.

\-   ⚫Z resztą, każdy zrobiłby to samo.
\-   ⚫Ale nie każdy ma tak pewną ręka i bystre oko, Sokole. - Wtrącił Javier i jęknął rozczarowany. - Cóż, wypadkowi uległ zatem jeno dzik oraz twoja laska, Liamie.
\-   Myślę, że dzik uległ panu hrabiemu a nie wypadkowi, Javierze - Ironicznie zaznaczył Hastriel, po czym odwrócił się do ludzi przy byku i ostro odezwał się do nich - Hej, przewieźcie tego dzika do obozu, ruchy!
\-   Zadowoliłyby mnie i moich ludzi zające, a tutaj proszę. Najemy się dzisiaj nie tylko strachu. - Zażartował, wsparty rękami na biodrach i obserwując towarzyszy pakujących zwierzę na improwizowane nosze. Wyglądał tak, że pasowałby również do obserwowania pola walki po wygranej bitwie.

⚫Obozowali niemal pod murami rodowej siedziby Javiera, Hastriel miał komnatę przygotowaną tylko dla niego i chociaż korzystał z gościnności, to prócz nocy, dużo czasu spędzał ze swymi ludźmi. Z pewnością czuli nad sobą jego protekcję.

⚫ Teraz nadziany na rożen i oskórowany dzik piekł się powoli. Mięso zasmakowało powoli ognia i dymu, a przecież od polowania mijała trzecia godzina. Ludzie Hastriela zmieniali właśnie wartę, co ich dowódca w towarzystwie lokalnej szlachty, mógł obserwować sącząc rozcieńczone wino.

⚫- Nadal nie może mi wyjść z głowy obraz pana hrabiego - to mówiąc spojrzał na rzeczonego i pokiwał głową z uznaniem - jak powściągające nerwy i napinając cięciwę doskonały byłeś w swem kunszcie. Nie było przesady w twych słowach, Javierze. Zaiste pan hrabia jest pysznym myśliwym, jakieś jeszcze talenta skrywa? \- Zapytał uprzejmie, nieco w formie prawionych komplementów, jednak z lekkim uśmiechem błąkającym się po ustach.

\- Maksimiliam - powiedział, gryząc kawałek chleba z uwagi na to, że zgłodniał - potrafi rzeczy naprawdę dobrze ujmować słowami i swym urokiem. Z jednej strony szkoda, że to nie on jest pierworodnym, który obejmie władzę w Sterling, ale z drugiej strony, może to i lepiej? - Machnął ręką w kierunku zachodnim, mniej więcej w kierunku stolicy. - Ciągle mu mówię, że w stolicy byłoby mu świetnie, ale on ciągle mówi, że ma swoje obowiązki i powinności na swoich ziemiach. To człowiek odpowiedzialny stety i niestety.

⚫Emisariusz zakołysał winem w kubku zastanawiając się.

⚫- Tak, panie hrabio. Javier ma rację, z pewnością jeśli rzecz się tyczy stolicy. Ciągle mało jest nam ludzi wielkich i doskonałych w swym... rzemiośle, chociaż nie, to byłoby ujmą... w swojej sztuce zatem, niech tak to ujmę. Pan, panie hrabio może nie być przekonanym, ale gdyby cesarz powołał pana na dwór, nie odmówiłby pan, prawda? Tak się składa, że widziałbym was, hrabio, jako Wielkiego Łowczego, a na dowód pańskiej doskonałości mam własne zdrowie lub nawet życie.

⚫Javier klasnął w dłonie, zapewniając przy tym, że wybór ten byłby doskonałym. Natomiast Maksimiliam, mimo zaskoczenia, ciągle trzymał fason. Skinął głową w uprzejmym ukłonie, dostrzegając bystre spojrzenie czarnych oczu Hastirela.

\-   ⚫Byłbym zaszczycony. Pańskie polecenie już jest dla mnie wielkim wyróżnieniem i sowitym podziękowaniem za ochronę zdrowia. - Odparł Maksimiliam.
\-   ⚫Jesteś skromny, jak zwykle. - Stwierdził Javier zerkając w stronę posiadłości.

⚫Rudowłosa niewiasta machała w ich stronę, Javier domyślając się iż jedna z jego sióstr potrzebuje jego pomocy, lub z nim pomówić wstał powoli i przeprosił szlachetne towarzystwo, więc Maksimiliam i Hastriel zostali sami w to leniwe przedpołudnie, z widokiem na ludzi var Hessa pilnującego pieczącego się dzika.

\- Baron jest naprawdę zacną osobą, prawda Panie hrabio? - Zagadnął go emisariusz po chwili. - Poznałem go kiedy przyjechał w dość niefortunnych okolicznościach do stolicy, gdy polowano na organizatorów nieudanego puczu. Miał małe szanse na powodzenie, bo wiedzieliśmy o nim, ale przypadek sprawił, że idealnie wtedy przybył baron. Gdyby nie mój instynkt, mogło by coś go spotkać, a tak spędził trochę czasu w mojej rezydencji. Która oczywiście będzie dla Pana stała otworem, jeżeli odwiedzicie stolicę. 

Kiedy var Hess mówił to wszystko, jego twarz nie wyrażała niczego jednoznacznego. Równie prawdopodobne było to, że pomógł Javierowi uniknąć nieszczęść, jak i to, że Javier należał do spisku a on wykrył to zawczasu, ale w jakiś sposób, może szantażem, wyperswadował mu ten pomysł.

⚫Maksimiliam nie był pochopny w swoich osądach, a nawet jeśli jakieś przypuszczenia zgodziłyby się w jego głowie, z pewnością nie wypowiedziałby ich na głos. Nie teraz. Natomiast podziękował skinieniem głowy i sięgnął po gąsiorek z winem, które wczesnym rankiem przyniósł Javier, a które obecnie niechybnie się kończyło. Napełnił nim kubek Hastriela a następnie swój.

\-   ⚫Rozumiem. Dziękuję za zaproszenie, niechybnie skorzystam, gdy odwiedzę stolicę. Pierwej, oczywiście uporam się z powinnościami w Sterling, następnie ze sprawami na Pograniczu, ale tam nie zabawię długo. - Odparł hrabia Sokół, dzieląc się tymi pobożnymi życzeniami.

⚫Na obliczu szlachcica zmęczenie było wyraźne. Utrudzony był porannym polowaniem oraz problemami rodowymi. Pozbawiony laski bardziej nadwyrężał nogę i czuł okolice rany wyraźnie i nico dotkliwie, chociaż wino było w stanie nieco uśmierzyć ból. Podejrzewał, że Hastriel także nieco się poobijał podczas upadku, aczkolwiek duma nie pozwalała mu nawet o tym napomknąć, a tym bardziej uskarżać się.

\-   Pogranicze to bardzo dziwne miejsce. Opiera się tak bardzo próbą zasiedlania, chociaż z mojej wiedzy wynika, że miejscowy książę radzi sobie najlepiej z tym zadaniem ze wszystkich do tej pory ludzi wyznaczonych do tego zadania. To, że wybrał Pana, Panie hrabio na swojego wasala dobrze o was świadczy. Proszę powiedzieć, jest tam co zobaczyć dla takiej osoby jak ja, która lubi cywilizację i wygody?

Zapach dolatujący z rożna pozwalał sądzić, że zbliża się moment posiłku. Ludzi wokół niego były gwarnii, ale brzmiał on inaczej od dajmy na to gwaru karczmy. Oni nawet będąc głośni byli uporządkowani i karni.

⚫- Zapewne poznam pańskie gusta lepiej... aczkolwiek natenczas nie sądzę, aby Pogranicze było dla pana kuszącą opcją. Jest nieokiełznane i dzikie, piękne w całej swej istocie, nie przeczę, lecz zdecydowanie dalekie od miejskiej wygody. Pokusiłbym się nawet o stwierdzenie, że w pewien sposób Pogranicze jest miłe memu sercu, niepozorne a ciągle stanowiące wyzwanie.

⚫Czarne, bystre oczy Hastriela przypatrywały się hrabiemu, poza tym uśmiechał się nieznacznie, słuchając słów towarzysza, bo Maksimiliam był człowiekiem wielkiej pasji, a tacy zawsze brzmią ciekawie i rozbudzają wyobraźnię nawet osób zatwardziałych i zgorzkniałych życiem.

\- Ciekawe. Pewnie właśnie dlatego ma pan takie serce do układania dzikich zwierząt. To prawda, że uczyła was, panie hrabio, tej sztuki półorczyca? Nigdy żadnej nie spotkałem, jaka ona jest? - Pytając pochylił się w stronę szlachcica, słuchając zarówno uprzejmie i jak i uważnie.

⚫- Odważna, lojalna i prostoduszna\... - Odparł bez zastanowienia i nieco chaotycznie Liam, a że rychło zdał sobie z tego sprawę, dalszy opis był dużo bardziej składny. - Nazywa się D'oh. Z pewnością jej podejście do zwierząt wiele nauczyło mnie o zwierzęcej naturze i dało mgliste pojęcie o tem, jak ją ujarzmiać. Natomiast... proszę pozwolić, panie var Hess, żebym i ja czegoś dowiedział.

\- Panie hrabio, proszę pytać. Najwyżej zasłonię się tajemnicą służby cesarskiej, jeżeli będzie trzeba. - Wskazał dzika, którego powoli zaczęli przygotowywać do zdjęcia jego podkomendni. - Jednak nie dłużej, niż zajmie nam oczekiwanie na podanie stawy. W porządku? - Jak zawsze w takim pytaniu, emisariusz zawierał niesamowicie mało pytania, a dużo stwierdzenia.

⚫- Jakie obecnie nastroje panują w stolicy? Pytał mnie pan o D'oh, ale nie zauważyłem w panu odrazy, chociażbym się jej spodziewał.

Hastriel nagle spochmurniał i spojrzał głęboko w oczy Maksimiliamowi.

\- Nie wszystkie moje zadania dla cesarza są tak przyjemne i miłe jak obecna wizyta. I w części tych zadań korzystamy z pomocy tych, którzy dla innych są niepożądanym towarzystwem. Ich pomoc jest często znacznie większa niż ludzka a o lojalności już nie wspominając. - Potarł odruchowo nadgarstek, ale tylko przez chwilę, jakby nie chciał aby zwróciło to uwagę hrabiego. - W stolicy jest lekko niespokojnie, bo próba przewrotu i powrotu pewnych starych porządków chociaż nieudana, ma jedną nieprzyjemną cechę - była. To zaś powoduje, że nawet kiedy jest już bezpiecznie, to ludzie się niepokoją, bo a nuż jednak znowu coś się stanie. I, rzecz jasna, boją się też o to, że mogą zostać oskarżeni o zdradę. Słusznie, niesłusznie - czasami wystarczy być w złym miejscu o złym czasie jak Javier. Cesarz nie jest skory do karania kogo popadnie, ale jego doradcy, którzy chcą się wykazać i zabezpieczyć sobie karierę, już tak. Jedynie kto jest całkowicie bezpieczny to ludzie jak ja, zawsze lojalni i tacy jak ty - całkowicie poza kręgami władzy, robiących to co do nich należy daleko od stolicy. Wejście pod ochronę obu typu osób jest teraz bardzo, bardzo cenione.



## Nie wiem
**Postacie:** Liam, Javier



⚫Liam wsunął palce w piękne, rude loki Javiera i przeczesał je delikatnie, ciesząc się ich kolorem oraz poruszającym jego zmysły aromatem. Byli sami w jego przestronnej, dobrze urządzonej komnacie, i tym razem nigdzie nie musieli się spieszyć, przynajmniej tej jednej nocy, aż do samego rana.

⚫- Wiesz jak mi zawsze pachniesz? Jak zboże latem, gorące od słońca. -- Wyszeptał wtulając swe usta w jego miękkie loki i upajając się nimi. Młody baron roześmiał się przestając na chwilę rozpinać kaftan swojego kochanka i podnosząc na niego wzrok.

⚫- Pan hrabia życzy sobie ujrzeć mnie jeszcze bardziej zarumienionego? Bardziej już się nie da. Sprawdź.

⚫- Przecież już nie pąsowiejesz, jak onegdaj, od byle komplementu, mój piękny.  -- Liam pogładził jego policzek i odgarnął włosy z czoła, czule acz zupełnie niepotrzebnie, bowiem te od razu wróciły na swoje miejsce. -- Dużo się zmieniło, od... sam nie wiem od jakiego czasu. Może mi umknął. Nie zauważyłem kiedy przybyło ci tylu trosk, że przestałeś mi mówić prawdę.

Javier zamarł jak dziecko przyłapane na czymś, co myślało, że nie wyjdzie na jaw. A przynajmniej tak szybko. Czuć można było jak zesztywniał cały i nagle patrzył wszędzie indziej, ale nie na ukochanego.

\- Ja... - Powiedział cicho, a jego serce biło nagle mocno i szybko, a w gardle czuł straszną suchość. - On...  - znowu urwał. W jego myślach kotłowało się wielkie poczucie winy, ukryte pod codziennym uśmiechem. Po policzkach spłynęły mu łzy. - Przepraszam cię, ale z nim było inaczej niż z tobą. - Powiedział to dalej nie patrząc na Liama, a hrabia westchnął ciężko.

⚫- Nie frasuj się, nie o to pytam. Po części z szacunku do ciebie, a po części z szacunku do moich własnych uczuć. - Odparł odchodząc parę kroków, ale bez celu. Stanął przy toaletce bezmyślnie przesuwając przedmioty, podczas gdy mówił. - Co ci do głowy strzeliło mieszać się w politykę? Nie lokalną, związaną z twoją ziemią, tylko w samym sercu cesarstwa? Toć nie jest to zabawą. Nie wiem, coś tam robił, ale wiem, żeś nieostrożny.

\- Mój ojciec nie ma jak wiesz dużego majątku, a bez niego wydanie za mąż moich sióstr jest bardzo trudne... - Mówił ktoś w ciele Javiera, głosem oschłym, wyzbyty emocji - To nie ja się mieszałem, ale mój ojciec. Miałem doprowadzić do otwarcia południowej bramy miasta, ale Hastriel musiał od kogoś wiedzieć, że użyję swoich wpływów aby wywrzeć presję na kapitanie tamtego garnizonu, i pochwycił mnie, po czym umieścił w swojej rezydencji. Nie tylko spędzałem z nim noce, ale też zacząłem dla niego pracować. I tak na przykład jego wizyta tutaj i przechwycenie kopalni to moja zasługa. Jedna z moich zasług.

⚫Maksimiliam wypuścił całe powietrze z płuc, oparł się obiema dłońmi o mebel i milczał pogrążony we własnych myślach, analizach, możliwych rozwiązaniach, alternatywnych scenariuszach wydarzeń, ale wszystko przedstawiało się w gruncie rzeczy kiepsko.

⚫- Ma cię w szachu zatem. - Stwierdził cicho. - I to nie twoją było przewiną. Chociaż przyznam, że jego wersja wydarzeń jest szlachetnie zgrabna. - Stwierdził, nie mogąc ukryć złości, która pobrzmiewała w jego głosie, chociaż starał się ją ukryć. Z resztą gniew, zamieniał się powoli w żal, szczególnie gdy hrabia podniósł wzrok i ujrzał udręczoną twarz tak bliskiej mu osoby. - Czemuś to wszystko zataił, Javierze? Czemu dowiaduję się o tej relacji twojej z cesarskim protegowanym w dniu, gdy żegnałem rodziców? I od niego samego, mimochodem, żeś znalazł się w złym miejscu o złym czasie? Jeno tego nie mogę zrozumieć. Wolałeś udawać, że nic się nie stało, licząc iż wszystko rozejdzie się po kościach?

\- I co byś zrobił? - Syknął. - Nie jestem byle lepszym dzieckiem we mgle, robiłem co mogłem, aby wypełnić wolę ojca i spotkała mnie za to kara. Gdybym się postawił ojcu, to co bym miał? Nic, kompletnie nic. Mój ojciec stracił dużo na znaczeniu po zmianie nam panującego. Co miałem ci powiedzieć? Przecież ty jesteś człowiekiem, który nigdy chyba nie łga, nie oszukuje, zawsze dotrzymuje słowa i jest wierny tradycji. Nie wiem jak to robisz, że potrafisz być ze wszech miar dobry, ale ja tak nie potrafię. I bałem się, że mnie porzucisz, kiedy dowiesz się, że nie jestem taki jak ty...

⚫ Liam przysiadł na odsuniętym sobie od toaletki krześle. Nie odpowiedział Javierowi na jego słowa i obawy, właściwie to milczał długo, pogrążony w swoich rozważaniach. Przez ostatnie tygodnie przybyło mu nieco siwizny na skroniach, które w ciepłym świetle świec połyskiwały teraz srebrnymi niteczkami.

⚫- Może ty też znasz mnie gorzej niż sądzisz, jako i ja ciebie, jak się właśnie jawi. Może i bym ci nie pomógł, ale bym wiedział co się u ciebie dzieje. Albo li chociaż nie ratował życia temu emisariuszowi. Javierze, do licha! Czym właściwie dla ciebie jest ta nasza relacja?

\- Wszystkim i niczym. Nie wyobrażam sobie życia bez ciebie, ale jednocześnie nikt nie może wiedzieć co robimy. - Cicho ale z bólem mówił Javier. - Poza tym znam cię, uratowałbyś życie choćby to był twój otwarty wróg.

⚫- Jakiego "życia beze mnie"? Toć wizyty raz w miesiącu, okazjonalne biesiadowanie, parę przelotnych spotkań, to nie życie. To tylko ulotne uciechy. - Odparł gorzkim tonem. - Jestem ogromnie rozczarowany, Javierze, że zaufanie, które w tobie pokładałem było tak jednostronne.

⚫ Mówiąc to, hrabia podniósł na Javiera wzrok swoich szarozioelonych oczu.

\- Tak to widzisz? - Podniósł głos Javier i wstał z łóżka - to idź i powiedz światu prawdę. Wtedy będę u twojego boku cały czas. - Głos zaczął mu się łamać - Myślałem, że jak przyjedziesz do stolicy, to będziemy spędzać znacznie więcej czasu niż teraz. To nie małżeństwo, to tylko spotkania kochanków, czasem, w nocy. Jednak kocham cię, i jeżeli tylko tak mogę z tobą być, to zgadzam się na to. - Łzy popłynęły Javierowi po policzku kiedy stał tak bezbronny naprzeciw Liama. - Kocham cię, nawet jeżeli popełniam błędy.

⚫- Przepraszam. - Westchnął cicho i wyciągnął dłoń ku niemu. Ucałował wnętrze jego dłoni i zamknął w swoich. - Rozeźliła mię myśl, że nie chcesz dzielić się swoimi troskami ze mną, ale nie powinienem przysparzać ci więcej smutku. W istocie nie to było moją intencją, bo drogi mi jesteś i martwię się o ciebie jak o nikogo innego.

\- Mój drogi, mój drogi - z bólem mówił Javier - miałeś tyle problemów z tą Prianką, jeszcze przez nią masz uszkodzoną nogę... Żyłeś jednak w spokoju którego nie chciałem zakłócać. Ty też słuchałeś karnie swojego ojca wtedy, i... do samego końca go słuchałeś albo przynajmniej starałeś się nie wejść z nim w konflikt.

⚫- Lecz ja nie sprzeniewierzałem się cesarskiej władzy... - jęknął. - Mój szacunek dla ojca nikomu nie czynił krzywdy i nie sprowadzał nadmiernego niebezpieczeństwa. Pomyśl. Przecież gdyby twoje losy potoczyły się nieco odmiennie, gdybyś stanął przed radą czy trybunałem, marny byłyby lost twój, Javierze, ojca twego i sióstr. Mądry jesteś, a jakbyś nie był tego świadom.

\- Nie widziałem tego w ten sposób, a ciebie nie było nigdzie w pobliżu, aby się poradzić. - Podszedł do zwierciadła, gdzie leżał grzebień, podarunek od jego matki. - Pojedziemy razem do stolicy?

⚫- Będę musiał domknąć sprawę biblioteki, wziąć udział w koronacji brata, oglądać żal i rozczarowanie Irvetty, oraz wrócić do mej Twierdzy, na Pogranicze. W moich wsiach zapewne jest sporo zniszczeń po zimie, chłopów przymierających głodem po przedwiośniu, muszę rozmówić się z Prianką, spotkać się z jej wujem, a mym seniorem... ale tak. Ostatecznie pojedziemy do stolicy.

⚫ Maksimiliam wstał powoli, bez laski kulał wyraźnie, a po polowaniu czuł nadwyrężony mięsień w okolicach rany, ale nie uskarżał się, to i tak nic by nie zmieniło. Objął Javiera ramionami, stojąc za nim.

⚫On zaś rękami objął ramiona, które go tuliły.

\- Tak bardzo chciałbym wszystkim powiedzieć o nas. ⚫Tak bardzo...

⚫- Wiem. - Przytulił policzek do jego włosów. - Nie jest łatwo, ale i tak dziękuję bogom, że mam ciebie, chociaż prawda, chciałbym zasypiać przy tobie i budzić się co rano. Nigdzie się nie spieszyć... Śniadać z tobą, i doglądać spraw.

\- Myślę, że jak zamieszkamy razem w stolicy, to nikt na to źle nie spojrzy, jeżeli tylko będą dwie sypialnie. Zwłaszcza, że pochodzimy z tych samych okolic, to trzymanie się razem nie będzie koliło w oczy. Ale już dosyć, proszę. Mniej rozmowy a więcej ciepła, proszę.

Koniec tomu 1